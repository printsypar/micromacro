#ifndef UTILITIES
#define UTILITIES

#include <ostream>
#include <iomanip>      // std::setw
#include <iostream>     // std::cout, std::endl

/// Printing output to file and terminal
class Tee {
    std::ostream &first, &second;

    template<typename T> friend Tee& operator<< (Tee&, T);

public:
    Tee(std::ostream &f, std::ostream &s) : first(f), second(s) {}
};

template <typename T>
Tee& operator<< (Tee &t, T val)
{
    t.first << val << std::flush;
    t.second << val << std::flush;
    return t;
}

/// Global variable
extern Tee* __cout;


/// Progress bar
static inline void loadbar(unsigned int x, unsigned int n, unsigned int w = 50)
{
    if ( (x != n) && (x % (n/100+1) != 0) ) return;

    float ratio  =  x/(float)n;
    int   c      =  ratio * w;

    cout << setw(3) << (int)(ratio*100) << "% [";
    for (int x=0; x<c; x++) cout << "=";
    for (int x=c; x<w; x++) cout << " ";
    cout << "]\r" << flush;
}

#endif // UTILITIES

