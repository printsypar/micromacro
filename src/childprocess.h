#ifndef CHILDPROCESS_H
#define CHILDPROCESS_H

#include<../src/includes.h>
#include<../src/utilities.h>

class childProcess:QProcess
{
public:
    childProcess(QString);
    ~childProcess();
    void setProcessName(QString _pN){processName = _pN;}
    int run(QString);
    void setArgs(QStringList _args){args = _args;}
    void removeFiles(QString, QString);
private:
    QString processName;
    QString program;
    QStringList args;
};

#endif // CHILDPROCESS_H
