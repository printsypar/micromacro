#ifndef SINGLETON_H
#define SINGLETON_H
//-----------------------------------------------------------------------------
template<typename T> class SINGLETON
{
 public:
  static T& Instance() {
    static T theSingleInstance; // T must have a protected default constructor
    return theSingleInstance;
  }
};
//-----------------------------------------------------------------------------
#endif
