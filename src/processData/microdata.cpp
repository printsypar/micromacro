#include "microdata.h"
#include<../src/processData/macrodata.h>

microData::microData(int _sampleNum)
{
    sampleNum = _sampleNum;
    pPatern = new microPattern[sampleNum];
}
microData::~microData()
{
    delete[] pPatern;
}
void microData::readKFlowDictFromGDR(QString inputFileName, int _sampleNum)
{
    *__cout << ">>>>> READING K from GDR" << inputFileName.toLatin1().constData() << "\n";
    QFile file(inputFileName);
    if (!file.open(QIODevice::ReadOnly|QIODevice::Text))
    {
        *__cout << "Could not open file " << inputFileName.toLatin1().constData() << "\n";
        exit(1);
    }
    QTextStream in(&file);
    double K[9]={.0, .0, .0, .0, .0, .0, .0, .0, .0};
    char str[128];
    while (!in.atEnd())
    {
        QString line = in.readLine();
        if (line.contains("Permeability"))
        {
            for (int j=1; j<4; j++)
                for (int i=1; i<4; i++)
                {
                    if (line.contains("Permeability"+QString::number(i)+QString::number(j)))
                    {
                        sscanf(line.toLatin1().constData(),"%s %le",str,&(K[(i-1)*3+j-1]));
                    }
                }
            pPatern[_sampleNum].setInflowPorousVoxelPerm(K);
        }
    }
    *__cout << "<<<<< Done " << "\n";
}

void microData::readKFilterDictFromTXT(QString inputFileName, int _sampleNum)
{
    *__cout << ">>>>> READING K from TXT " << inputFileName.toLatin1().constData() << "\n";
    QFile file(inputFileName);
    if (!file.open(QIODevice::ReadOnly|QIODevice::Text))
    {
        *__cout << "  Could not open file " << inputFileName.toLatin1().constData() << "\n";
        exit(1);
    }
    QTextStream in(&file);
    double K[9]={.0, .0, .0, .0, .0, .0, .0, .0, .0};
    int dir;
    char str[128];
    while (!in.atEnd())
    {
        QString line = in.readLine();
        if (line.contains("Direction"))
        {
            sscanf(line.toLatin1().constData(),"%s %d",str,&dir);
        }
        if (line.contains("PhysicalFlowPermeabilitiesX"))
        {
            sscanf(line.toLatin1().constData(),"%s %le",str,&(K[dir]));
        }
        if (line.contains("PhysicalFlowPermeabilitiesY"))
        {
            sscanf(line.toLatin1().constData(),"%s %le",str,&(K[3+dir]));
        }
        if (line.contains("PhysicalFlowPermeabilitiesZ"))
        {
            sscanf(line.toLatin1().constData(),"%s %le",str,&(K[6+dir]));
        }
    }
    pPatern[_sampleNum].setInflowPorousVoxelPerm(K);
    *__cout << "  Inflow Porous Voxel #" << _sampleNum << "\n";
    *__cout << "  K[" << dir << "] = " << K[dir] << "\n";
    *__cout << "  K[" << dir+3 << "] = " << K[dir+3] << "\n";
    *__cout << "  K[" << dir+6 << "] = " << K[dir+6] << "\n";
    *__cout << "<<<<< Done" << "\n";
}

void microData::initPattern(int _sampleNum, int _i, int _j, int _k, macroData *macData)
{
    *__cout << ">>>>> microData::initPattern strated... " << "\n";
    // setting the Inflow Porous Voxel
    pPatern[_sampleNum].setInflowPorousVoxelInd(_i, _j, _k);
    int N = macData->getCount();
    for (int i=0; i<N; i++)
    {
        if (macData->getIndexI(i) == _i
                && macData->getIndexJ(i) == _j
                && macData->getIndexK(i) == _k)
        {
            pPatern[_sampleNum].setInflowPorousVoxelInd(i);
            if (macData->getPorousIndicator(i)!=1) {
                *__cout << "  Input voxel (" << _i << ", " << _j << ", " << _k << ") is not porous!!!" << "\n";
                exit(1);
            }
        }
    }

    // setting the Fluid Voxels
    int I[6] = {_i-1, _i+1, _i, _i, _i, _i};
    int J[6] = {_j, _j, _j-1, _j+1, _j, _j};
    int K[6] = {_k, _k, _k, _k, _k-1, _k+1};
    int index[6];
    int l = 0;
    for (int pos=0; pos<6; pos++)
    {
        //printf("Looking for (%d, %d, %d)\n",I[pos],J[pos],K[pos]);
        for (int i=0; i<N; i++)
        {
            if (macData->getIndexI(i) == I[pos]
                    && macData->getIndexJ(i) == J[pos]
                    && macData->getIndexK(i) == K[pos]
                    && macData->getPorousIndicator(i) == 0)
            {
                index[l] = i;
                l++;
            }
        }
    }
    *__cout << "  Fluid " << l << " VOXELS found" << "\n";
    if (l==0) exit(1);
    pPatern[_sampleNum].setFluidVoxel(l);
    for (int i=0; i<l; i++)
    {
        pPatern[_sampleNum].setFluidVoxelInd(i, macData->getIndexI(index[i]),
                                             macData->getIndexJ(index[i]),
                                             macData->getIndexK(index[i]));
        pPatern[_sampleNum].setFluidVoxelInd(i, index[i]);
    }

    // setting the Porous Voxels
    // ...

    *__cout << "<<<<< Done" << "\n";
}

void microData::setVelocity(macroData* macData)
{
    double vx, vy, vz;
    for (int i=0; i<sampleNum; i++)
    {
        for (int ind=0; ind<pPatern[i].getFluidVoxelNum(); ind++)
        {
            vx = macData->getVelocity(0,pPatern[i].getFluidVoxelInd(ind));
            vy = macData->getVelocity(1,pPatern[i].getFluidVoxelInd(ind));
            vz = macData->getVelocity(2,pPatern[i].getFluidVoxelInd(ind));
            pPatern[i].setFluidVoxelVelocity(ind,vx,vy,vz);
        }
    }
}

void microData::print()
{
    *__cout << ">>>>> Print sample info after full run..." << "\n";
    for (int i=0; i<sampleNum; i++)
    {
        *__cout << "  Sample Number #" << i << "\n";
        pPatern[i].print();
    }
    *__cout << "<<<<< Done" << "\n";
}

microPattern::microPattern()
{
    nFluidVoxel = 0;
    nPorousVoxel = 0;
    pFluidVoxel = NULL;
    pPorousVoxel = NULL;
}

microPattern::~microPattern()
{
    if (pFluidVoxel != NULL) {
        delete[] pFluidVoxel;
        pFluidVoxel = NULL;
    }
    if (pPorousVoxel != NULL) delete[] pPorousVoxel;
}

void microPattern::setInflowPorousVoxelInd(int _i, int _j, int _k)
{
    inflowPorousVoxel.i = _i;
    inflowPorousVoxel.j = _j;
    inflowPorousVoxel.k = _k;
}

void microPattern::setInflowPorousVoxelPerm(double _K[])
{
    for (int i=0; i<9; i++)
        inflowPorousVoxel.K[i] = _K[i];
}

void microPattern::setFluidVoxel(int n)
{
    nFluidVoxel = n;
    pFluidVoxel = new Voxel[n];
}

void microPattern::setFluidVoxelInd(int ind, int _i, int _j, int _k)
{
    pFluidVoxel[ind].i = _i;
    pFluidVoxel[ind].j = _j;
    pFluidVoxel[ind].k = _k;
}

void microPattern::setFluidVoxelVelocity(int ind, double _Vx, double _Vy, double _Vz)
{
    pFluidVoxel[ind].V[0] = _Vx;
    pFluidVoxel[ind].V[1] = _Vy;
    pFluidVoxel[ind].V[2] = _Vz;
}

void microPattern::setPorousVoxel(int n)
{
    nPorousVoxel = n;
    pPorousVoxel = new Voxel[n];
}

void microPattern::setPorousVoxelInd(int ind, int _i, int _j, int _k)
{
    pPorousVoxel[ind].i = _i;
    pPorousVoxel[ind].j = _j;
    pPorousVoxel[ind].k = _k;
}

void microPattern::setPorousVoxelPerm(int ind, double _K[])
{
    for (int i=0; i<9; i++)
    {
        pPorousVoxel[ind].K[i] = _K[i];
    }
}

void microPattern::print(void)
{
    inflowPorousVoxel.print();
    for (int i=0; i<nFluidVoxel; i++)
        pFluidVoxel[i].print();
    for (int i=0; i<nPorousVoxel; i++)
        pPorousVoxel[i].print();
}
