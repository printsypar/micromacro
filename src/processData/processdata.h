#ifndef PROCESSDATA_H
#define PROCESSDATA_H

#include<../src/processData/microdata.h>
#include<../src/processData/macrodata.h>

class processData
{
private:
    double time;    // [s]
    microData *micData;
    macroData *macData;
public:
    processData(int gdtNum);
    ~processData();
    double getTime(void){return time;}
    void setTime(double dt){time+=dt;}
    microData* getMicroData(void){return micData;}
    macroData* getMacroData(void){return macData;}
};

#endif // PROCESSDATA_H
