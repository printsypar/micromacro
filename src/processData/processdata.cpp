#include "processdata.h"

processData::processData(int gdtNum)
{
    time = 0.0;
    micData = new microData(gdtNum);
    macData = new macroData();
}

processData::~processData()
{
    if (micData!=NULL) delete micData;
    if (macData!=NULL) delete macData;
}

