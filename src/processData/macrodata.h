#ifndef MACRODATA_H
#define MACRODATA_H

#include<../src/includes.h>
#include<../src/utilities.h>
#include<stdio.h>

class microData;
class MATRIX;

class macroData
{
public:
    macroData();
    ~macroData();
    void readKFromFile(QString);
    void writeKToFile(QString);
    void writeKToFile(QString,double);
    void setPerm(int, int, int, int, int, int, double);
    void setPerm(double);
    void setPermLinearInterpolation(microData*, int);
    void setPermLinearInterpolation(microData*, int, int, int);
    void setPermLinearInterpolationVelocityWeights(microData*, int);
    void setPermLinearInterpolationVelocityBased(microData*, int, bool);
    int getCount(void) {return count;}
    int getIndexI(int ind) {return x[ind];}
    int getIndexJ(int ind) {return y[ind];}
    int getIndexK(int ind) {return z[ind];}
    int getPorousIndicator(int ind) {return porousIndicator[ind];}
    void readVPFromFile(QString);
    void writePorousVelocityToFile(QString);
    void writePorousPermToFile(QString);
    double getVoxelSize(){return dx;}
    double getVelocity(int dir, int ind)
    {
        if (dir==0) return vx[ind];
        else if (dir==1) return vy[ind];
        else if (dir==2) return vz[ind];
        return -1;
    }
private:
    int count;
    double dx, dy, dz; //voxel size
    int minX, minY, minZ;
    int *x; // i index
    int *y; // j index
    int *z; // k index
    int *porousIndicator;   // 1 - porous, 0 - void voxel
    double density;     // [kg/m^3]
    double viscosity;   // [kg/m/s]
    double flowrate;    // [l/min]
    double *perm;   // permeability [m^2]
    double *p;      // pressure     [Pa]
    double *vx;     // velosity     [m/s]
    double *vy;
    double *vz;
};

#endif // MACRODATA_H
