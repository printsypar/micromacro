#include "macrodata.h"
#include<../src/processData/microdata.h>

macroData::macroData()
{
    count = 0;
    x = NULL;
    y = NULL;
    z = NULL;
    porousIndicator = NULL;
    perm = NULL;
    p = NULL;
    vx = NULL;
    vy = NULL;
    vz = NULL;
}
macroData::~macroData()
{
    if (x!=NULL) delete []x;
    if (y!=NULL) delete []y;
    if (z!=NULL) delete []z;
    if (porousIndicator!=NULL) delete []porousIndicator;
    if (perm!=NULL) delete []perm;
    if (p!=NULL) delete []p;
    if (vx!=NULL) delete []vx;
    if (vy!=NULL) delete []vy;
    if (vz!=NULL) delete []vz;
}
void macroData::readKFromFile(QString inputFileName)
{
    FILE* pFile = fopen(inputFileName.toLatin1().constData(),"r");
    if (pFile == NULL){
        *__cout << "Could not open the file " << inputFileName.toLatin1().constData() << "\n";
        exit(1);
    } else {
        *__cout << ">>>>> Reading file " << inputFileName.toLatin1().constData() << "\n";
    }

    int _count= 0;
    char tempString[1024];
    fscanf(pFile,"%lf\t",&dx);
    fscanf(pFile,"%lf\t",&dy);
    fscanf(pFile,"%lf\n",&dz);
    while (!feof(pFile)) {
        // ASCII VERSION
        _count++;
        fscanf(pFile, "%s", tempString);
    }
    count = (_count-1)/9;

    if (perm != NULL) delete []perm;
    if (x != NULL) delete []x;
    if (y != NULL) delete []y;
    if (z != NULL) delete []z;
    if (porousIndicator != NULL) delete []porousIndicator;
    perm = new double[count];
    x = new int[count];
    y = new int[count];
    z = new int[count];
    porousIndicator = new int[count];

    rewind ( pFile );
    for (int i=0; i<3; i++)
        fscanf(pFile, "%s", tempString);
    double tempPerm;
    for (int i=0; i<count; i++) {
        fscanf(pFile, "%d\t", &(x[i]));
        fscanf(pFile, "%d\t", &(y[i]));
        fscanf(pFile, "%d\t", &(z[i]));
        fscanf(pFile, "%lf ", &perm[i]);
        if (fabs(perm[i]+1.0)<1.e-8)
            porousIndicator[i] = 0;
        else
            porousIndicator[i] = 1;
        perm[i] *= 1.e-6;
        for(int j=1; j<6; j++)
            fscanf(pFile, "%lf ", &tempPerm);
        fscanf(pFile,"\n");

        if (i==0)
        {
            minX = x[i]; minY = y[i]; minZ = z[i];
        }
        else
        {
            if (x[i] < minX) minX = x[i];
            if (y[i] < minY) minY = y[i];
            if (z[i] < minZ) minZ = z[i];
        }
        //printf("%d:\t(%d, %d, %d)\t%.3le\n",i,x[i],y[i],z[i],perm[i]);
        loadbar(i, count);
    }
    cout << endl;
    /*for (int i=0; i<count; i++)
    {
        x[i] -= minX;
        y[i] -= minY;
        z[i] -= minZ;
    }*/
    fclose(pFile);
    *__cout << "<<<<< Done" << "\n";
}

void macroData::writeKToFile(QString inputFileName)
{
    //setting permeability to savedK.sol
    FILE* pFile = fopen(inputFileName.toLatin1().constData(),"w");
    if (pFile == NULL){
        *__cout << "Could not open the file " << inputFileName.toLatin1().constData() << "\n";
        exit(1);
    } else {
        *__cout << ">>>>> Writing to file " << inputFileName.toLatin1().constData() << "\n";
    }
    fprintf(pFile,"%.10e\t",dx);
    fprintf(pFile,"%.10e\t",dy);
    fprintf(pFile,"%.10e\n",dz);
    for (int i=0; i<count; i++) {
        fprintf(pFile, "%d\t", x[i]);//+minX);
        fprintf(pFile, "%d\t", y[i]);//+minY);
        fprintf(pFile, "%d\t", z[i]);//+minZ);
        fprintf(pFile, "%.10e ", perm[i]*1.e+6);
        fprintf(pFile, "%.10e ", 0.0);
        fprintf(pFile, "%.10e ", 0.0);
        fprintf(pFile, "%.10e ", perm[i]*1.e+6);
        fprintf(pFile, "%.10e ", 0.0);
        fprintf(pFile, "%.10e ", perm[i]*1.e+6);
        fprintf(pFile,"\n");
        loadbar(i, count);
    }
    cout << endl;
    fclose(pFile);
    *__cout << "<<<<< Done" << "\n";
}

void macroData::writeKToFile(QString inputFileName, double _perm)
{
    //setting permeability in xml file
    Xmldoc inputFile(inputFileName);
    QDomDocument *doc = inputFile.getDomDocument();
    QDomText permTextNode = doc->createTextNode(QString::number(_perm));
    if (inputFile.paramAvailable("ProjectInfo::FilterMediaParameters::Media0::Permeability")){
        inputFile.setParameter("ProjectInfo::FilterMediaParameters::Media0::Permeability", permTextNode);
    }
    else{
        *__cout <<"ProjectInfo::FilterMediaParameters::Media0::Permeability is unavailable. *.xml input file is buggy " ;
        exit(1);
    }
    QFile file(inputFileName);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    out << inputFile.getDomDocument()->toString();
    file.close();
}

void macroData::setPerm(int x0, int x1, int y0, int y1, int z0, int z1, double _perm)
{
    for (int i=0; i<count; i++) {
        if(x[i]>=x0 && x[i]<=x1 && y[i]>=y0 && y[i]<=y1 && z[i]>=z0 && z[i]<=z1)
            if (porousIndicator[i] == 1)
                perm[i] = _perm;
    }
}

void macroData::setPerm(double _perm)
{
    *__cout << ">>>>> Starting constant interpolation";
    for (int i=0; i<count; i++)
    {
        if (porousIndicator[i] == 1)
            perm[i] = _perm;
    }
    *__cout << "<<<<< Done" << "\n";
}

void macroData::setPermLinearInterpolation(microData* micData, int interDir)
{
    *__cout << ">>>>> Starting linear interpolation" << "\n";
    if (micData->getNumOfSamples()<2){
        *__cout << "macroData::setPermLinearInterpolation: number of samples less than 2. Linear interpolation failed! " ;
        exit(1);
    }

    int numOfSamples = micData->getNumOfSamples();
    int **Ind = new int*[numOfSamples];
    for (int i=0; i<numOfSamples; i++)
        Ind[i] = new int[3];
    double *Perm = new double[numOfSamples];

    *__cout << "  I\tJ\tK\tPermeability\n";
    for (int i=0; i<numOfSamples; i++) {
        for (int j=0; j<3; j++) {
            Ind[i][j] = micData->getInflowPorousVoxelInd(i,j);
        }
        Perm[i] = micData->getPerm(i,8);
        *__cout << "  " << Ind[i][0] << "\t" << Ind[i][1] << "\t" << Ind[i][2] << "\t" << Perm[i] << "\n";
    }
    bool sort = false;
    for (int i=0; i<numOfSamples-1; i++) {
        if (Ind[i][interDir]>Ind[i+1][interDir])
            sort = true;
    }
    if (sort) *__cout << "  Sorting is required" << "\n";
    else *__cout << "  Sorting is NOT required" << "\n";
    int minInd;
    int temp=-1;
    double tempDouble;
    if(sort) {
        for (int i=0; i<numOfSamples-1; i++) {
            minInd = i;
            for (int j=i+1; j<numOfSamples; j++) {
                if (Ind[minInd][interDir]>Ind[j][interDir])
                    minInd = j;
            }
            if (minInd!=i) {
                for (int j=0; j<3; j++) {
                    temp = Ind[minInd][j];
                    Ind[minInd][j] = Ind[i][j];
                    Ind[i][j] = temp;
                }
                tempDouble = Perm[minInd];
                Perm[minInd] = Perm[i];
                Perm[i] = tempDouble;
            }
        }
        *__cout << "  I\tJ\tK\tPermeability\n";
        for (int i=0; i<numOfSamples; i++) {
            *__cout << "  " << Ind[i][0] << "\t" << Ind[i][1] << "\t" << Ind[i][2] << "\t" << Perm[i] << "\n";
        }
    }

    bool found;
    for (int i=0; i<count; i++)
    {
        if (porousIndicator[i] == 1)
        {
            switch (interDir) {
            case 0: temp = x[i]; break;
            case 1: temp = y[i]; break;
            case 2: temp = z[i]; break;
            }
            if (temp <= Ind[0][interDir]) {
                perm[i] = Perm[0];
            } else {
                found = false;
                for (int j=0; j<numOfSamples-1; j++) {
                    if (temp == Ind[j][interDir]) {
                        perm[i] = Perm[j];
                        found = true;
                    } else if (temp>Ind[j][interDir] && temp<Ind[j+1][interDir]) {
                        perm[i] = (double)(temp-Ind[j][interDir])/(double)(Ind[j+1][interDir]-Ind[j][interDir])*(Perm[j+1]-Perm[j])+Perm[j];
                        found = true;
                    }
                }
                if (!found) {
                    perm[i] = Perm[numOfSamples-1];
                }
            }
//            if (x[i]==245 && z[i]==182) perm[i] = 0.1;
//            if (x[i]==172 && z[i]==88) perm[i] = 0.2;
//            if (x[i]==124 && z[i]==202) perm[i] = 0.3;
//            if (x[i]==54 && z[i]==254) perm[i] = 0.4;
        }
    }
    for (int i=0; i<numOfSamples; i++)
        delete[] Ind[i];
    delete[] Ind;
    delete[] Perm;
    *__cout << "<<<<< Done" << "\n";
}

void macroData::setPermLinearInterpolation(microData* micData, int interDir, int depthDir, int interDepthSize)
{
    *__cout << ">>>>> Starting linear interpolation accounting for depth" << "\n";
    if (micData->getNumOfSamples()<2){
        *__cout << "macroData::setPermLinearInterpolation: number of samples less than 2. Linear interpolation failed! " ;
        exit(1);
    }

    int numOfSamples = micData->getNumOfSamples();
    int **Ind = new int*[numOfSamples];
    for (int i=0; i<numOfSamples; i++)
            Ind[i] = new int[3];
    double *Perm = new double[numOfSamples];

    for (int i=0; i<numOfSamples; i++) {
        for (int j=0; j<3; j++) {
            Ind[i][j] = micData->getInflowPorousVoxelInd(i,j);
        }
        Perm[i] = micData->getPerm(i,8);
        *__cout << "  " << Ind[i][0] << "\t" << Ind[i][1] << "\t" << Ind[i][2] << "\t" << Perm[i] << "\n";
    }
    bool sort = false;
    for (int i=0; i<numOfSamples-1; i++) {
        if (Ind[i][interDir]>Ind[i+1][interDir])
            sort = true;
    }
    if (sort) *__cout << "  Sorting is required\n";
    else *__cout << "  Sorting is NOT required\n";
    int minInd;
    int temp;
    double tempDouble;
    if(sort) {
        for (int i=0; i<numOfSamples-1; i++) {
            minInd = i;
            for (int j=i+1; j<numOfSamples; j++) {
                if (Ind[minInd][interDir]>Ind[j][interDir])
                    minInd = j;
            }
            if (minInd!=i) {
                for (int j=0; j<3; j++) {
                    temp = Ind[minInd][j];
                    Ind[minInd][j] = Ind[i][j];
                    Ind[i][j] = temp;
                }
                tempDouble = Perm[minInd];
                Perm[minInd] = Perm[i];
                Perm[i] = tempDouble;
            }
        }
    }
    *__cout << "  I\tJ\tK\tPermeability\n";
    for (int i=0; i<numOfSamples; i++) {
        *__cout << "  " << Ind[i][0] << "\t" << Ind[i][1] << "\t" << Ind[i][2] << "\t" << Perm[i] << "\n";
    }
    *__cout << "\n";
    ///////////////////////////////////////////////////////////////////
    double K=0;
    int maxXi;
    int depthInd=-1, interInd=-1;
    for (int i=0; i<count; i++)
    {
        switch(depthDir) {
        case 0: depthInd = x[i]; break;
        case 1: depthInd = y[i]; break;
        case 2: depthInd = z[i]; break;
        }
        switch(interDir) {
        case 0: interInd = x[i]; break;
        case 1: interInd = y[i]; break;
        case 2: interInd = z[i]; break;
        }

        if (porousIndicator[i] == 1
                && depthInd>Ind[0][depthDir]-1
                && depthInd<Ind[0][depthDir]+interDepthSize)
        {
            K = perm[i];
            maxXi = x[i];
            for (int j=0; j<count; j++)
            {
                if(porousIndicator[j]==1) {
                    switch(depthDir){
                    case 0: if(y[i]==y[j] && z[i]==z[j] && maxXi<x[j]){
                            maxXi = x[j];
                            K = perm[j];
                        }
                        break;
                    case 1: if(x[i]==x[j] && z[i]==z[j] && maxXi<y[j]){
                            maxXi = y[j];
                            K = perm[j];
                        }
                        break;
                    case 2: if(y[i]==y[j] && x[i]==x[j] && maxXi<z[j]){
                            maxXi = z[j];
                            K = perm[j];
                        }
                        break;
                    }
                }
            }
            if (interInd <= Ind[0][interDir]) {
                    perm[i] = Perm[0];
                } else {
                    bool found = false;
                    for (int j=0; j<numOfSamples-1; j++) {
                        if (interInd == Ind[j][interDir]) {
                            perm[i] = Perm[j];
                            found = true;
                        } else if (interInd>Ind[j][interDir] && interInd<Ind[j+1][interDir]) {
                            perm[i] = (double)(interInd-Ind[j][interDir])/(double)(Ind[j+1][interDir]-Ind[j][interDir])*(Perm[j+1]-Perm[j])+Perm[j];
                            found = true;
                        }
                    }
                    if (!found) {
                        perm[i] = Perm[numOfSamples-1];
                    }
                }
            }
            perm[i] += (K-perm[i])/(double)interDepthSize*(double)(depthInd-Ind[0][depthDir]);

        if (porousIndicator[i] == 1 && y[i]==15 && z[i]==25)
            *__cout << x[i] << "\t" << y[i] << "\t" << z[i] << "\t" << perm[i] << "\n";
    }
    for (int i=0; i<numOfSamples; i++)
        delete[] Ind[i];
    delete[] Ind;
    delete[] Perm;
    *__cout << "<<<<< Done" << "\n";
}

void macroData::setPermLinearInterpolationVelocityWeights(microData* micData, int interDir)
{
    *__cout << ">>>>> Starting linear interpolation using velocity weights" << "\n";
    if (micData->getNumOfSamples()<2){
        *__cout << "macroData::setPermLinearInterpolation: number of samples less than 2. Linear interpolation failed! " ;
        exit(1);
    }

    int numOfSamples = micData->getNumOfSamples();
    int *ind = new int[numOfSamples];
    int **Ind = new int*[numOfSamples];
    for (int i=0; i<numOfSamples; i++)
        Ind[i] = new int[3];
    vector<double> Perm(numOfSamples);

    for (int i=0; i<numOfSamples; i++) {
        ind[i] = micData->getInflowPorousVoxelInd(i);
        for (int j=0; j<3; j++) {
            Ind[i][j] = micData->getInflowPorousVoxelInd(i,j);
        }
        Perm[i] = micData->getPerm(i,8);
        *__cout << "  " << Ind[i][0] << "\t" << Ind[i][1] << "\t" << Ind[i][2] << "\t" << Perm[i] << "\n";
    }
    bool sort = false;
    for (int i=0; i<numOfSamples-1; i++) {
        if (Ind[i][interDir]>Ind[i+1][interDir])
            sort = true;
    }
    if (sort) *__cout << "  Sorting is required\n";
    else *__cout << "  Sorting is NOT required\n";
    int minInd;
    int temp = -1;
    double tempDouble;
    if(sort) {
        for (int i=0; i<numOfSamples-1; i++) {
            minInd = i;
            for (int j=i+1; j<numOfSamples; j++) {
                if (Ind[minInd][interDir]>Ind[j][interDir])
                    minInd = j;
            }
            if (minInd!=i) {
                for (int j=0; j<3; j++) {
                    temp = Ind[minInd][j];
                    Ind[minInd][j] = Ind[i][j];
                    Ind[i][j] = temp;
                }
                temp = ind[minInd];
                ind[minInd] = ind[i];
                ind[i] = temp;
                tempDouble = Perm[minInd];
                Perm[minInd] = Perm[i];
                Perm[i] = tempDouble;
            }
        }
    }
    *__cout << "  I\tJ\tK\tPermeability\n";
    for (int i=0; i<numOfSamples; i++) {
        *__cout << "  " << Ind[i][0] << "\t" << Ind[i][1] << "\t" << Ind[i][2] << "\t" << Perm[i] << "\n";
    }
    *__cout << "\n";

    //FILE *f = fopen(fileName.toLatin1().constData(),"w");

    double w1, w2;
    bool found;
    for (int i=0; i<count; i++)
    {
        if (porousIndicator[i] == 1)
        {
            switch (interDir) {
            case 0: temp = x[i]; break;
            case 1: temp = y[i]; break;
            case 2: temp = z[i]; break;
            }
            int ind1=-1, ind2=-1;
            if (temp <= Ind[0][interDir]) {
                ind1 = 0;
                ind2 = 1;
            } else {
                found = false;
                for (int j=0; j<numOfSamples-1; j++) {
                    if (temp>Ind[j][interDir] && temp<=Ind[j+1][interDir]) {
                        found = true;
                        ind1 = j;
                        ind2 = j+1;
                    }
                }
                if (!found) {
                    ind1 = numOfSamples-2;
                    ind2 = numOfSamples-1;
                }
            }
            double minV, maxV;
            int minI=-1, maxI=-1;
            if (vz[ind[ind1]]<vz[ind[ind2]]){
                minV = vz[ind[ind1]]; minI = ind1;
                maxV = vz[ind[ind2]]; maxI = ind2;
            } else {
                minV = vz[ind[ind2]]; minI = ind2;
                maxV = vz[ind[ind1]]; maxI = ind1;
            }
            if (vz[i]<=maxV && vz[i]>=minV){
                w1 = fabs(vz[ind[minI]]-vz[i]);
                w2 = fabs(vz[ind[maxI]]-vz[i]);
                perm[i] = (w2*Perm[minI]+w1*Perm[maxI])/(w1+w2);
            } else if(vz[i]<minV){
                perm[i] = Perm[minI];
            } else {
                perm[i] = Perm[maxI];
            }
            //if (porousIndicator[i] == 1 && z[i]==49 && y[i]==15)
            //    fprintf(f,"%d\t%d\t%d\t%le\t%le\n",x[i],y[i],z[i],perm[i],vz[i]);
        }
    }
    //fclose(f);
    delete [] ind;
    for (int i=0; i<numOfSamples; i++)
        delete[] Ind[i];
    delete[] Ind;
    *__cout << "<<<<< Done" << "\n";
}

void macroData::setPermLinearInterpolationVelocityBased(microData* micData, int flowDir, bool positiveFlowDir)
{
    *__cout << ">>>>> Starting Velocity-Based Interpolation" << "\n";
    if (micData->getNumOfSamples()<2){
        *__cout << "macroData::setPermInterpolation: ";
        *__cout << "number of samples less than 2. Velocity-based interpolation failed! " ;
        exit(1);
    }

    int numOfSamples = micData->getNumOfSamples();
    int *ind = new int[numOfSamples];
    int **Ind = new int*[numOfSamples];
    for (int i=0; i<numOfSamples; i++)
        Ind[i] = new int[3];
    double *Perm = new double[numOfSamples];
    double *Vel = new double[numOfSamples];

    *__cout << "  I\tJ\tK\tPermeability\tVelocity\n";
    for (int i=0; i<numOfSamples; i++) {
        ind[i] = micData->getInflowPorousVoxelInd(i);
        for (int j=0; j<3; j++) {
            Ind[i][j] = micData->getInflowPorousVoxelInd(i,j);
        }
        Perm[i] = micData->getPerm(i,8);
        Vel[i] = micData->getVelocity(i,0,flowDir);
        *__cout << "  " << Ind[i][0] << "\t" << Ind[i][1] << "\t" << Ind[i][2] << "\t" << Perm[i] << "\t" << Vel[i] << "\n";
    }
    bool sort = false;
    for (int i=0; i<numOfSamples-1; i++) {
        if (Vel[i]>Vel[i+1])
            sort = true;
    }
    if (sort) *__cout << "  Sorting is required\n";
    else *__cout << "  Sorting is NOT required\n";
    int minInd;
    int temp;
    double tempDouble=0;
    if(sort) {
        for (int i=0; i<numOfSamples-1; i++) {
            minInd = i;
            for (int j=i+1; j<numOfSamples; j++) {
                if (Vel[minInd]>Vel[j])
                    minInd = j;
            }
            if (minInd!=i) {
                for (int j=0; j<3; j++) {
                    temp = Ind[minInd][j];
                    Ind[minInd][j] = Ind[i][j];
                    Ind[i][j] = temp;
                }
                temp = ind[minInd];
                ind[minInd] = ind[i];
                ind[i] = temp;
                tempDouble = Perm[minInd];
                Perm[minInd] = Perm[i];
                Perm[i] = tempDouble;
                tempDouble = Vel[minInd];
                Vel[minInd] = Vel[i];
                Vel[i] = tempDouble;
            }
        }
        for (int i=0; i<numOfSamples; i++) {
            *__cout << "  " << Ind[i][0] << "\t" << Ind[i][1] << "\t" << Ind[i][2] << "\t" << Perm[i] << "\t" << Vel[i] << "\n";
        }
    }

    bool found1, found2;
    int I, J, K;
    int index;
    for (int i=0; i<count; i++)
    {
        if (porousIndicator[i] == 1)
        {
            I = x[i]; J = y[i]; K = z[i];
            switch(flowDir){
            case 0:
                if (positiveFlowDir) I--;
                else I++;
                break;
            case 1:
                if (positiveFlowDir) J--;
                else J++;
                break;
            case 2:
                if (positiveFlowDir) K--;
                else K++;
                break;
            }
            found1 = false;
            for (int j=0; j<count; j++)
            {
                if (x[j] == I && y[j] == J && z[j] == K && porousIndicator[j] == 0)
                {
                    index = j;
                    found1 = true;
                    break;
                }
            }
            if (found1) {
                switch (flowDir) {
                case 0: tempDouble = vx[index]; break;
                case 1: tempDouble = vy[index]; break;
                case 2: tempDouble = vz[index]; break;
                }
                if (tempDouble <= Vel[0]) {
                    perm[i] = Perm[0];
                } else {
                    found2 = false;
                    for (int j=0; j<numOfSamples-1; j++) {
                        if (tempDouble>Vel[j] && tempDouble<=Vel[j+1]) {
                            found2 = true;
                            perm[i] = Perm[j]+(tempDouble-Vel[j])*(Perm[j+1]-Perm[j])/(Vel[j+1]-Vel[j]);
                        }
                    }
                    if (!found2) {
                        perm[i] = Perm[numOfSamples-1];
                    }
                }
                for (int j=0; j<count; j++)
                {
                    switch(flowDir){
                    case 0:
                        if (J == y[j] && K == z[j] && porousIndicator[j]==1) perm[j] = perm[i];
                        break;
                    case 1:
                        if (I == x[j] && K == z[j] && porousIndicator[j]==1) perm[j] = perm[i];
                        break;
                    case 2:
                        if (I == x[j] && J == y[j] && porousIndicator[j]==1) perm[j] = perm[i];
                        break;
                    }
                }
            }
            //if (x[i]==125 && y[i]==15 && z[i]==90) perm[i] = 1.;
        }
        loadbar(i, count);
    }
    cout << endl;

    delete [] ind;
    for (int i=0; i<numOfSamples; i++)
        delete[] Ind[i];
    delete[] Ind;
    delete[] Perm;
    delete[] Vel;
    *__cout << "<<<<< Done" << "\n";
}

void macroData::readVPFromFile(QString inputFileName)
{
    if (vx!=NULL) delete []vx;
    if (vy!=NULL) delete []vy;
    if (vz!=NULL) delete []vz;
    if (p!=NULL) delete []p;
    vx = new double[count];
    vy = new double[count];
    vz = new double[count];
    p = new double[count];

    FILE* pFile = fopen(inputFileName.toLatin1().constData(),"rb");
    if (pFile == NULL){
        *__cout << "Could not open the file " << inputFileName.toLatin1().constData() << "\n";
        exit(EXIT_FAILURE);
    } else {
        *__cout << ">>>>> Reading file " << inputFileName.toLatin1().constData() << "\n";
    }

    // defining size of the file
    fseek (pFile , 0 , SEEK_END);
    int lSize = ftell (pFile);
    fseek (pFile , 0 , SEEK_SET);

    // creating dummy variables
    int dummyCount = (lSize/8-3)/4-count;
    double *dummyVar = new double[dummyCount];

    // reading file
    fread(vx, sizeof(double), count, pFile);
    fread(dummyVar, sizeof(double), dummyCount, pFile);
    fread(vy, sizeof(double), count, pFile);
    fread(dummyVar, sizeof(double), dummyCount, pFile);
    fread(vz, sizeof(double), count, pFile);
    fread(dummyVar, sizeof(double), dummyCount, pFile);
    fread(p, sizeof(double), count, pFile);
    fread(dummyVar, sizeof(double), dummyCount, pFile);

    fread(&density,sizeof(double),1,pFile);
    fread(&viscosity,sizeof(double),1,pFile);
    fread(&flowrate,sizeof(double),1,pFile);

    for (int i=0; i<count; i++)
    {
        vx[i] *= 1.0e-3;
        vy[i] *= 1.0e-3;
        vz[i] *= 1.0e-3;
        p[i] *= 1.0e3;
        loadbar(i,count);
    }
    cout << endl;

    density *= 1.e+9;
    viscosity *= 1.e+3;
    flowrate *= 1.0e-6*60.0;

    /*for (int i=0; i<count; i++){
        printf("%d:\t%le\t",i,vx[i]);
        printf("%le\t",vy[i]);
        printf("%le\t",vz[i]);
        printf("%le\n",p[i]);
    }*/
    /*printf("\ndensity = %le\n",density);
    printf("viscosity = %le\n",viscosity);
    printf("flowrate = %le\n",flowrate);*/

    delete[] dummyVar;

    fclose(pFile);
    *__cout << "<<<<< Done" << "\n";
}

void macroData::writePorousVelocityToFile(QString inputFileName)
{
    FILE* pFile=fopen(inputFileName.toLatin1().constData(),"w");
    if (pFile == NULL){
        *__cout << "Could not open the file " << inputFileName.toLatin1().constData() << "\n";
        exit(1);
    } else {
        *__cout << ">>>>> Writing to file " << inputFileName.toLatin1().constData() << "\n";
    }
    fprintf(pFile,"I, J, K - indices as in savedK.sol\n");
    fprintf(pFile,"Vx, Vy, Vz - velocity in [m/s]\n");
    for (int i=0; i<count; i++) {
        if(porousIndicator[i]==1){
            fprintf(pFile, "%d\t", x[i]);
            fprintf(pFile, "%d\t", y[i]);
            fprintf(pFile, "%d\t", z[i]);
            fprintf(pFile, "%.10e\t", vx[i]);
            fprintf(pFile, "%.10e\t", vy[i]);
            fprintf(pFile, "%.10e\n", vz[i]);
        }
        loadbar(i, count);
    }
    cout << endl;
    fclose(pFile);
    *__cout << "<<<<< Done\n";
}

void macroData::writePorousPermToFile(QString inputFileName)
{
    FILE* pFile=fopen(inputFileName.toLatin1().constData(),"w");
    if (pFile == NULL){
        *__cout << "Could not open the file " << inputFileName.toLatin1().constData() << "\n";
        exit(1);
    } else {
        *__cout << ">>>>> Writing to file " << inputFileName.toLatin1().constData() << "\n";
    }
    fprintf(pFile,"I, J, K - indices as in savedK.sol\n");
    fprintf(pFile,"K - permeability in [m^2]\n");
    for (int i=0; i<count; i++) {
        if(porousIndicator[i]==1){
            fprintf(pFile, "%d\t", x[i]);
            fprintf(pFile, "%d\t", y[i]);
            fprintf(pFile, "%d\t", z[i]);
            fprintf(pFile, "%.10e\n", perm[i]);
        }
        loadbar(i, count);
    }
    cout << endl;
    fclose(pFile);
    *__cout << "<<<<< Done\n";
}

