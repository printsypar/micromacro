#ifndef MICRODATA_H
#define MICRODATA_H

#include<../src/includes.h>
#include<../src/utilities.h>

class macroData;

class Voxel {
public:
    Voxel()
    {
        i = j = k = index = 0;
        V[0] = V[1] = V[2] = 0.0;
        for (int ind=0; ind<9; ind++)
            K[ind] = 0.0;
    }
    ~Voxel(){}
    int i;
    int j;
    int k;
    int index;
    // data corresponds to micro samples,
    // but it is initialized from macro results
    double V[3]; // velocity
    // data corresponds to and
    // is initialized from micro results
    double K[9]; // permeability
    void print()
    {
        *__cout << "  Index 1d:  "<< index << ",\t";
        *__cout << "  3d: (" << i << ", " << j << ", " << k << ")\n";
        *__cout << "  velocity = (" << V[0] << ", "<< V[1] << ", "<< V[2] << ") [m/s]\n";
        *__cout << "  permeability in [m^2]:\n";
        *__cout << "    " << K[0] << "\t" << K[1] << "\t" << K[2] << "\n";
        *__cout << "    " << K[3] << "\t" << K[4] << "\t" << K[5] << "\n";
        *__cout << "    " << K[6] << "\t" << K[7] << "\t" << K[8] << "\n";
    }
};

class microPattern
{
private:

    int nFluidVoxel;
    int nPorousVoxel;
    Voxel inflowPorousVoxel;
    Voxel *pFluidVoxel;
    Voxel *pPorousVoxel;

public:

    microPattern();
    ~microPattern();

    void setInflowPorousVoxelInd(int, int, int);
    void setInflowPorousVoxelInd(int _index){inflowPorousVoxel.index = _index;}
    void setInflowPorousVoxelPerm(double[]);
    int getInflowPorousVoxelInd(){return inflowPorousVoxel.index;}
    int getInflowPorousVoxelInd(int dir){
        switch(dir){
        case 0: return inflowPorousVoxel.i; break;
        case 1: return inflowPorousVoxel.j; break;
        case 2: return inflowPorousVoxel.k; break;
        default: return -1;
        };
    }
    double getInflowPorousVoxelPerm(int ind){return inflowPorousVoxel.K[ind];}

    void setFluidVoxel(int);
    void setFluidVoxelInd(int, int, int, int);
    void setFluidVoxelInd(int samNum, int _index){pFluidVoxel[samNum].index = _index;}
    void setFluidVoxelVelocity(int, double, double, double);
    int getFluidVoxelNum(){return nFluidVoxel;}
    int getFluidVoxelInd(int samNum){return pFluidVoxel[samNum].index;}
    double getFluidVoxelVelocity(int samNum, int dir){return pFluidVoxel[samNum].V[dir];}


    void setPorousVoxel(int);
    void setPorousVoxelInd(int, int, int, int);
    void setPorousVoxelInd(int samNum, int _index){pPorousVoxel[samNum].index = _index;}
    void setPorousVoxelPerm(int, double[]);
    int getPorousVoxel(){return nPorousVoxel;}
    int getPorousVoxelInd(int samNum){return pPorousVoxel[samNum].index;}
    int getPorousVoxelInd(int samNum, int dir){
        switch(dir){
        case 0: return pPorousVoxel[samNum].i; break;
        case 1: return pPorousVoxel[samNum].j; break;
        case 2: return pPorousVoxel[samNum].k; break;
        }
        exit(1);
    }
    double getPorousVoxelPerm(int samNum, int ind){return pPorousVoxel[samNum].K[ind];}

    void print(void);
};

class microData
{
public:
    microData(int);
    ~microData();
    void readKFlowDictFromGDR(QString, int);
    void readKFilterDictFromTXT(QString, int);
    double getPerm(int i, int j){return pPatern[i].getInflowPorousVoxelPerm(j);}
    double getPerm(int i, int j, int k){return pPatern[i].getPorousVoxelPerm(j,k);}
    void initPattern(int, int, int, int, macroData*);
    void setVelocity(macroData*);
    double getVelocity(int ind1, int ind2, int dir)
    {
        return pPatern[ind1].getFluidVoxelVelocity(ind2,dir);
    }
    int getNumOfSamples(){return sampleNum;}
    int getInflowPorousVoxelInd(int samNum, int dir){
        return pPatern[samNum].getInflowPorousVoxelInd(dir);
    }
    int getInflowPorousVoxelInd(int samNum) {
        return pPatern[samNum].getInflowPorousVoxelInd();
    }

    void print(void);
private:
    int sampleNum;
    microPattern *pPatern;
};
#endif // MICRODATA_H
