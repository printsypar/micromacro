#include "inputparam.h"

inputParam::inputParam()
{
    flParam.viscosity = 0.0124;         //
    flParam.density = 869.325;          //
    setVelocity(5);               // is used only once to define clean perm of samples
}

void inputParam::readInputXML(QString fileName)
{
    QString err;
    Xmldoc inputFile(fileName);

    bool flag = genInfo.readFromXML(inputFile, &err);

    QString str;
    if (inputFile.paramAvailable("MicroMacroCouplingInput::FluidParameters::Viscosity")) {
        str = inputFile.getParameter("MicroMacroCouplingInput::FluidParameters::Viscosity").c_str();
        flParam.viscosity = str.toDouble();
    } else {
        err += "MicroMacroCouplingInput::FluidParameters::Viscosity not specified\n" ;
        flag = false;
    }
    if (inputFile.paramAvailable("MicroMacroCouplingInput::FluidParameters::Density")) {
        str = inputFile.getParameter("MicroMacroCouplingInput::FluidParameters::Density").c_str();
        flParam.density = str.toDouble();
    } else {
        err += "MicroMacroCouplingInput::FluidParameters::Density not specified\n" ;
        flag = false;
    }
    flag = procInfo.readFromXML(inputFile, &err);

    if (!flag) *__cout << "Error occured while reading XML:" << "\n" << err.toLatin1().constData() << "\n";
    //genInfo.print();
    //printFluidParam();
    //procInfo.print();
}

void inputParam::printFluidParam(void) {
    *__cout << "<FluidParameters>:" << "\n";
    *__cout << "\t<Viscosity>:\t" << flParam.viscosity << "\n";
    *__cout << "\t<Density>:\t" << flParam.density << "\n";
}

void inputParam::createInputXML(QString fileName)
{
    QFile file(fileName);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);

    QDomDocument doc;
    QDomNode node( doc.createProcessingInstruction("xml", "version 1.0"));
    doc.setContent( QString("<MicroMacroCouplingInput/>"));
    QDomElement mainElement = doc.documentElement();

    QDomElement generalInfo = doc.createElement("GeneralInfo");
    QDomElement projDir = doc.createElement("ProjectDirectory");
    projDir.appendChild(doc.createTextNode("./"));
    generalInfo.appendChild(projDir);

    QDomElement macroInfo = doc.createElement("MacroInfo");
    QDomElement appFiltEST = doc.createElement("FiltEST");
    QDomElement license = doc.createElement("License");
    QDomElement vxl = doc.createElement("VXL");
    appFiltEST.appendChild(doc.createTextNode("./flowSim"));
    license.appendChild(doc.createTextNode("notworking.lic"));
    vxl.appendChild(doc.createTextNode("_housing.vxl"));
    macroInfo.appendChild(appFiltEST);
    macroInfo.appendChild(license);
    macroInfo.appendChild(vxl);
    generalInfo.appendChild(macroInfo);

    QDomElement microInfo = doc.createElement("MicroInfo");
    QDomElement appGeoDict = doc.createElement("GeoDict");
    QDomElement gdtDir = doc.createElement("GDT_Folder");
    appGeoDict.appendChild(doc.createTextNode("./geodict.sh"));
    gdtDir.appendChild(doc.createTextNode("./"));
    microInfo.appendChild(appGeoDict);
    microInfo.appendChild(gdtDir);
    generalInfo.appendChild(microInfo);

    mainElement.appendChild(generalInfo);

    QDomElement fluidParams = doc.createElement("FluidParameters");
    QDomElement xviscosity = doc.createElement("Viscosity");
    QDomElement xdensity = doc.createElement("Density");
    xviscosity.appendChild(doc.createTextNode(QString::number(flParam.viscosity)));
    xdensity.appendChild(doc.createTextNode(QString::number(flParam.density)));
    fluidParams.appendChild(xviscosity);
    fluidParams.appendChild(xdensity);

    mainElement.appendChild(fluidParams);

    QDomElement processData = doc.createElement("ProcessData");
    QDomElement xrunNum = doc.createElement("NumberOfRuns");
    QDomElement xtimeStep = doc.createElement("TimeStep");
    QDomElement xconcentration = doc.createElement("Concentration");
    QDomElement xdeltaK = doc.createElement("PermeabilityStoppingCreteria");
    QDomElement interpolation = doc.createElement("InterpolationMethod");
    xrunNum.appendChild(doc.createTextNode(QString::number(getRunNum())));
    xtimeStep.appendChild(doc.createTextNode(QString::number(getTimeStep())));
    xconcentration.appendChild(doc.createTextNode(QString::number(getConcentration())));
    xdeltaK.appendChild(doc.createTextNode(QString::number(getDeltaK())));
    interpolation.appendChild(doc.createTextNode("VELOCITY_BASED"));
    processData.appendChild(xrunNum);
    processData.appendChild(xtimeStep);
    processData.appendChild(xconcentration);
    processData.appendChild(xdeltaK);
    processData.appendChild(interpolation);

    QDomElement macroData = doc.createElement("MacroData");
    QDomElement flowDirectionMac = doc.createElement("FlowDirection");
    QDomElement xflowRate = doc.createElement("FlowRate");
    QDomElement wKtoFile = doc.createElement("WritePermToFile");
    QDomElement wVtoFile = doc.createElement("WriteVelocityToFile");
    flowDirectionMac.appendChild(doc.createTextNode("2"));
    xflowRate.appendChild(doc.createTextNode(QString::number(getFlowRate())));
    wKtoFile.appendChild(doc.createTextNode("true"));
    wVtoFile.appendChild(doc.createTextNode("false"));
    macroData.appendChild(flowDirectionMac);
    macroData.appendChild(xflowRate);
    macroData.appendChild(wKtoFile);
    macroData.appendChild(wVtoFile);
    processData.appendChild(macroData);

    QDomElement microData = doc.createElement("MicroData");
    QDomElement flowDirectionMic = doc.createElement("FlowDirection");
    QDomElement xinflowArea = doc.createElement("InflowArea");
    flowDirectionMic.appendChild(doc.createTextNode("2"));
    xinflowArea.appendChild(doc.createTextNode(QString::number(getInflowArea())));
    microData.appendChild(flowDirectionMic);
    microData.appendChild(xinflowArea);

    int numOfSam = 2;
    QDomElement sampleData = doc.createElement("MicroStructures");
    QDomElement numOfSamples = doc.createElement("NumberOfSamples");
    numOfSamples.appendChild(doc.createTextNode(QString::number(numOfSam)));
    sampleData.appendChild(numOfSamples);

    QDomElement *sample = new QDomElement[numOfSam];
    QDomElement *x = new QDomElement[numOfSam];
    QDomElement *y = new QDomElement[numOfSam];
    QDomElement *z = new QDomElement[numOfSam];
    for(int i=0; i<numOfSam; i++) {
        sample[i] = doc.createElement("Sample"+QString::number(i+1));
        x[i] = doc.createElement("I");
        y[i] = doc.createElement("J");
        z[i] = doc.createElement("K");
        x[i].appendChild(doc.createTextNode("0"));
        y[i].appendChild(doc.createTextNode("0"));
        z[i].appendChild(doc.createTextNode("0"));
        sample[i].appendChild(x[i]);
        sample[i].appendChild(y[i]);
        sample[i].appendChild(z[i]);
        sampleData.appendChild(sample[i]);
    }
    microData.appendChild(sampleData);

    int numOfPartTypes = 3;
    QDomElement partDictr = doc.createElement("ParticleDistribution");
    QDomElement numberOfPartTypes = doc.createElement("NumberOfParticleTypes");
    numberOfPartTypes.appendChild(doc.createTextNode(QString::number(numOfPartTypes)));
    partDictr.appendChild(numberOfPartTypes);
    QDomElement *partType = new QDomElement[numOfPartTypes];
    QDomElement *radius = new QDomElement[numOfPartTypes];
    QDomElement *densityPart = new QDomElement[numOfPartTypes];
    QDomElement *probability = new QDomElement[numOfPartTypes];
    QDomElement *adhModel = new QDomElement[numOfPartTypes];
    for(int i=0; i<numOfPartTypes; i++) {
        partType[i] = doc.createElement("ParticleType"+QString::number(i+1));
        radius[i] = doc.createElement("Radius");
        densityPart[i] = doc.createElement("Density");
        probability[i] = doc.createElement("Probability");
        adhModel[i] = doc.createElement("AdhesionModel");
        radius[i].appendChild(doc.createTextNode("0.0"));
        densityPart[i].appendChild(doc.createTextNode("0.0"));
        probability[i].appendChild(doc.createTextNode(QString::number(100/numOfPartTypes)));
        adhModel[i].appendChild(doc.createTextNode("CAUGHT_ON_FIRST_TOUCH"));
        partType[i].appendChild(radius[i]);
        partType[i].appendChild(densityPart[i]);
        partType[i].appendChild(probability[i]);
        partType[i].appendChild(adhModel[i]);
        partDictr.appendChild(partType[i]);
    }
    microData.appendChild(partDictr);

    processData.appendChild(microData);
    mainElement.appendChild(processData);

    out << doc.toString();
    file.close();\
    *__cout << "File " << fileName.toLatin1().constData() << " was written." << "\n";
}
