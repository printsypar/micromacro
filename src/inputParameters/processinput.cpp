#include "processinput.h"

processInput::processInput()
{
    runNum = 2;
    interpMethod = 0;
    timeStep = 60;
    concentration = 2e+11;
    deltaK = 0.1;
    velocity = 5;
}

bool processInput::readFromXML(Xmldoc fileName, QString *err)
{
    bool flag = true;
    Xmldoc inputFile(fileName);
    QString str1 = "MicroMacroCouplingInput::ProcessData::";
    if (inputFile.paramAvailable(str1.toStdString()+"NumberOfRuns")) {
        runNum = inputFile.getParameterWithType<int>(str1.toStdString()+"NumberOfRuns");
    } else {
        *err += str1+"NumberOfRuns not specified\n" ;
        flag = false;
    }
    if (inputFile.paramAvailable(str1.toStdString()+"TimeStep")) {
        timeStep = inputFile.getParameterWithType<double>(str1.toStdString()+"TimeStep");
    } else {
        *err += str1+"TimeStep not specified\n" ;
        flag = false;
    }
    if (inputFile.paramAvailable(str1.toStdString()+"Concentration")) {
        concentration = inputFile.getParameterWithType<double>(str1.toStdString()+"Concentration");
    } else {
        concentration = inputFile.getParameterWithType<double>(str1.toStdString()+"Concentration");
        *err += str1+"Concentration not specified\n" ;
        flag = false;
    }
    if (inputFile.paramAvailable(str1.toStdString()+"PermeabilityStoppingCreteria")) {
        deltaK = inputFile.getParameterWithType<double>(str1.toStdString()+"PermeabilityStoppingCreteria");
    } else {
        *err += str1+"PermeabilityStoppingCreteria not specified\n" ;
        flag = false;
    }
    if (inputFile.paramAvailable(str1.toStdString()+"InterpolationMethod")) {
        if (inputFile.getParameter(str1.toStdString()+"InterpolationMethod") == "CONSTANT") {
            interpMethod = CONSTANT;
        } else if (inputFile.getParameter(str1.toStdString()+"InterpolationMethod") == "LINEAR") {
            interpMethod = LINEAR;
        } else if (inputFile.getParameter(str1.toStdString()+"InterpolationMethod") == "LINEAR_WITH_DEPTH") {
            interpMethod = LINEAR_WITH_DEPTH;
        } else if (inputFile.getParameter(str1.toStdString()+"InterpolationMethod") == "VELOCITY_WEIGHTED") {
            interpMethod = VELOCITY_WEIGHTED;
        } else if (inputFile.getParameter(str1.toStdString()+"InterpolationMethod") == "VELOCITY_BASED") {
            interpMethod = VELOCITY_BASED;
        } else {
            *err += str1+"InterpolationMethod not specified\n" ;
        }
    } else {
        *err += str1+"InterpolationMethod not specified\n" ;
        flag = false;
    }

    str1 = "MicroMacroCouplingInput::ProcessData::MacroData::";
    if (inputFile.paramAvailable(str1.toStdString()+"FlowDirection")) {
        procMacInfo.setFlowDirection(inputFile.getParameterWithType<int>(str1.toStdString()+"FlowDirection"));
    } else {
        *err += str1+"FlowDirection not specified\n" ;
        flag = false;
    }
    if (inputFile.paramAvailable(str1.toStdString()+"FlowRate")) {
        procMacInfo.setFlowRate(inputFile.getParameterWithType<double>(str1.toStdString()+"FlowRate"));
    } else {
        *err += str1+"FlowRate not specified\n" ;
        flag = false;
    }
    if (inputFile.paramAvailable(str1.toStdString()+"WritePermToFile")) {
        procMacInfo.setWriteKToFile(inputFile.getParameterWithType<bool>(str1.toStdString()+"WritePermToFile"));
        //*__cout << inputFile.getParameterWithType<bool>(str1.toStdString()+"WritePermToFile");
    } else {
        *err += str1+"WritePermToFile not specified\n" ;
        flag = false;
    }
    if (inputFile.paramAvailable(str1.toStdString()+"WriteVelocityToFile")) {
        procMacInfo.setWriteVToFile(inputFile.getParameterWithType<bool>(str1.toStdString()+"WriteVelocityToFile"));
        //*__cout << inputFile.getParameterWithType<bool>(str1.toStdString()+"WriteVelocityToFile");
    } else {
        *err += str1+"WriteVelocityToFile not specified\n" ;
        flag = false;
    }

    str1 = "MicroMacroCouplingInput::ProcessData::MicroData::";
    if (inputFile.paramAvailable(str1.toStdString()+"FlowDirection")) {
        procMicInfo.setFlowDirection(inputFile.getParameterWithType<int>(str1.toStdString()+"FlowDirection"));
    } else {
        *err += str1+"FlowDirection not specified\n" ;
        flag = false;
    }
    if (inputFile.paramAvailable(str1.toStdString()+"InflowArea")) {
        procMicInfo.setInflowArea(inputFile.getParameterWithType<double>(str1.toStdString()+"InflowArea"));
    } else {
        *err += str1+"InflowArea not specified\n" ;
        flag = false;
    }

    str1 += "MicroStructures::";
    if (inputFile.paramAvailable(str1.toStdString()+"NumberOfSamples")) {
        procMicInfo.setNumOfSamples(inputFile.getParameterWithType<int>(str1.toStdString()+"NumberOfSamples"));
    } else {
        *err += str1+"NumberOfSamples not specified\n" ;
        flag = false;
    }
    int I=0, J=0, K=0;
    str1 += "Sample";
    for (int i=0; i<procMicInfo.getNumberOfSamples(); i++) {
        if (inputFile.paramAvailable(str1.toStdString()+QString::number(i+1).toStdString()+"::I")) {
            I = inputFile.getParameterWithType<int>(str1.toStdString()+QString::number(i+1).toStdString()+"::I");
        } else {
            *err += str1+QString::number(i+1)+"::I";
            flag = false;
        }
        if (inputFile.paramAvailable(str1.toStdString()+QString::number(i+1).toStdString()+"::J")) {
            J = inputFile.getParameterWithType<int>(str1.toStdString()+QString::number(i+1).toStdString()+"::J");
        } else {
            *err += str1+QString::number(i+1)+"::J";
            flag = false;
        }
        if (inputFile.paramAvailable(str1.toStdString()+QString::number(i+1).toStdString()+"::K")) {
            K = inputFile.getParameterWithType<int>(str1.toStdString()+QString::number(i+1).toStdString()+"::K");
        } else {
            *err += str1+QString::number(i+1)+"::K";
            flag = false;
        }
        if (flag) {
            procMicInfo.setSampleIndices(i,I,J,K);
        }
    }

    str1 = "MicroMacroCouplingInput::ProcessData::MicroData::ParticleDistribution::";
    if (inputFile.paramAvailable(str1.toStdString()+"NumberOfParticleTypes")) {
        procMicInfo.setNumOfParticleTypes(inputFile.getParameterWithType<int>(str1.toStdString()+"NumberOfParticleTypes"));
    } else {
        *err += str1+"NumberOfParticleTypes not specified\n" ;
    }
    double temp;
    QString stemp;
    str1 += "ParticleType";
    for (int i=0; i<procMicInfo.getNumberOfParticleTypes(); i++) {
        if (inputFile.paramAvailable(str1.toStdString()+QString::number(i+1).toStdString()+"::Radius")) {
            temp = inputFile.getParameterWithType<double>(str1.toStdString()+QString::number(i+1).toStdString()+"::Radius");
            procMicInfo.setParticleRadius(i, temp);
        } else {
            *err += str1+QString::number(i+1)+"::Radius";
        }
        if (inputFile.paramAvailable(str1.toStdString()+QString::number(i+1).toStdString()+"::Density")) {
            temp = inputFile.getParameterWithType<double>(str1.toStdString()+QString::number(i+1).toStdString()+"::Density");
            procMicInfo.setParticleDensity(i, temp);
        } else {
            *err += str1+QString::number(i+1)+"::Density";
        }
        if (inputFile.paramAvailable(str1.toStdString()+QString::number(i+1).toStdString()+"::Probability")) {
            temp = inputFile.getParameterWithType<double>(str1.toStdString()+QString::number(i+1).toStdString()+"::Probability");
            procMicInfo.setParticleProbability(i, temp);
        } else {
            *err += str1+QString::number(i+1)+"::Probability";
        }
        if (inputFile.paramAvailable(str1.toStdString()+QString::number(i+1).toStdString()+"::AdhesionModel")) {
            stemp = inputFile.getParameter(str1.toStdString()+QString::number(i+1).toStdString()+"::AdhesionModel").c_str();
            if(stemp == "CAUGHT_ON_FIRST_TOUCH")
                procMicInfo.setParticleAdhModel(i, CAUGHT_ON_FIRST_TOUCH);
        } else {
            *err += str1+QString::number(i+1)+"::AdhesionModel";
        }
    }
    return flag;
}

void processInput::print()
{
     *__cout <<"<ProcessData>:" << "\n";
     *__cout <<"\t<RunNum>:\t" << runNum << "\n";
     *__cout <<"\t<TimeStep>:\t" << timeStep << "\n";
     *__cout <<"\t<Concentr>:\t" << concentration << "\n";
     *__cout <<"\t<DeltaK>:\t" << deltaK << "\n";
     *__cout <<"\t<InterpMethod>:\t" << interpMethod << "\n";
     procMacInfo.print();
     procMicInfo.print();
}

