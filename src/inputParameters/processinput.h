#ifndef PROCESSINPUT_H
#define PROCESSINPUT_H

#include<../src/includes.h>
#include<../src/utilities.h>

struct indices
{
    int i;
    int j;
    int k;
};

struct particleTypes
{
    double radius;
    double density;
    double probability;
    int adhModel;
};

class processMacroInput
{
private:
    int flowDirection;
    double flowRate;    //[l/min]
    bool writeKToFile;
    bool writeVToFile;

public:
    processMacroInput(){
        flowDirection = 2;
        flowRate = 0.1;
        writeKToFile = false;
        writeVToFile = false;
    }

    void setFlowDirection(int _flDirection){flowDirection = _flDirection;}
    void setFlowRate(double _flowRate){flowRate = _flowRate;}
    void setWriteKToFile(bool _wKtF){writeKToFile = _wKtF;}
    void setWriteVToFile(bool _wVtF){writeVToFile = _wVtF;}
    int getFlowDirection(void){return flowDirection;}
    double getFlowRate(void){return flowRate;}
    bool getWriteKToFile(void){return writeKToFile;}
    bool getWriteVToFile(void){return writeVToFile;}

    void print(void) {
        *__cout <<"\t<MacroData>:" << "\n";
        *__cout <<"\t\t<FlowDirection>\t" << flowDirection << "\n";
        *__cout <<"\t\t<FlowRate>\t" << flowRate << "\n";
        *__cout <<"\t\t<WriteKToFile>\t" << writeKToFile << "\n";
        *__cout <<"\t\t<WriteVToFile>\t" << writeVToFile << "\n";
    }
};

class processMicroInput
{
private:
    int flowDirection;
    double inflowArea;  //[m^2]
    int numOfBatches;
    int numOfParticles;
    std::vector<indices> samples;
    std::vector<particleTypes> particleType;

public:
    processMicroInput(){
        flowDirection = 2;
        inflowArea = pow(0.00016,2.);
        samples = std::vector<indices>(2);
        numOfBatches = 2;
        numOfParticles = 0;
    }

    void setFlowDirection(int _flDirection){flowDirection = _flDirection;}
    void setInflowArea(double _inflowArea){inflowArea = _inflowArea;}
    void setNumOfBatches(int _numOfBatches){numOfBatches = _numOfBatches;}
    void setNumOfParticles(int _numOfParticles){numOfParticles = _numOfParticles;}
    void setNumOfSamples(int _numOfSam){
        samples = std::vector<indices>(_numOfSam);
    }
    void setSampleIndices(int samNum, int _i, int _j, int _k){
        samples[samNum].i=_i;
        samples[samNum].j=_j;
        samples[samNum].k=_k;
    }
    void setNumOfParticleTypes(int _numOfParticleTypes){
        particleType = std::vector<particleTypes>(_numOfParticleTypes);
    }
    void setParticleRadius(int particleTypeNum, double _radius){
        particleType[particleTypeNum].radius = _radius;
    }
    void setParticleDensity(int particleTypeNum, double _density){
        particleType[particleTypeNum].density = _density;
    }
    void setParticleProbability(int particleTypeNum, double _probability){
        particleType[particleTypeNum].probability = _probability;
    }
    void setParticleAdhModel(int particleTypeNum, int _adhModel){
        particleType[particleTypeNum].adhModel = _adhModel;
    }

    int getFlowDirection(void){return flowDirection;}
    double getInflowArea(void){return inflowArea;}
    int getNumberOfBatches(void){return numOfBatches;}
    int getNumberOfParticles(void){return numOfParticles;}
    int getNumberOfSamples(void){return samples.size();}
    int getSampleIndexI(int samNum){return samples[samNum].i;}
    int getSampleIndexJ(int samNum){return samples[samNum].j;}
    int getSampleIndexK(int samNum){return samples[samNum].k;}
    int getNumberOfParticleTypes(void){return particleType.size();}
    double getParticleRadius(int particleTypeNum){
        return particleType[particleTypeNum].radius;
    }
    double getParticleDensity(int particleTypeNum){
        return particleType[particleTypeNum].density;
    }
    double getParticleProbability(int particleTypeNum){
        return particleType[particleTypeNum].probability;
    }
    int getParticleAdhModel(int particleTypeNum){
        return particleType[particleTypeNum].adhModel;
    }
    void increaseNumOfBatches(void){numOfBatches++;}

    void print(void) {
        *__cout <<"\t<MicroData>:" << "\n";
        *__cout <<"\t\t<FlowDirection>\t" << flowDirection << "\n";
        *__cout <<"\t\t<InflowArea>\t" << inflowArea << "\n";
        *__cout <<"\t\t<NumOfSamples>\t" << samples.size() << "\n";
        for (int i=0; i<(int)samples.size(); i++)
            *__cout <<"\t\t\t<sample " << i+1 << ">: " << samples[i].i << " " << samples[i].j << " " << samples[i].k << "\n";
        *__cout <<"\t\t<NumOfParticleTypes>\t" << particleType.size() << "\n";
        for (int i=0; i<(int)particleType.size(); i++){
            *__cout <<"\t\t\t<particleType_" << i+1 << ">" << "\n";
            *__cout <<"\t\t\t\t<Radius>\t" << particleType[i].radius << "\n";
            *__cout <<"\t\t\t\t<Density>\t" << particleType[i].density << "\n";
            *__cout <<"\t\t\t\t<Probabiltiy>\t" << particleType[i].probability << "\n";
            *__cout <<"\t\t\t\t<AdhModel>\t" << particleType[i].adhModel << "\n";
        }
    }
};

class processInput
{
private:
    int runNum;
    int interpMethod;
    double timeStep;        //[s]
    double concentration;   //[1/m^3]
    double deltaK;          //[-] stopping creteria for FilterDict
    double velocity;        //[m/s]

    processMacroInput procMacInfo;
    processMicroInput procMicInfo;

public:
    processInput();

    bool readFromXML(Xmldoc, QString*);

    void setVelocity(double _velocity){
        velocity = fabs(_velocity);
        setNumOfParticles();
    }
    void setNumberOfBatches(int _numOfBatches){procMicInfo.setNumOfBatches(_numOfBatches);}
    void setNumOfParticles(void){
        procMicInfo.setNumOfParticles((int)(fabs(timeStep*concentration*getVelocity()*getInflowArea())));
        if (procMicInfo.getNumberOfParticles()==0) procMicInfo.setNumOfParticles(5);
    }
    int getRunNumber(void){return runNum;}
    int getInterpolationMethod(void){return interpMethod;}
    double getTimeStep(void){return timeStep;}
    double getConcentration(void){return concentration;}
    double getPermStopCreteria(void){return deltaK;}
    double getVelocity(void){return velocity;}

    int getMacroFlowDirection(void){return procMacInfo.getFlowDirection();}
    double getFlowRate(void){return procMacInfo.getFlowRate();}
    bool getWriteKToFile(void){return procMacInfo.getWriteKToFile();}
    bool getWriteVToFile(void){return procMacInfo.getWriteVToFile();}

    int getMicroFlowDirection(void){return procMicInfo.getFlowDirection();}
    double getInflowArea(void){return procMicInfo.getInflowArea();}
    int getNumOfSamples(void){return procMicInfo.getNumberOfSamples();}
    int getNumOfBatches(void){return procMicInfo.getNumberOfBatches();}
    int getNumOfParticles(void){return procMicInfo.getNumberOfParticles();}
    void increaseNumOfgBatches(void){procMicInfo.increaseNumOfBatches();}
    int getSampleIndexI(int samNum){return procMicInfo.getSampleIndexI(samNum);}
    int getSampleIndexJ(int samNum){return procMicInfo.getSampleIndexJ(samNum);}
    int getSampleIndexK(int samNum){return procMicInfo.getSampleIndexK(samNum);}
    int getNumberOfParticleTypes(void){return procMicInfo.getNumberOfParticleTypes();}
    double getParticleRadius(int n){return procMicInfo.getParticleRadius(n);}
    double getParticleDensity(int n){return procMicInfo.getParticleDensity(n);}
    double getParticleProbability(int n){return procMicInfo.getParticleProbability(n);}
    int getParticleAdhModel(int n){return procMicInfo.getParticleAdhModel(n);}

    void print(void);
};

#endif // PROCESSINPUT_H
