#ifndef INPUTPARAM_H
#define INPUTPARAM_H

#include<../src/includes.h>
#include<../src/inputParameters/generalinput.h>
#include<../src/inputParameters/processinput.h>

struct fluidParam
{
    double viscosity;   //[kg/m/s]
    double density;     //[kg/m^3]
};

class inputParam
{
    ///!!!!! INFLOW AREA HAS TO BE INCLUDED IN ORDER TO CONTROL TIME AND CONCENTRATION
private:
    generalInput genInfo;
    fluidParam flParam;
    processInput procInfo;

public:
    inputParam();

    // Fluid Parameters
    void setViscosity(double _visc){flParam.viscosity = _visc;}
    void setDensity(double _dens){flParam.density = _dens;}
    double getViscosity(void){return flParam.viscosity;}
    double getDensity(void){return flParam.density;}

    void setVelocity(double _vel){procInfo.setVelocity(_vel);}
    void setNumOfBatches(int n){procInfo.setNumberOfBatches(n);}
    void increaseNumOfBatches(void){procInfo.increaseNumOfgBatches();}

    double getFlowRate(void){return procInfo.getFlowRate();}
    double getConcentration(void){return procInfo.getConcentration();}
    double getTimeStep(void){return procInfo.getTimeStep();}
    int getRunNum(void){return procInfo.getRunNumber();}
    double getDeltaK(void){return procInfo.getPermStopCreteria();}
    double getInflowArea(void){return procInfo.getInflowArea();}
    double getVelocity(void){return procInfo.getVelocity();}

    void readInputXML(QString);
    void printFluidParam(void);
    void createInputXML(QString);

    generalInput getGeneralInfo(void){return genInfo;}
    fluidParam getFluidParam(void){return flParam;}
    processInput* getProcessInfo(void){return &procInfo;}
};

#endif // INPUTPARAM_H
