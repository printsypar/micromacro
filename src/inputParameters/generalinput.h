#ifndef GENERALINPUT_H
#define GENERALINPUT_H

#include<../src/includes.h>
#include<../src/utilities.h>

class generalMacroInput
{
private:
    QFile *appFiltEST;
    QFile *licenseFiltEST;
    QFile *vxl;

public:
    generalMacroInput(){
        appFiltEST = new QFile("");
        licenseFiltEST = new QFile("");
        vxl = new QFile("");
    }

    void setAppFiltEST(QString _appFiltEST) {appFiltEST->setFileName(_appFiltEST);}
    void setLicenseFiltEST(QString _licenseFiltEST) {licenseFiltEST->setFileName(_licenseFiltEST);}
    void setVXL(QString _vxl){vxl->setFileName(_vxl);}

    QString getAppFiltEST(void) {return appFiltEST->fileName();}
    QString getLicenseFiltEST(void) {return licenseFiltEST->fileName();}
    QString getVXL(void) {return vxl->fileName();}

    void print(void) {
        *__cout << "\t<MacroInfo>:" << "\n";
        *__cout << "\t\t" << appFiltEST->fileName().toLatin1().constData() << "\n";
        *__cout << "\t\t" << licenseFiltEST->fileName().toLatin1().constData() << "\n";
        *__cout << "\t\t" << vxl->fileName().toLatin1().constData() << "\n";
    }
};



class generalMicroInput
{
private:
    QFile *appGeoDict;
    QDir gdtDir;

public:
    generalMicroInput(){
        appGeoDict = new QFile("");
    }
    void setAppGeoDict(QString _appGeoDict){
        appGeoDict->setFileName(_appGeoDict);
    }
    void setGDTDir(QString _gdtDir){gdtDir.setPath(_gdtDir);}

    QString getAppGeoDict(void){return appGeoDict->fileName();}
    QString getGDTDir(void){return gdtDir.absolutePath();}

    void print(void) {
        *__cout << "\t<MicroInfo>:" << "\n";
        *__cout << "\t\t" << appGeoDict->fileName().toLatin1().constData() << "\n";
        *__cout << "\t\t" << gdtDir.absolutePath().toLatin1().constData() << "\n";
    }
};



class generalInput
{
private:
    QDir projectDir;
    generalMacroInput generalMacInp;
    generalMicroInput generalMicInp;

public:
    generalInput();

    bool readFromXML(Xmldoc, QString*);

    QString getProjectDir(void){return projectDir.absolutePath();}
    QString getAppFilEST(void){return generalMacInp.getAppFiltEST();}
    QString getLicenseFiltEST(void){return generalMacInp.getLicenseFiltEST();}
    QString getVXL(void){return generalMacInp.getVXL();}
    QString getAppGeoDict(void){return generalMicInp.getAppGeoDict();}
    QString getGDTDir(void){return generalMicInp.getGDTDir();}
    void print(void){
        *__cout << "<GeneralInput>:" << "\n";
        *__cout << "\t<ProjectDir>:\t" << projectDir.absolutePath().toLatin1().constData() << "\n";
        generalMacInp.print();
        generalMicInp.print();
    }
};

#endif // GENERALINPUT_H
