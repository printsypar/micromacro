#include "generalinput.h"

generalInput::generalInput()
{
}

bool generalInput::readFromXML(Xmldoc fileName, QString *err)
{
    bool flag = true;
    Xmldoc inputFile(fileName);
    QString str;
    if (inputFile.paramAvailable("MicroMacroCouplingInput::GeneralInfo::ProjectDirectory")) {
        str = inputFile.getParameter("MicroMacroCouplingInput::GeneralInfo::ProjectDirectory").c_str();
        projectDir.setPath(str);
    } else {
        *err += "MicroMacroCouplingInput::GeneralInfo::ProjectDirectory not specified\n" ;
        flag = false;
    }

    if (inputFile.paramAvailable("MicroMacroCouplingInput::GeneralInfo::MacroInfo::FiltEST")) {
        str = inputFile.getParameter("MicroMacroCouplingInput::GeneralInfo::MacroInfo::FiltEST").c_str();
        generalMacInp.setAppFiltEST(str);
    } else {
        *err += "MicroMacroCouplingInput::GeneralInfo::MacroInfo::FiltEST not specified\n" ;
        flag = false;
    }
    if (inputFile.paramAvailable("MicroMacroCouplingInput::GeneralInfo::MacroInfo::License")) {
        str = inputFile.getParameter("MicroMacroCouplingInput::GeneralInfo::MacroInfo::FiltEST").c_str();
        generalMacInp.setLicenseFiltEST(str);
    } else {
        *err += "MicroMacroCouplingInput::GeneralInfo::MacroInfo::License not specified\n" ;
        flag = false;
    }
    if (inputFile.paramAvailable("MicroMacroCouplingInput::GeneralInfo::MacroInfo::VXL")) {
        str = inputFile.getParameter("MicroMacroCouplingInput::GeneralInfo::MacroInfo::VXL").c_str();
        generalMacInp.setVXL(str);
    } else {
        *err += "MicroMacroCouplingInput::GeneralInfo::MacroInfo::VXL not specified\n" ;
        flag = false;
    }

    if (inputFile.paramAvailable("MicroMacroCouplingInput::GeneralInfo::MicroInfo::GeoDict")) {
        str = inputFile.getParameter("MicroMacroCouplingInput::GeneralInfo::MicroInfo::GeoDict").c_str();
        generalMicInp.setAppGeoDict(str);
    } else {
        *err += "MicroMacroCouplingInput::GeneralInfo::MicroInfo::GeoDict not specified\n" ;
        flag = false;
    }
    if (inputFile.paramAvailable("MicroMacroCouplingInput::GeneralInfo::MicroInfo::GDT_Folder")) {
        str = inputFile.getParameter("MicroMacroCouplingInput::GeneralInfo::MicroInfo::GDT_Folder").c_str();
        generalMicInp.setGDTDir(str);
    } else {
        *err += "MicroMacroCouplingInput::GeneralInfo::MicroInfo::GDT_Folder not specified\n" ;
        flag = false;
    }
    return flag;
}
