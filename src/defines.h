#ifndef DEFINES_H
#define DEFINES_H

#define MAC_NAME_XML "FiltEST.xml"
#define MAC_NAME_SOL "saved.sol"
#define MAC_NAME_K "savedK.sol"

#define PROJ_NAME_SUB "run"

#define START_FROM_BEGINNING 0
#define K_RECOMPUTE 1

#define CAUGHT_ON_FIRST_TOUCH 0

#define POSITIVE true
#define NEGATIVE false

// INTERPOLATION TYPES
#define CONSTANT 0
#define LINEAR 1
#define LINEAR_WITH_DEPTH 2
#define VELOCITY_WEIGHTED 3
#define VELOCITY_BASED 4


#define DBG_OUT_STP(str) cout << str << endl; cin.ignore();
#define DBG_OUT(str) cout << str << endl;
#define DBG_STP cout << "press any key to continue..." << endl; cin.ignore();
#define OUT_PROGRESS(num, tot) cout << "\r" << num << " of " << tot << "          " << flush;
//DBGOUT_STP(ind==50, "Value" << parameter);

#define MYASSERT(cnd, msg) {if(!(cnd)) {\
    cout << "Assert failed in: " << __FUNCTION__ << ":" << __FILE__ << endl;\
    cout << msg << endl; exit(1);}}


#endif // DEFINES_H
