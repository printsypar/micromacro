#include "xmldocReader.h"
#include <stdlib.h>
#include <QFile>
#include <QStringList>


bool Xmldoc::openFile(QFile & file) {
    if (!file.exists())
        return false;
    file.open(QIODevice::ReadWrite);
	file.setTextModeEnabled(true);
	return true;
}



Xmldoc::Xmldoc(QString filename) {
    //string s = filename.toStdString();
    //cout<<"Filename = "<<filename.toLatin1().constData()<<endl;
    QFile file(filename);
    //cout << "Opening File!"<<endl;

	
	if (!openFile(file)) {
        cerr << "Could not open file " << filename.toStdString() << endl;
		exit(1);
		
    }
    parseToDomDocument(file, xmlDocument);
}

void Xmldoc::print(ostream & out ){
    QString qs;
    qs = xmlDocument.toString();
    string s = qs.toStdString();
    out<<s.c_str()<<endl;
}

bool Xmldoc::parseToDomDocument(QFile &file, QDomDocument &document) {
    //cout << "parseToDomDocument"<<endl;
	if (!document.setContent(&file)) {
		cout << "document is not ready to be parsed!"<<endl;
        file.close();
		exit(1);
        return false;
    }
    file.close();
    //cout << "XML Document is ok. Ready for use. " << endl;
    return true;
}

bool Xmldoc::paramAvailable(string paramPath){
     QStringList pathTokens = QString(paramPath.c_str()).split("::");
      string empty("");

    if (pathTokens.size() == 0) {
        cout << "Empty parameter path given, could not retrieve parameter!" << endl;
        exit(1);
        return false;
    }
    if (pathTokens.size() == 1) {
        cout << "Parameter path too short, only document node given!" << endl;
        exit(1);
        return false;
    }

    QString documentToken = pathTokens.first();
    pathTokens.removeFirst();
    vector<QDomNodeList> resultNodes;
    QDomNode documentElement = xmlDocument.documentElement();
    resultNodes.push_back(getParameterNodesByPath(documentElement, pathTokens));

    for (unsigned int resultsIdx = 0; resultsIdx < resultNodes.size(); resultsIdx++) {
        if (!resultNodes[resultsIdx].isEmpty()) {
            QDomNode resultNode = resultNodes[resultsIdx].item(0);
            QDomText textNode = resultNode.firstChild().toText();
            if (textNode.isNull()) {
                return false;
            } else {
                return true;
            }
        }
    }

    return false; /*This is to avoid compiler warning*/
}

string Xmldoc::getParameter(string paramPath) {
    QStringList pathTokens = QString(paramPath.c_str()).split("::");

    string empty("");

    if (pathTokens.size() == 0) {
        cout << "Empty parameter path given, could not retrieve parameter!" << endl;
        exit(1);
        return empty;
    }
    if (pathTokens.size() == 1) {
        cout << "Parameter path too short, only document node given!" << endl;
        exit(1);
        return empty;
    }

  //  QString documentToken = pathTokens.first();
    pathTokens.removeFirst();
    vector<QDomNodeList> resultNodes;
    QDomNode documentElement = xmlDocument.documentElement();
    resultNodes.push_back(getParameterNodesByPath(documentElement, pathTokens));

    for (unsigned int resultsIdx = 0; resultsIdx < resultNodes.size(); resultsIdx++) {
        if (!resultNodes[resultsIdx].isEmpty()) {
            QDomNode resultNode = resultNodes[resultsIdx].item(0);
            QDomText textNode = resultNode.firstChild().toText();
            if (textNode.isNull()) {
                cout << "Parameter node is not a final element, probably too short path!" << endl;
                cout << "Path given: " << paramPath << endl;
                abort();
                return empty; /*This is to avoid compiler warning*/
            } else {
                return textNode.data().toStdString();
            }
        }
    }
    cout << "Parameter with path " << paramPath << " could not be located!" << endl;
    exit(1);
    return empty; /*This is to avoid compiler warning*/
}

void Xmldoc::setParameter(string paramPath, QDomText &value) {
    QStringList pathTokens = QString(paramPath.c_str()).split("::");


    if (pathTokens.size() == 0) {
        cout << "Empty parameter path given, could not retrieve parameter!" << endl;
        exit(1);

    }
    if (pathTokens.size() == 1) {
        cout << "Parameter path too short, only document node given!" << endl;
        exit(1);

    }

  //  QString documentToken = pathTokens.first();
    pathTokens.removeFirst();
    vector<QDomNodeList> resultNodes;
    QDomNode documentElement = xmlDocument.documentElement();
    resultNodes.push_back(getParameterNodesByPath(documentElement, pathTokens));

    for (unsigned int resultsIdx = 0; resultsIdx < resultNodes.size(); resultsIdx++) {
        if (!resultNodes[resultsIdx].isEmpty()) {
            QDomNode resultNode = resultNodes[resultsIdx].item(0);
            QDomText textNode = resultNode.firstChild().toText();
            if (textNode.isNull()) {
                cout << "Parameter node is not a final element, probably too short path!" << endl;
                cout << "Path given: " << paramPath << endl;
                abort();

            }
            else {
                resultNode.replaceChild(value, textNode);
                xmlDocument.replaceChild(value, textNode);
            }
        }
    }
   // cout << "Parameter with path " << paramPath << " could not be located!" << endl;
   // exit(1);

}


QDomNodeList Xmldoc::getParameterNodesByPath(QDomNode node, QStringList pathTokens) {
    QString currentToken = pathTokens.first();
    pathTokens.removeFirst();
    QDomNodeList emptyList;

    QDomNodeList currentNodeList = node.toElement().elementsByTagName(currentToken);

    if (pathTokens.size() == 0) {
        return currentNodeList;
    } else {
        QDomNodeList childNodeList;
        /*Iterate over all nodes with the current token and find the children
         *with the next token*/
        for (int nodeIdx = 0; nodeIdx < currentNodeList.size(); nodeIdx++) {
            childNodeList = getParameterNodesByPath(currentNodeList.item(nodeIdx), pathTokens);
            if (!childNodeList.isEmpty()) return childNodeList;
        }
    }
    return emptyList;
}
