#ifndef _xmldocReader_h_
#define _xmldocReader_h_

#include <QtXml/QDomDocument>
#include <QtXml/QDomNode>
#include <QtXml/QDomNodeList>
#include <sstream>
#include<string>
#include<vector>
#include<typeinfo>
#include<iostream>
using namespace std;

/**
 * @file xmldoc.h
 * @brief A class for reading of xml documents.
 * Very important to know that recently this class uses xml-library which comes from Qt.
 * At least the version 4.6 is needed.
 **/


/*! \brief A class for reading of xml documents
 * Complete xml-document is saved as xml-Dom-document
 * At each moment if it is necessary the data with keywords can be obtained
 */
class QFile;

class QStringList;


class Xmldoc {
private:
    QDomDocument xmlDocument;
    bool openFile(QFile & file);
public:
    /*! \brief A constructor of the class.
     * @param filename should contain the name of the xml-file which should be read.
     *
     */
    Xmldoc(QString filename);
	
    /*! \brief Read the xml-file in to document
     * @param file is of type QFile and corresponds to the  xml-file which has to be read.
     * @param document is a internal representation (QDomDoclument)of the xml-document
     */
    bool parseToDomDocument(QFile &file, QDomDocument &document);
    /*! \brief Get a parameter if the path to the parameter is given
     * The used separator between keywords is ::
     * Example      BoundaryConditions::Pressure::Inlet::Value
     * \param paramPath is the complete path with separators "::"
     * \return string which contains the parameter          */
    string getParameter(string paramPath);
    void setParameter(string paramPath, QDomText &value);
    void print(ostream & out = cout);
    QDomDocument* getDomDocument(){return &xmlDocument;}

    /*! \brief Check if the parameter the given path exists in the document
     * @param paramPath is the complete path with separators "::"
     * @return true if parameter exists and false in other case
     */
    bool paramAvailable(string paramPath);
    QDomNodeList getParameterNodesByPath(QDomNode node, QStringList pathTokens);

    template<class T>
    T convertParameterFromString(const std::string& parameter) {

        if (typeid (T) == typeid (bool)){
//            return (QString(parameter.c_str()).contains("true", Qt::CaseInsensitive) == true);
        }
        std::istringstream stream(parameter);
        T convertedParameter;
        stream >> convertedParameter;
        return convertedParameter;
    }

    template<class T>
    T getParameterWithType(std::string parameterPath) {
        return convertParameterFromString<T > (getParameter(parameterPath));
    }

};


#endif


