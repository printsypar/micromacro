#ifndef MACROINPUT_H
#define MACROINPUT_H

#include<../src/includes.h>
#include<../src/inputParameters/inputparam.h>

class macroInput
{
    //friend class permHandler;
public:
    macroInput();
    ~macroInput();
    void setVXL(QString);
    void copyFiles(QString);
    void modifyXml(int);
    void copySolFiles(QString, QString);
    void createXMLforFiltEST(QString, inputParam*);
    QString getCurrentXML(void){return currentXML.fileName();}

private:
    QFile sourceXML;
    QFile sourceVXL;
    QFile currentXML;
    QFile currentVXL;
};

#endif // MACROINPUT_H
