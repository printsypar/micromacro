#ifndef PROJSTRUCTURE_H
#define PROJSTRUCTURE_H

#include<../src/includes.h>
#include<../src/inputParameters/inputparam.h>
#include<../src/projStructure/macroinput.h>
#include<../src/projStructure/microinput.h>

class projStructure
{
public:
    projStructure();
    ~projStructure();
    void initialize(inputParam*);
    void setProjDir(QString);
    void setNewRun();
    void setParamsForFilterDictSingleRun(int gdtNum, inputParam *params){
        micInput.setParamsForFilterDictSingleRun(   getMicroDir(),
                                                    getMicroGDT(gdtNum,
                                                                params->getProcessInfo()->getNumOfBatches(),
                                                                true),
                                                    gdtNum,
                                                    params);
    }
    void setParamsForFilterDict(int gdtNum, int prevNumOfBatches, inputParam* params){
        micInput.setParamsForFilterDict(    getMicroDir(),
                                            getMicroGDT(gdtNum, prevNumOfBatches, startNewRun),
                                            startNewRun,
                                            gdtNum,
                                            params);
    }

    void prepareInputForMacro(int);
    void createInputFiles(inputParam *params){
        micInput.createGMCforFilterDict(projDir.absolutePath(), params);
        macInput.createXMLforFiltEST(projDir.absolutePath(), params);
    }
    QString getMacroXmlFile(){
        return macInput.getCurrentXML();
    }
    QString getMacroDir(){
        return currentProjDir.absolutePath()+QDir::separator()+"macro";
    }
    QString getMacroPrevDir();
    QString getMacroLog();
    QString getMacroKFile();
    QString getMacroVPFile();
    QString getMicroInputFile(void){
        return micInput.getGMC();
    }
    QString getMicroGDT(int, int, bool);
    QString getMicroDir(){
        return currentProjDir.absolutePath()+QDir::separator()+"micro";
    }
    QString getMicroPrevDir();
    QString getMicroLog(int);
    QString getMicroResultTXT(int j, int _numOfBatches){
        QString str = getMicroDir()+QDir::separator()+"FilterDictSinglePassResult";
        if (j>0) str += "_no"+QString::number(j);
        str += QDir::separator();
        int numOfBatches;
        if (_numOfBatches == 1) numOfBatches = 1;
        else numOfBatches = _numOfBatches+1;
        if (numOfBatches < 10)
        {
            str += "Batch0000";
        } else {
            str += "Batch000";
        }
        str = str + QString::number(numOfBatches) + QDir::separator() + "FlowResults.txt";
        return str;
    }
    QString getProjDir(void){return projDir.absolutePath();}

    void setStartNewRun(bool _var){startNewRun = _var;}
    bool getStartNewRun(void){return startNewRun;}

    int getSizeOfGDT(void) {
        return micInput.getGDTNum();
    }
private:
    bool startNewRun;
    int runNum;
    QDir projDir;
    QDir currentProjDir;
    macroInput macInput;
    microInput micInput;
};

#endif // PROJSTRUCTURE_H
