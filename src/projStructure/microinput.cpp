#include "microinput.h"

microInput::microInput()
{
    gdtNum = 0;
    gmcNum = 0;
    gdtFiles = NULL;
    gmcFilterDict = NULL;
}

microInput::~microInput()
{
    if (gdtFiles != NULL) delete []gdtFiles;
}

void microInput::setGDTs(QStringList _gdtFiles)
{
    gdtNum = _gdtFiles.size();
    if (gdtFiles != NULL) delete []gdtFiles;
    gdtFiles = new QFile[gdtNum];
    for (int i=0; i<gdtNum; i++)
    {
        if (!QFile::exists(_gdtFiles.at(i)))
        {
            *__cout << "input file " << _gdtFiles.at(i).toLatin1().constData() << "does not exist" << "\n";
            exit(1);
        }
        gdtFiles[i].setFileName(_gdtFiles.at(i));
    }
}

void microInput::createGMCforFilterDict(QString path, inputParam* params)
{
    if (gmcFilterDict != NULL) delete gmcFilterDict;
    gmcFilterDict = new QFile(path+QDir::separator()+"FilterDict.gmc");
    if (!gmcFilterDict->open(QIODevice::WriteOnly|QIODevice::Text))
    {
        *__cout << "Could not open file " << (path+QDir::separator()+"FilterDict.gmc").toLatin1().constData() << "!temp" << "\n";
        exit(1);
    }
    QTextStream out(gmcFilterDict);
    out << "<Header>\n";
    out << "  Release      2012R2\n";
    out << "  Revision     12047\n";
    out << "  BuildDate    11 Jan 2013\n";
    out << "  CreationDate 25 Feb 2013\n";
    out << "  CreationTime 15:48:56\n";
    out << "  Creator      printsg\n";
    out << "  Platform     64 bit Linux\n";
    out << "</Header>\n";
    out << "\n";
    out << "<Description>\n";
    out << "Automatically generated Macro file\n";
    out << "for Micro-Macro-Coupling for GeoDict 2012R2\n";
    out << "recorded ---\n";
    out << "by printsg of KAUST\n";
    out << "</Description>\n";
    out << "\n";
    out << "<Macro>\n";
    out << "\n";
    out << "GeoDict:Preferences {\n";
    out << "  SpecifyThreadNumber       false\n";
    out << "  NumberOfThreads           2\n";
    out << "  <Gad>\n";
    out << "    UseXMLStyle             true\n";
    out << "    GadInMemory             true\n";
    out << "    UseDiameter             true\n";
    out << "  </Gad>\n";
    out << "  <IO>\n";
    out << "    WriteDbgPrintfToConsole true\n";
    out << "    WriteDbgPrintfToLogfile true\n";
    out << "  </IO>\n";
    out << "  DiskSpaceOption           Standard\n";
    out << "  CompressOption            Keep Originals\n";
    out << "}\n";
    out << "\n";
    out << "GeoDict:ChangeProjectFolder {\n";
    out << "  FolderName  %%%\n";
    out << "  CreateIfNotPresent false\n";
    out << "}\n";
    out << "\n";
    out << "GeoDict:LoadFile {\n";
    out << "  FileName  %%%\n";
    out << "}\n";
    out << "\n";
    out << "FilterDict:SinglePass {\n";
    out << "  <ProcessData>\n";
    out << "    Temperature                         293.15\n";
    out << "    FlowDirection                       ";
    out << params->getProcessInfo()->getMicroFlowDirection() << "\n";
    out << "    Density                             ";
    out << params->getFluidParam().density << "\n";
    out << "    Viscosity                           ";
    out << params->getFluidParam().viscosity << "\n";
    out << "    PressureDropIncreaseValue           100000\n";
    out << "    BatchesPerFlowField                 1\n";
    out << "    UseEStatic                          false\n";
    out << "    ElectrostaticSurfaceCharge          1e-06\n";
    out << "    ElectrostaticParticleCharge         1e-06\n";
    out << "    ParticlesPerBatch                   000\n";
    out << "    Velocity                            000\n";
    out << "    NumberOfBatchesValue                000\n";
    out << "    ParticleConcentration               ";
    out << params->getProcessInfo()->getConcentration() << "\n";
    out << "  </ProcessData>\n";
    out << "  <ParticleDistribution>\n";
    out << "    NumberOfParticleTypes               ";
    out << params->getProcessInfo()->getNumberOfParticleTypes() << "\n";
    for (int i=0; i<params->getProcessInfo()->getNumberOfParticleTypes(); i++) {
        out << "    <ParticleType" << i+1 << ">\n";
        out << "      Radius                            ";
        out << params->getProcessInfo()->getParticleRadius(i) << "\n";
        out << "      Density                           ";
        out << params->getProcessInfo()->getParticleDensity(i) << "\n";
        out << "      Probability                       ";
        out << params->getProcessInfo()->getParticleProbability(i) << "\n";
        out << "      Material                          14\n";
        out << "      AdhesionModel                     ";
        if (params->getProcessInfo()->getParticleAdhModel(i)==0)
            out << "CAUGHT_ON_FIRST_TOUCH\n";
        out << "    </ParticleType" << i+1 << ">\n";
    }
    out << "  </ParticleDistribution>\n";
    out << "  <SolverData>\n";
    out << "    <Parallelization>\n";
    out << "      Mode                              LOCAL_MPI\n";
    out << "      NumberOfProcesses                 4\n";
    out << "    </Parallelization>\n";
    out << "    <FlowSolver>\n";
    out << "      NumberOfProcesses                 4\n";
    out << "    </FlowSolver>\n";
    out << "    <FilterSolver>\n";
    out << "      NumberOfProcesses                 4\n";
    out << "    </FilterSolver>\n";
    out << "    <EStaticSolver>\n";
    out << "      NumberOfProcesses                 4\n";
    out << "    </EStaticSolver>\n";
    out << "    InitialFlowPDE                      STOKES\n";
    out << "    IterativeFlowPDE                    STOKES\n";
    out << "    InitialFlowSolver                   EJ\n";
    out << "    IterativeFlowSolver                 EJ\n";
    out << "    <VolumeFractions>\n";
    out << "      RhoMin                            0.05\n";
    out << "      RhoMax                            0.7\n";
    out << "      SigmaMax                          7336000000\n";
    out << "      InitialValuesFromFile             0\n";
    out << "      InitialValuesFileName\n";
    out << "      InitialValuesFromVoxelPerm        0\n";
    out << "    </VolumeFractions>\n";
    out << "    <InitialFlowField>\n";
    out << "      DiffusionTest                     false\n";
    out << "      RestartComputation                false\n";
    out << "      FileName\n";
    out << "      RestartFileName\n";
    out << "    </InitialFlowField>\n";
    out << "    <IterativeFlowFields>\n";
    out << "      InitializeComputationWithPrevious 0\n";
    out << "    </IterativeFlowFields>\n";
    out << "    <FlowSolverSettings>\n";
    out << "      <EJ>\n";
    out << "        Accuracy                        0.0001\n";
    out << "        MaxNumberOfIterations           100000\n";
    out << "        MaximalSolverRunTime            240\n";
    out << "        RestartSaveIntervalTime         6\n";
    out << "        SlipLength                      0\n";
    out << "        SolverType                      0\n";
    out << "        StoppingCriterion               0\n";
    out << "        CheckInterval                   100\n";
    out << "      </EJ>\n";
    out << "      <LB>\n";
    out << "        Accuracy                        0.0001\n";
    out << "        MaxNumberOfIterations           100000\n";
    out << "        ScalingValue                    -1.9\n";
    out << "      </LB>\n";
    out << "      <EFV>\n";
    out << "        Accuracy                        0.0001\n";
    out << "        MaxNumberOfIterations           100000\n";
    out << "        MaximalSolverRunTime            240\n";
    out << "        RestartSaveIntervalTime         6\n";
    out << "        SolverType                      0\n";
    out << "        StoppingCriterion               0\n";
    out << "        CheckInterval                   100\n";
    out << "        SlipLength                      0\n";
    out << "        RelaxationPressure              0.8\n";
    out << "        RelaxationVelocity              0.00480966\n";
    out << "      </EFV>\n";
    out << "    </FlowSolverSettings>\n";
    out << "    <Permeabilities>\n";
    out << "      Color1                            0,0,0\n";
    out << "      Color2                            0,0,0\n";
    out << "      Color3                            0,0,0\n";
    out << "      Color4                            0,0,0\n";
    out << "      Color5                            0,0,0\n";
    out << "      Color6                            0,0,0\n";
    out << "      Color7                            0,0,0\n";
    out << "      Color8                            0,0,0\n";
    out << "      Color9                            0,0,0\n";
    out << "      Color10                           0,0,0\n";
    out << "      Color11                           0,0,0\n";
    out << "      Color12                           0,0,0\n";
    out << "      Color13                           0,0,0\n";
    out << "      Color14                           0,0,0\n";
    out << "      Color15                           0,0,0\n";
    out << "    </Permeabilities>\n";
    out << "    BrownianMotion                      true\n";
    out << "    WriteCollisionPoints                false\n";
    out << "    UseFilterUDF                        false\n";
    out << "    FilterUDFFileName\n";
    out << "    InitialParticlePosition             1e-06\n";
    out << "    ReadParticleInitialValuesFromFile   false\n";
    out << "    ReadParticleInitialValuesFileName\n";
    out << "    ResultFileName                      %%%\n";
    out << "    WritePTLogFile                      true\n";
    out << "    TrajectoryPrecision                 0.5\n";
    out << "    <ExpertSettings>\n";
    out << "      RandomSeed                        123\n";
    out << "      timeStepScaling                   1\n";
    out << "      maxNumberOfTimeSteps              0\n";
    out << "      particlePositioningTrials         1\n";
    out << "      velocityInterpolation             1\n";
    out << "      DirichletBoundaryOffset           50\n";
    out << "    </ExpertSettings>\n";
    out << "  </SolverData>\n";
    out << "ContinuePreviousRun                   false\n";
    out << "</Macro>\n";
    gmcFilterDict->close();
}

void microInput::setParamsForFilterDictSingleRun(QString microDir, QString microGDT, int gdtNum, inputParam *params)
{
    QString inputFileName=getGMC();
    *__cout << ">>>>> WRITING GMC for FilterDictSingleRun" << "\n";
    QFile file(inputFileName);
    if (!file.open(QIODevice::ReadOnly|QIODevice::Text))
    {
        *__cout << "Could not open file " << inputFileName.toLatin1().constData() << "\n";
        exit(1);
    }
    QTextStream in(&file);

    QFile fileNew(inputFileName+"!temp");
    if (!fileNew.open(QIODevice::WriteOnly|QIODevice::Text))
    {
        *__cout << "Could not open file" << inputFileName.toLatin1().constData() << "!temp" << "\n";
        exit(1);
    }
    QTextStream out(&fileNew);

    bool velocityFound=false;
    while (!in.atEnd())
    {
        QString line = in.readLine();
        if (line.contains("GeoDict:ChangeProjectFolder"))
        {
            out << line << "\n";
            line = in.readLine();
            line = "  FolderName  "+microDir;
            *__cout << "  SETTING PRO: " << line.toLatin1().constData() << "\n";
        }
        if (line.contains("GeoDict:LoadFile"))
        {
            out << line << "\n";
            line = in.readLine();
            line = "  FileName  "+microGDT;
            *__cout << "  SETTING GDT: " << line.toLatin1().constData() << "\n";
        }
        if (line.contains("ParticlesPerBatch"))
        {
            line.replace(QRegExp("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?"),QString::number(1));
            *__cout << "  SETTING ###: " << line.toLatin1().constData() << "\n";
        }
        if (!velocityFound && line.contains("Velocity"))
        {
            line.replace(QRegExp("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?"),QString::number(params->getVelocity()));
            *__cout <<"  SETTING ###: " << line.toLatin1().constData() << "\n";
            velocityFound = true;
        }
        if (line.contains("NumberOfBatchesValue"))
        {
            line.replace(QRegExp("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?"),QString::number(1));
            *__cout <<"  SETTING ###: " << line.toLatin1().constData() << "\n";
        }
        if (line.contains("RestartComputation"))
        {
            line = "      RestartComputation                false";
            *__cout <<"  SETTING ###: " << line.toLatin1().constData() << "\n";
        }
        if (line.contains("RestartFileName"))
        {
            line = "      RestartFileName                   ";
            *__cout << "  SETTING EJR: " << line.toLatin1().constData() << "\n";
        }
        if (line.contains("ResultFileName"))
        {
            line = "    ResultFileName                      FilterDictSinglePassResult";
            if (gdtNum > 0) line += "_no"+QString::number(gdtNum);
            line += ".gdr";
            *__cout << "  SETTING GDR: " << line.toLatin1().constData() << "\n";
        }
        if (line.contains("ContinuePreviousRun"))
        {
            line = "  ContinuePreviousRun                   false";
            *__cout << "  SETTING ###: " << line.toLatin1().constData() << "\n";
        }
        /*if (!densityFound && line.contains("Density"))
        {
            line.replace(QRegExp("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?"),QString::number(params->getDensity()));
            densityFound = true;
        }
        if (line.contains("Viscosity"))
        {
            line.replace(QRegExp("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?"),QString::number(params->getViscosity()));
        }*/
        /*if (line.contains("ParticleConcentration"))
        {
            line.replace(QRegExp("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?"),QString::number(params->getConcentration()));
        }*/
        out << line << "\n";
    }
    file.close();
    QFile::remove(inputFileName);

    if (!fileNew.rename(inputFileName))
    {
        *__cout << "File " << inputFileName.toLatin1().constData() << " was not renamed" << "\n";
        exit(1);
    }
    fileNew.close();
    *__cout << "<<<<< Done" << "\n";
}

void microInput::setParamsForFilterDict(QString microDir, QString microGDT, bool startNewRun, int gdtNum, inputParam* params)
{
    *__cout << ">>>>> WRITING GMC for FilterDict" << "\n";
    QString inputFileName=getGMC();
    QFile file(inputFileName);
    if (!file.open(QIODevice::ReadOnly|QIODevice::Text))
    {
        *__cout << "Could not open file " << inputFileName.toLatin1().constData() << "\n";
        exit(1);
    }
    QTextStream in(&file);

    QFile fileNew(inputFileName+"!temp");
    if (!fileNew.open(QIODevice::WriteOnly|QIODevice::Text))
    {
        *__cout << "Could not open file " << inputFileName.toLatin1().constData() << "!temp" << "\n";
        exit(1);
    }
    QTextStream out(&fileNew);

    bool velFound=false;
    while (!in.atEnd())
    {
        QString line = in.readLine();
        if (line.contains("GeoDict:ChangeProjectFolder"))
        {
            out << line << "\n";
            line = in.readLine();
            line = "  FolderName  "+microDir;
            *__cout << "  SETTING PRO: " << line.toLatin1().constData() << "\n";
        }
        else if (line.contains("GeoDict:LoadFile"))
        {
            out << line << "\n";
            line = in.readLine();
            line = "  FileName  "+microGDT;
            *__cout << "  SETTING GDT: " << line.toLatin1().constData() << "\n";
        }
        else if (line.contains("RestartComputation"))
        {
            if (!startNewRun) {
                line = "      RestartComputation                true";
            } else {
                line = "      RestartComputation                false";
            }
            *__cout << "  SETTING ###: " << line.toLatin1().constData() << "\n";
        }
        else if (line.contains("RestartFileName"))
        {
            line = "      RestartFileName                   ";
            QString str = microGDT; //proj->getMicroGDT(gdtNum, prevNumOfBatches, startNewRun);
            str.replace("geometry.gdt","moments.ejr");
            if (!startNewRun) line += str;
            *__cout << "  SETTING EJR: " << line.toLatin1().constData() << "\n";
        }
        else if (line.contains("ParticlesPerBatch"))
        {
            line.replace(QRegExp("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?"),QString::number(params->getProcessInfo()->getNumOfParticles()));
            *__cout << "  SETTING ###: " << line.toLatin1().constData() << "\n";
        }
        else if (line.contains("Velocity") && !velFound)
        {
            line.replace(QRegExp("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?"),QString::number(params->getVelocity()));
            velFound=true;
        }
        else if (line.contains("NumberOfBatchesValue"))
        {
            if (startNewRun) { params->setNumOfBatches(2);}
            line.replace(QRegExp("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?"),QString::number(params->getProcessInfo()->getNumOfBatches()));
            *__cout << "  SETTING ###: " << line.toLatin1().constData() << "\n";
        }
        else if (line.contains("ResultFileName"))
        {
            line = "    ResultFileName                      FilterDictSinglePassResult";
            if (gdtNum > 0) line += "_no"+QString::number(gdtNum);
            line += ".gdr";
            *__cout << "  SETTING GDR: " << line.toLatin1().constData() << "\n";
        }
        else if (line.contains("ContinuePreviousRun"))
        {
            if(params->getProcessInfo()->getNumOfBatches()==2)
                line = "  ContinuePreviousRun                   false";
            else
                line = "  ContinuePreviousRun                   true";
            *__cout << "  SETTING ###: " << line.toLatin1().constData() << "\n";
        }
        else if (line.contains("RandomSeed"))
        {
            line = "      RandomSeed                        ";
            line += QString::number(rand()%256+1);
            //DBG_OUT_STP(line.toLatin1().data())
        }

        out << line << "\n";
    }
    file.close();
    QFile::remove(inputFileName);

    if (!fileNew.rename(inputFileName))
    {
        *__cout << "File " << inputFileName.toLatin1().constData() << " was not renamed" << "\n";
        exit(1);
    }
    fileNew.close();
    *__cout << "<<<<< Done" << "\n";
}
