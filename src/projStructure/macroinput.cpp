#include "macroinput.h"

macroInput::macroInput()
{
}

macroInput::~macroInput()
{
}

void macroInput::setVXL(QString fileName)
{
   if (!QFile::exists(fileName))
   {
       *__cout << "input file " << fileName.toLatin1().constData() << " does not exist! " << "\n";
       exit(1);
   }
   sourceVXL.setFileName(fileName);
   currentVXL.setFileName(fileName);
}

void macroInput::copyFiles(QString projDir)
{
    QString str;
    str = projDir + QDir::separator();
    str += MAC_NAME_XML;
    QFileInfo src(sourceXML);
    QFileInfo dest(str);
    if(src.absoluteFilePath().compare(dest.absoluteFilePath()) == 0)
    {
        //same file, no need to copy
        *__cout << "Warning! File " << src.absoluteFilePath().toLatin1().constData() << " already exists" << "\n";
    }
    else
    {
        if (QFile::exists(str))
        {
            QFile::remove(str);
            *__cout << "Warning! File " << str.toLatin1().constData() << " was overwritten" << "\n";
        }
        if (!QFile::copy(sourceXML.fileName(), str)) exit(1);
            currentXML.setFileName(str);
    }
    //printf("output: %s\n",currentFiles[i].fileName().toLatin1().constData());
    //printf("cpy: %s\n",dest.absoluteFilePath().toLatin1().constData());
    //printf("src: %s\n",src.absoluteFilePath().toLatin1().constData());
}

void macroInput::modifyXml(int i)
{
    Xmldoc inputFile(currentXML.fileName());
    QDomDocument *doc = inputFile.getDomDocument();
    QDomText projectPathTextNode = doc->createTextNode(currentXML.fileName());
    inputFile.setParameter("ProjectInfo::GeneralInfo::ProjectPath", projectPathTextNode);
    if (!inputFile.paramAvailable("ProjectInfo::GeneralInfo::ProjectPath")) {
        *__cout <<"ProjectInfo::GeneralInfo::ProjectPath is unavailable. *.xml input file is buggy " ;
        exit(1);
    }
    QFileInfo fi(currentXML.fileName());
    QDomText outputPathTextNode = doc->createTextNode(fi.absolutePath());
    inputFile.setParameter("ProjectInfo::GeneralInfo::OutputPath", outputPathTextNode );
    if (!inputFile.paramAvailable("ProjectInfo::GeneralInfo::OutputPath")){
        *__cout <<"ProjectInfo::GeneralInfo::OutputPath is unavailable. *.xml input file is buggy " ;
        exit(1);
    }
    QDomText startOptionsTextNode;
    if (i==0)
    {
        startOptionsTextNode = doc->createTextNode("Start-from-beginning");
    }
    else
    {
        startOptionsTextNode = doc->createTextNode("K-recompute");
    }
    inputFile.setParameter("ProjectInfo::StartSaveOptions::StartOptions::Restart", startOptionsTextNode );
    if (!inputFile.paramAvailable("ProjectInfo::StartSaveOptions::StartOptions::Restart")){
        *__cout <<"ProjectInfo::StartSaveOptions::StartOptions::Restart is unavailable. *.xml input file is buggy " ;
        exit(1);
    }
    QFile file(currentXML.fileName());
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    out << inputFile.getDomDocument()->toString();
    file.close();
}

void macroInput::copySolFiles(QString projPrevDir, QString projDir)
{
    QString strPrevDir, strDir;
    //printf("source: %s\n",sourceFiles[i].fileName().toLatin1().constData());
    strPrevDir = projPrevDir + QDir::separator();
    strDir = projDir + QDir::separator();
    strPrevDir += MAC_NAME_K;
    strDir += MAC_NAME_K;
    QFileInfo src(strPrevDir);
    QFileInfo dest(strDir);
    if(src.absoluteFilePath().compare(dest.absoluteFilePath()) == 0)
        {
            //same file, no need to copy
        *__cout << ">>>>> Warning! File " << src.absoluteFilePath().toLatin1().constData() << " already exists <<<<< Done" << "\n";
        }
        else
        {
            if (QFile::exists(strDir))
            {
                QFile::remove(strDir);
                *__cout << ">>>>>> Warning! File " << strDir.toLatin1().constData() << " was overwritten <<<<< Done" << "\n";
            }
            if (!QFile::copy(strPrevDir, strDir)) exit(1);
        }
}

void macroInput::createXMLforFiltEST(QString path, inputParam *params)
{
    sourceXML.setFileName(path+QDir::separator()+MAC_NAME_XML);
    currentXML.setFileName(path+QDir::separator()+MAC_NAME_XML);
    sourceXML.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&sourceXML);

    QDomDocument doc;
    QDomNode node( doc.createProcessingInstruction("xml", "version 1.0"));
    doc.setContent( QString("<ProjectInfo/>"));
    QDomElement mainElement = doc.documentElement();

    QDomElement generalInfo = doc.createElement("GeneralInfo");
    QDomElement projName = doc.createElement("ProjectName");
    //QDomElement version = doc.createElement("Version");
    QDomElement dimension = doc.createElement("Dimension");
    QDomElement license = doc.createElement("LicenseFile");
    QDomElement projPath = doc.createElement("ProjectPath");
    QDomElement geomFormat = doc.createElement("GeometryFormat");
    QDomElement geomPath = doc.createElement("GeometryPath");
    QDomElement levels = doc.createElement("Levels");
    QDomElement outputPath = doc.createElement("OutputPath");
    QDomElement units = doc.createElement("Units");
    projName.appendChild(doc.createTextNode("MiMaCoupling"));
    //version.appendChild(doc.createTextNode("1.0"));
    dimension.appendChild(doc.createTextNode("3"));
    license.appendChild(doc.createTextNode("***.lic"));
    projPath.appendChild(doc.createTextNode(currentXML.fileName()));
    geomFormat.appendChild(doc.createTextNode("VXL"));
    geomPath.appendChild(doc.createTextNode(currentVXL.fileName()));
    levels.appendChild(doc.createTextNode("1"));
    QFileInfo fi(currentXML.fileName());
    outputPath.appendChild(doc.createTextNode(fi.absolutePath()));
    units.appendChild(doc.createTextNode("SI Units"));
    //generalInfo.appendChild(version);
    generalInfo.appendChild(projName);
    generalInfo.appendChild(dimension);
    generalInfo.appendChild(license);
    generalInfo.appendChild(projPath);
    generalInfo.appendChild(geomFormat);
    generalInfo.appendChild(geomPath);
    generalInfo.appendChild(levels);
    generalInfo.appendChild(outputPath);
    generalInfo.appendChild(units);
    mainElement.appendChild(generalInfo);

    QDomElement moduleParams = doc.createElement("ModuleParams");
    QDomElement moduleName = doc.createElement("Name");
    //QDomElement equation = doc.createElement("Equation");
    QDomElement parallelFlag = doc.createElement("ParallelFlag");
    QDomElement numOfProc = doc.createElement("NumberOfProcesses");
    //equation.appendChild(doc.createTextNode("Flow with Local Permeability"));
    moduleName.appendChild(doc.createTextNode("Flow Only"));
    parallelFlag.appendChild(doc.createTextNode("Sequential"));
    numOfProc.appendChild(doc.createTextNode("1"));
    moduleParams.appendChild(moduleName);
    moduleParams.appendChild(parallelFlag);
    moduleParams.appendChild(numOfProc);
    mainElement.appendChild(moduleParams);

    QDomElement housingInfo = doc.createElement("HousingInfo");
    QDomElement numOfMedia = doc.createElement("NumOfMedia");
    QDomElement numOfMesh = doc.createElement("NumOfMesh");
    QDomElement numOfInlets = doc.createElement("NumOfInlets");
    QDomElement numOfOutlets = doc.createElement("NumOfOutlets");
    numOfMedia.appendChild(doc.createTextNode("1"));
    numOfMesh.appendChild(doc.createTextNode("0"));
    numOfInlets.appendChild(doc.createTextNode("1"));
    numOfOutlets.appendChild(doc.createTextNode("1"));
    QDomElement inlet0 = doc.createElement("Inlet0");
    QDomElement flowRate = doc.createElement("FlowRate");
    flowRate.appendChild(doc.createTextNode(QString::number(params->getFlowRate())));
    inlet0.appendChild(flowRate);
    QDomElement outlet0 = doc.createElement("Outlet0");
    QDomElement outpressure = doc.createElement("Pressure");
    outpressure.appendChild(doc.createTextNode("0.0"));
    outlet0.appendChild(outpressure);
    QDomElement symmetryForWalls = doc.createElement("UseSymmetryForWalls");
    symmetryForWalls.appendChild(doc.createTextNode("false"));
    housingInfo.appendChild(numOfMedia);
    housingInfo.appendChild(numOfMesh);
    housingInfo.appendChild(numOfInlets);
    housingInfo.appendChild(numOfOutlets);
    housingInfo.appendChild(inlet0);
    housingInfo.appendChild(outlet0);
    housingInfo.appendChild(symmetryForWalls);
    mainElement.appendChild(housingInfo);

    QDomElement fluidParams = doc.createElement("FluidParameters");
    QDomElement fluidName = doc.createElement("Name");
    QDomElement viscosity = doc.createElement("Viscosity");
    //QDomElement flowRate = doc.createElement("FlowRate");
    QDomElement density = doc.createElement("Density");
    fluidName.appendChild(doc.createTextNode("myFluid"));
    viscosity.appendChild(doc.createTextNode(QString::number(params->getViscosity())));
    //flowRate.appendChild(doc.createTextNode(QString::number(params->getFlowRate())));
    density.appendChild(doc.createTextNode(QString::number(params->getDensity())));
    fluidParams.appendChild(fluidName);
    fluidParams.appendChild(viscosity);
    //fluidParams.appendChild(flowRate);
    fluidParams.appendChild(density);
    mainElement.appendChild(fluidParams);

    QDomElement filterMediaParam = doc.createElement("FilterMediaParameters");
    QDomElement media0 = doc.createElement("Media0");
    QDomElement mediaName = doc.createElement("Name");
    QDomElement mediaType = doc.createElement("Type");
    QDomElement perm0 = doc.createElement("Permeability");
    mediaName.appendChild(doc.createTextNode("myMediaName"));
    mediaType.appendChild(doc.createTextNode("Porous"));
    perm0.appendChild(doc.createTextNode("1.0e-10"));
    media0.appendChild(mediaName);
    media0.appendChild(mediaType);
    media0.appendChild(perm0);
    filterMediaParam.appendChild(media0);
    mainElement.appendChild(filterMediaParam);

    QDomElement solverOptions = doc.createElement("SolverOptions");
    QDomElement equation = doc.createElement("Equation");
    QDomElement nonDarcyScheme = doc.createElement("NonDarcyElement");
    equation.appendChild(doc.createTextNode("NSB"));
    nonDarcyScheme.appendChild(doc.createTextNode("ND_JacSpl"));
    solverOptions.appendChild(equation);
    solverOptions.appendChild(nonDarcyScheme);
    QDomElement timeSteppingParams = doc.createElement("TimeSteppingParams");
    QDomElement timestep = doc.createElement("Timestep");
    QDomElement maxTimeSteps = doc.createElement("MaxTimesteps");
    QDomElement epsDt = doc.createElement("EpsDt");
    QDomElement numIterInfoSaved = doc.createElement("NumIterInfoSaved");
    QDomElement epsDp = doc.createElement("EpsDp");
    timestep.appendChild(doc.createTextNode("1.e-02"));
    maxTimeSteps.appendChild(doc.createTextNode("1000"));
    epsDt.appendChild(doc.createTextNode("1.e-04"));
    numIterInfoSaved.appendChild(doc.createTextNode("5"));
    epsDp.appendChild(doc.createTextNode("0.001"));
    timeSteppingParams.appendChild(timestep);
    timeSteppingParams.appendChild(maxTimeSteps);
    timeSteppingParams.appendChild(epsDt);
    timeSteppingParams.appendChild(numIterInfoSaved);
    timeSteppingParams.appendChild(epsDp);
    solverOptions.appendChild(timeSteppingParams);
    QDomElement discrParams = doc.createElement("DiscretizationParams");
    QDomElement convScheme = doc.createElement("ConvectiveScheme");
    QDomElement averType = doc.createElement("AveragingType");
    convScheme.appendChild(doc.createTextNode("Upwind"));
    averType.appendChild(doc.createTextNode("Arithmetic"));
    discrParams.appendChild(convScheme);
    discrParams.appendChild(averType);
    solverOptions.appendChild(discrParams);
    QDomElement linearSolverOptions = doc.createElement("LinearSolverOptions");
    QDomElement solvertype = doc.createElement("Solvertype");
    QDomElement maxVelIter = doc.createElement("MaxVelocityIterations");
    QDomElement maxPCIter = doc.createElement("MaxPCIterations");
    QDomElement epsVel = doc.createElement("EpsVel");
    QDomElement epsPc = doc.createElement("EpsPc");
    solvertype.appendChild(doc.createTextNode("SAMG"));
    maxVelIter.appendChild(doc.createTextNode("100"));
    maxPCIter.appendChild(doc.createTextNode("100"));
    epsVel.appendChild(doc.createTextNode("1.e-08"));
    epsPc.appendChild(doc.createTextNode("1.e-08"));
    linearSolverOptions.appendChild(solvertype);
    linearSolverOptions.appendChild(maxVelIter);
    linearSolverOptions.appendChild(maxPCIter);
    linearSolverOptions.appendChild(epsVel);
    linearSolverOptions.appendChild(epsPc);
    solverOptions.appendChild(linearSolverOptions);
    mainElement.appendChild(solverOptions);

    QDomElement startSaveOptions = doc.createElement("StartSaveOptions");
    QDomElement startOptions = doc.createElement("StartOptions");
    QDomElement restart = doc.createElement("Restart");
    restart.appendChild(doc.createTextNode("Start-from-beginning"));
    startOptions.appendChild(restart);
    startSaveOptions.appendChild(startOptions);
    QDomElement saveOptions = doc.createElement("SaveOptions");
    QDomElement saveStep = doc.createElement("SaveStep");
    QDomElement fileFormat = doc.createElement("FileFormat");
    QDomElement fileType = doc.createElement("FileType");
    QDomElement seeRes = doc.createElement("SeeRes");
    QDomElement writePerm = doc.createElement("WritePermeability");
    saveStep.appendChild(doc.createTextNode("1"));
    fileFormat.appendChild(doc.createTextNode("VTU"));
    fileType.appendChild(doc.createTextNode("BINARY"));
    seeRes.appendChild(doc.createTextNode("false"));
    writePerm.appendChild(doc.createTextNode("true"));
    saveOptions.appendChild(saveStep);
    saveOptions.appendChild(fileFormat);
    saveOptions.appendChild(fileType);
    saveOptions.appendChild(seeRes);
    saveOptions.appendChild(writePerm);
    startSaveOptions.appendChild(saveOptions);
    mainElement.appendChild(startSaveOptions);

    QDomElement result = doc.createElement("Result");
    QDomElement pressureDrop = doc.createElement("PressureDrop");
    pressureDrop.appendChild(doc.createTextNode("0.0"));
    result.appendChild(pressureDrop);
    mainElement.appendChild(result);

    out << doc.toString();
    sourceXML.close();
}
