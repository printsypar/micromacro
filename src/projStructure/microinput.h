#ifndef MICROINPUT_H
#define MICROINPUT_H

#include<stdlib.h>
#include<time.h>

#include<../src/includes.h>
#include<../src/utilities.h>
#include<../src/inputParameters/inputparam.h>

class microInput
{
public:
    microInput();
    ~microInput();
    void setGDTs(QStringList);
    QString getGDT(int i){return gdtFiles[i].fileName();}
    void createGMCforFilterDict(QString, inputParam*);
    QString getGMC(void){return gmcFilterDict->fileName();}
    int getGDTNum(void){return gdtNum;}

    void setParamsForFilterDictSingleRun(QString, QString, int, inputParam*);
    void setParamsForFilterDict(QString, QString, bool, int, inputParam*);
private:
    int gdtNum;
    int gmcNum;
    QFile *gdtFiles;
    QFile *gmcFilterDict;
};

#endif // MICROINPUT_H
