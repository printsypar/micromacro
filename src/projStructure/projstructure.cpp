#include "projstructure.h"

projStructure::projStructure()
{
    runNum = 0;
}

projStructure::~projStructure()
{
}

void projStructure::initialize(inputParam *params)
{
    //QString projectPath = "/u/p/printsyp/micro-macro-coupling/tests/SShaped_1";
    //QString projectPath = "/home/printsg/workspace/Tests/Temp5";

    // FiltEST input files
    //QString exeFiltEST = "/home/printsg/workspace/FiltEST/branches/bin/flowSim_backup";
    //QStringList macroFile(projectPath+QDir::separator()+"flowInput_SIUnits.xml");
    //macroFile << projectPath+QDir::separator()+"_housing.vxl";

    // GeoDict input files
    //QString exeGeoDict = "/u/p/printsyp/geodict2012R1.sh";
    //QString exeGeoDict = "/home/printsg/GeoDict2012R2-Rev12047-Linux-64-glibc2.15/geodict.sh";

    //QDir gdtDir("/home/printsg/workspace/Tests/Temp5");
    QDir gdtDir(params->getGeneralInfo().getGDTDir());
    QString fileName("*.gdt");
    QStringList gdtFiles = gdtDir.entryList(QStringList(fileName),
                                      QDir::Files | QDir::NoSymLinks);
    if (gdtFiles.size()==0) {
        *__cout << "No gdt files were found";
        exit(1);
    }
    for (int i=0; i<gdtFiles.size(); i++) {
        gdtFiles.replace(i,gdtDir.absolutePath()+QDir::separator()+gdtFiles.at(i));
    }
    setProjDir(params->getGeneralInfo().getProjectDir());
    macInput.setVXL(params->getGeneralInfo().getVXL());
    micInput.setGDTs(gdtFiles);
    setNewRun();
    createInputFiles(params);
}

void projStructure::setProjDir(QString projDirName)
{
    QFileInfo fi(projDirName);
    if (fi.isRelative()) projDirName = fi.absoluteFilePath();
    if (!projDir.mkpath(projDirName)) exit(1);
    projDir.setPath(projDirName);
    currentProjDir.setPath(projDirName);
}

void projStructure::setNewRun()
{
    runNum++;
    // creating new folders
    QString subProjDir(projDir.absolutePath()+QDir::separator()+PROJ_NAME_SUB);
    if (runNum < 10)
    {
        subProjDir += "00";
    }
    else if (runNum < 100)
    {
        subProjDir += "0";
    }
    subProjDir += QString::number(runNum);

    if (!currentProjDir.mkpath(subProjDir+QDir::separator()+"macro")) exit(1);
    if (!currentProjDir.mkpath(subProjDir+QDir::separator()+"micro")) exit(1);
    currentProjDir.setPath(subProjDir);
}

void projStructure::prepareInputForMacro(int i)
{
    // copying input files for new step
    macInput.copyFiles(getMacroDir());
    macInput.modifyXml(i);
    if (runNum > 1)
    {
        macInput.copySolFiles(getMacroPrevDir(),getMacroDir());
    }
}

QString projStructure::getMacroPrevDir()
{
    if (runNum > 1)
    {
        QString subProjDir(projDir.absolutePath()+QDir::separator()+PROJ_NAME_SUB);
        if (runNum < 10)
        {
            subProjDir += "00";
        }
        else if (runNum < 100)
        {
            subProjDir += "0";
        }
        subProjDir += QString::number(runNum-1);
        subProjDir += QDir::separator();
        subProjDir += "macro";
        return subProjDir;
    }
    else
    {
        return "";
    }
}

QString projStructure::getMacroLog()
{
    return getMacroDir()+QDir::separator()+"output.log";
}
QString projStructure::getMacroKFile()
{
    return getMacroDir()+QDir::separator()+MAC_NAME_K;
}
QString projStructure::getMacroVPFile()
{
    return getMacroDir()+QDir::separator()+MAC_NAME_SOL;
}
QString projStructure::getMicroGDT(int i, int numOfBatches, bool newRun)
{
    if (runNum > 1)
    {
        QString str;
        if (newRun){
            str = getMicroPrevDir()+QDir::separator()+"FilterDictSinglePassResult";
        } else {
            str = getMicroDir()+QDir::separator()+"FilterDictSinglePassResult";
        }
        if (i>0) str += "_no"+QString::number(i);
        str += QDir::separator();
        numOfBatches += 1;
        if (numOfBatches < 10)
        {
            str += "Batch0000";
        } else {
            str += "Batch000";
        }
        str = str + QString::number(numOfBatches) + QDir::separator() + "geometry.gdt";
        return str;
    }
    else
    {
        return micInput.getGDT(i);
    }
}

QString projStructure::getMicroPrevDir()
{
    if (runNum > 1)
    {
        QString subProjDir(projDir.absolutePath()+QDir::separator()+PROJ_NAME_SUB);
        if (runNum < 10)
        {
            subProjDir += "00";
        }
        else if (runNum < 100)
        {
            subProjDir += "0";
        }
        subProjDir += QString::number(runNum-1);
        subProjDir += QDir::separator();
        subProjDir += "micro";
        return subProjDir;
    }
    else
    {
        return "";
    }
}

QString projStructure::getMicroLog(int j)
{
    return getMicroDir()+QDir::separator()+"output_"+QString::number(j)+".log";
}
