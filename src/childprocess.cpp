#include "childprocess.h"

childProcess::childProcess(QString _program)
{
    //process = new QProcess(this);
    program = _program;
    args.clear();
}
childProcess::~childProcess()
{
    //delete process;
}
int childProcess::run(QString _output)
{
    *__cout << ">>>>> " << processName.toLatin1().constData() << " is running..." << "\n";
    *__cout << "  (for more details see " << _output.toLatin1().constData() << ")" << "\n";
    setStandardOutputFile(_output);
    //QProcess *console = new QProcess();
    //process->setStandardOutputProcess(console);
    //process->setStandardOutputProcess(console);
    //printf("%s\n",_output.toLatin1().constData());
    //printf("%s\n",program.toLatin1().constData());
    //printf("%s\n",args.at(0).toLatin1().constData());
    //console->start("");
    start(program, args);
    //console.start()
    waitForFinished(-1);

    *__cout << "<<<<< ";

    if (exitCode() == 0)
    {
        *__cout << processName.toLatin1().constData() << " executed successfully!" << "\n";
        return 0;
    }
    else
    {
        *__cout << processName.toLatin1().constData() << " failed!" <<  "\n";
        return exitCode();
    }
}

void childProcess::removeFiles(QString dirName, QString fileName)
{
    QDir currentMacroDir(dirName);
    QStringList files;
    files = currentMacroDir.entryList(QStringList(fileName),
                                      QDir::Files | QDir::NoSymLinks);
    for (int j=0; j<files.size(); j++)
    {
        //printf("%s\n",currentMacroDir.absoluteFilePath(files[j]).toLatin1().constData());
        QFile::remove(currentMacroDir.absoluteFilePath(files[j]));
    }
}
