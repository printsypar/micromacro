#ifndef MATRIX_H
#define MATRIX_H
//-----------------------------------------------------------------------------
#include<math.h>
#include<stdio.h>
#include<string.h>
#include<vector>

#include "errorhandler.h"

//-----------------------------------------------------------------------------
class MATRIX {

private:

    int   nEl[3];	//nEl[0] number of columns
                    //nEl[1] number of rows
    std::vector<double>  data;

public:

    MATRIX();
    MATRIX(const int *_nEl);
    MATRIX(const int *_nEl, const double val);
    MATRIX(const int *_nEl, const double *val);
    MATRIX(const MATRIX& m);

    MATRIX &operator = (const MATRIX &);
    MATRIX operator + (const MATRIX &)const;
    MATRIX operator + (const double val);
    MATRIX& operator += (const MATRIX& m);
    MATRIX operator - (const MATRIX &)const;
    MATRIX operator - (const double val)const;
    MATRIX& operator -= (const MATRIX& m);
    MATRIX operator * (const double val)const;
    MATRIX operator * (const MATRIX& m);

    bool operator == (MATRIX& m);
    bool operator != (MATRIX& m);

    double determinant2D22(void)const;
    double determinant2D33(void)const;
    double determinant2D44(void)const;
    double determinant2D(void)const;

    MATRIX Inverse2D22(void)const;
    MATRIX Inverse2D33(void)const;
    MATRIX Inverse2D44(void)const;
    MATRIX Inverse2D(void)const;

    void SetNEl(const int  _nEl);
    void SetNEl(const int  *_nEl);
    void SetNEl(const int  Nx, const int  Ny, const int  Nz);

    void SetData(double val);
    void SetData(const double *val);
    void SetData(const int  elInd, double val);
    void SetData(const int *elInd, double val);
    void SetData(int x, int y, int z, double val);

    int GetNEl(const int dir) const;
    int GetNEl() const;
    const int* GetNElCmp() const;

    const std::vector<double>&  GetData() const;
    double GetData(const int  elInd) const;
    double GetData(const int *elInd) const;
    double GetData(int x, int y, int z) const;

    void  print(void) const;
    void  printToFile(char*) const;

};//class MATRIX
//-----------------------------------------------------------------------------
#endif
