#include<fstream>

#include<../src/includes.h>
#include<../src/utilities.h>
#include<../src/childprocess.h>
#include<../src/projStructure/projstructure.h>
#include<../src/projStructure/macroinput.h>
#include<../src/projStructure/microinput.h>
#include<../src/inputParameters/inputparam.h>
#include<../src/processData/processdata.h>
#include<../src/processData/macrodata.h>
#include<../src/processData/microdata.h>

int checkInput(int argc, char *argv[]);

Tee* __cout;

int main(int argc, char *argv[])
{
    inputParam params;

    int option = checkInput(argc, argv);
    if (option == -1) exit(1);
    else if (option == 0) return 0;
    else if (option == 1) { params.createInputXML(argv[2]); return 0;}

    params.readInputXML(QString(argv[argc-1])); // reading input parameters

    projStructure struc;
    struc.initialize(&params);

    QString outputFileName(struc.getProjDir()+QDir::separator()+"simulation.log");
    std::ofstream outfile(outputFileName.toLatin1().data());
    __cout = new Tee(std::cout, outfile);

    int gdtNum = struc.getSizeOfGDT();
    processData procData(gdtNum);

    int permDir = -1;
    switch (params.getProcessInfo()->getMicroFlowDirection()) {
    case 0: permDir = 0; break;
    case 1: permDir = 4; break;
    case 2: permDir = 8; break;
    }
    int velDir = params.getProcessInfo()->getMacroFlowDirection();

    childProcess FiltEST(params.getGeneralInfo().getAppFilEST());
    childProcess GeoDict(params.getGeneralInfo().getAppGeoDict());
    FiltEST.setProcessName("FiltEST");
    GeoDict.setProcessName("GeoDict");


    /// Running simulaitons ...
    std::vector<double> initK(gdtNum);
    int prevNumOfBatches=0;
    for (int i=0; i<params.getProcessInfo()->getRunNumber(); i++)
    {
        /// MICRO SIMULATIONS
        if (i==0)
        {
            // FilterDict single run
            for (int j=0; j<gdtNum; j++) {
                struc.setParamsForFilterDictSingleRun(j, &params);
                GeoDict.setArgs(QStringList()<<struc.getMicroInputFile());
                GeoDict.run(struc.getMicroLog(j));
                procData.getMicroData()->readKFilterDictFromTXT(struc.getMicroResultTXT(j,1), j);
            }
        }
        else
        {
            // FilterDict run
            struc.setStartNewRun(true);
            for (int j=0; j<gdtNum; j++)
                initK[j] = procData.getMicroData()->getPerm(j,permDir);
            bool stop=false;
            while(!stop) {
                for (int j=0; j<gdtNum; j++) {
                    params.setVelocity(procData.getMicroData()->getVelocity(j,0,velDir));
                    struc.setParamsForFilterDict(j, prevNumOfBatches, &params);
                    GeoDict.setArgs(QStringList()<<struc.getMicroInputFile());
                    //if (i>1) //!!!!!!!!!!!!!
                    GeoDict.run(struc.getMicroLog(j));
                    procData.getMicroData()->readKFilterDictFromTXT(
                                struc.getMicroResultTXT(j,params.getProcessInfo()->getNumOfBatches()), j);
                }
                *__cout << "Permeability change after "
                       << params.getProcessInfo()->getNumOfBatches() << " batches ("
                       << procData.getTime()+params.getProcessInfo()->getNumOfBatches()*params.getTimeStep() << " s):" << "\n";
                for (int j=0; j<gdtNum; j++) {
                    double DK = procData.getMicroData()->getPerm(j,permDir);
                    DK = fabs((DK-initK[j])/initK[j]);
                    if (DK > params.getDeltaK()) stop = true;
                    *__cout << "  Sample Number #" << j << ":\t" << DK*100. << " %" << "\n";
                }

                prevNumOfBatches = params.getProcessInfo()->getNumOfBatches();
                if (stop != true) {
                    params.increaseNumOfBatches();
                } else {
                    procData.setTime(params.getProcessInfo()->getNumOfBatches()*params.getProcessInfo()->getTimeStep());
                    *__cout << "FilterDict stopped." << "\n";
                }
                struc.setStartNewRun(false);
            }
        }

        /// MACRO SIMULATIONS
        if (i==0)
        {
            // if i==0 we need an artificial run to produce files
            // with permeability and geometry
            struc.prepareInputForMacro(START_FROM_BEGINNING);
            procData.getMacroData()->writeKToFile(  struc.getMacroXmlFile(),
                                                    procData.getMicroData()->getPerm(0,permDir));
            FiltEST.setArgs(QStringList()<<struc.getMacroXmlFile());
            FiltEST.run(struc.getMacroLog());
            FiltEST.removeFiles(struc.getMacroDir()+QDir::separator()+"VisualizationOutput","*.vtu");


            // initialize macro permeability and geometry
            procData.getMacroData()->readKFromFile(struc.getMacroKFile());
            procData.getMacroData()->readVPFromFile(struc.getMacroVPFile());

            // initializing indices for micro patterns
            for (int j=0; j<gdtNum; j++)
                procData.getMicroData()->initPattern(   j,
                                                        params.getProcessInfo()->getSampleIndexI(j),
                                                        params.getProcessInfo()->getSampleIndexJ(j),
                                                        params.getProcessInfo()->getSampleIndexK(j),
                                                        procData.getMacroData());
        }

        /// PERMEABILITY INTERPOLATION
        switch(params.getProcessInfo()->getInterpolationMethod()) {
        case CONSTANT:
            procData.getMacroData()->setPerm(procData.getMicroData()->getPerm(0,permDir));
            break;
        case LINEAR:
            procData.getMacroData()->setPermLinearInterpolation(procData.getMicroData(), 1);
            break;
        case LINEAR_WITH_DEPTH:
            if (i==0)
                procData.getMacroData()->setPermLinearInterpolation(procData.getMicroData(), 1);
            else
                procData.getMacroData()->setPermLinearInterpolation(procData.getMicroData(), 1, 0, 3);
            break;
        case VELOCITY_WEIGHTED:
            procData.getMacroData()->setPermLinearInterpolationVelocityWeights(procData.getMicroData(), 1);
            break;
        case VELOCITY_BASED:
            if (i==0)
                procData.getMacroData()->setPermLinearInterpolation(procData.getMicroData(), 0);
            else
                procData.getMacroData()->setPermLinearInterpolationVelocityBased(
                            procData.getMicroData(),
                            params.getProcessInfo()->getMacroFlowDirection(),
                            POSITIVE);
            break;
        }
        struc.prepareInputForMacro(K_RECOMPUTE);
        procData.getMacroData()->writeKToFile(struc.getMacroKFile());
        FiltEST.setArgs(QStringList()<<struc.getMacroXmlFile());
        //if (i>1) //!!!!!!!!!!!!!
        FiltEST.run(struc.getMacroLog());
        procData.getMacroData()->readVPFromFile(struc.getMacroVPFile());
        if (params.getProcessInfo()->getWriteVToFile())
            procData.getMacroData()->writePorousVelocityToFile(
                        struc.getMacroDir()+QDir::separator()+"porousV.txt");
        if (params.getProcessInfo()->getWriteKToFile())
            procData.getMacroData()->writePorousPermToFile(
                        struc.getMacroDir()+QDir::separator()+"porousK.txt");

        procData.getMicroData()->setVelocity(procData.getMacroData());
        procData.getMicroData()->print();
        if (i<params.getRunNum()-1) struc.setNewRun();

    } // for-loop for number of prescribed runs

    outfile.close();
    return 0;
}


int checkInput(int argc, char *argv[])
{
    if (argc==1){
        printf("Solver requires input arguments! For more info use --help\n");
        return -1;
    } else if (argc==2) {
        QString option(argv[1]);
        if (option == "--help") {
            printf("\\Usage: MiMaCoupling [OPTION] [FILE]\n");
            printf("[OPTION]:\n");
            printf("\t--help\t display this help and exit\n");
            printf("\t-wXML\t create input XML file with default setting and exit.");
            printf("\t\t\t\t File name is required!\n");
            printf("[FILE]:\n");
            printf("\txml-file with input parameters\n");
            return 0;
        }
        return 1000;
    } else {
        for (int i=1; i<argc-1; i++) {
            QString option(argv[i]);
            if (option == "-wXML"){
                return 1;
            } else {
                printf("Unknown option specified: %s\n",option.toLatin1().constData());
                return -1;
            }
        }
    }
    return 0;
}
