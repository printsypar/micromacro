#ifndef INCLUDES_H
#define INCLUDES_H

// QT files
#include<QString>
#include<QStringList>
#include<QDir>
#include<QFile>
#include<qtextstream.h>
#include<QProcess>

#include <QtXml/QDomDocument>
#include <QtXml/QDomNode>
#include <QtXml/QDomNodeList>
#include <QtXml/QDomText>

// C++ libraries
#include<iostream>
#include<stdlib.h>

//
#include<../src/defines.h>
#include<../src/xmlInterface/xmldocReader.h>
#include<../src/matrix.h>
#include<../src/errorhandler.h>

#endif // INCLUDES_H

