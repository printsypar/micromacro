#-------------------------------------------------
#
# Project created by QtCreator 2013-02-11T14:17:24
#
#-------------------------------------------------

QT       += core
QT	 += xml
QT       -= gui

TARGET = MiMaCoupling
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += ../src/main.cpp \
    ../src/inputParameters/inputparam.cpp \
    ../src/projStructure/projstructure.cpp \
    ../src/processData/macrodata.cpp \
    ../src/processData/microdata.cpp \
    ../src/xmlInterface/xmldocReader.cpp \
    ../src/childprocess.cpp \
    ../src/matrix.cpp \
    ../src/errorhandler.cpp \
    ../src/projStructure/macroinput.cpp \
    ../src/projStructure/microinput.cpp \
    ../src/processData/processdata.cpp \
    ../src/inputParameters/generalinput.cpp \
    ../src/inputParameters/processinput.cpp

HEADERS += \
    ../src/defines.h \
    ../src/includes.h \
    ../src/inputParameters/inputparam.h \
    ../src/projStructure/projstructure.h \
    ../src/processData/macrodata.h \
    ../src/processData/microdata.h \
    ../src/xmlInterface/xmldocReader.h \
    ../src/childprocess.h \
    ../src/matrix.h \
    ../src/errorhandler.h \
    ../src/singleton.h \
    ../src/projStructure/microinput.h \
    ../src/projStructure/macroinput.h \
    ../src/processData/processdata.h \
    ../src/inputParameters/generalinput.h \
    ../src/inputParameters/processinput.h \
    ../src/utilities.h
