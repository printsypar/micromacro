% !TEX root = ./MiMaCoupling.tex
\section{Numerical experiments}
\label{numExp}
\begin{table}
  \begin{center}
  \begin{tabular}{lll}
  \hline
  Variable & Dimension & Value \\
  \hline
  Fluid viscosity, $\mu$ & $[Pa\;s]$ & $0.0124$ \\
  Fluid density, $\rho$ & $[kg/m^3]$ & $869$ \\
  Flow rate, $Q$ & $[l/min]$ & $0.1$ \\
  Particle concentration, $C$ & $[1/m^3]$ & $10^{12}$ \\
  \hline
  \end{tabular}
  \caption{Input parameters for fluid and flow}
  \label{tab:fandf}
  \end{center}
\end{table}

\begin{table}
  \begin{center}
  \begin{tabular}{lllll}
  \hline
  Variable & Dimension & Particle type 1 & Particle type 2 & Particle type 3 \\
  \hline
  Particle radius, $R$ & $[m]$ & $10^{-6}$ & $2\times10^{-6}$ & $3\times10^{-6}$ \\
  Particle density, $\rho_p$ & $[kg/m^3]$ & $2600$ & $2600$ & $2600$ \\
  Probability & $[\%]$ & $75.5$ & $19.5$ & $5$ \\
  \hline
  \end{tabular}
  \caption{Particle distribution}
  \label{tab:particle}
  \end{center}
\end{table}

The described multiscale algorithm was implemented as separate software tool. The tool works as an interface between the FiltEST flow solver and FilterDict module in GeoDict software. The FiltEST flow solver is used to compute flow field and pressure distribution for the macroscale computations and FilterDict solvers are for the microscale efficiency computations.

A numerical experiment for the filter element shown in Fig.~\ref{fig2} is presented. The filter element has one inlet on the lower part of the filter housing (Fig.~\ref{fig2}, right), one outlet on the upper part (Fig.~\ref{fig2}, left) and the porous media layer in between (Fig.~\ref{fig2}, middle). Parameters, which are used for performing flow simulations, are presented in Table~\ref{tab:fandf}. Parameters for the particle distribution are summarized in Table~\ref{tab:particle}. The microscale computations are performed with time step $600\;s$ and the stopping criteria is chosen as
$$
\exists \alpha \text{ such that }\frac{\left|K_{zz}(\omega_{\alpha},t^{n+1})-K_{zz}(\omega_{\alpha},t^{n})\right|}{\left|K_{zz}(\omega_{\alpha},t^{n})\right|}<\epsilon,
$$
with  $\epsilon=0.25$.
For the numerical experiment with these parameters we assume that the flow establishes very fast for time steps which we are interested in at both macro and microscale problems. Therefore, partial time derivatives in equations (\ref{td1}) and (\ref{td3}) can be neglected. Then, the initial conditions (\ref{td2}) and (\ref{td4}) are not used any more.
%\begin{figure}[!h]
%  \centering
%  \includegraphics[width=1.0\textwidth]{pics/filterElement} 
%  \caption{Filter element}
%  \label{fig2}
%\end{figure}

For the computations we use four different realizations of the microstructure, which were generated with the help of GeoDict. The virtual samples have $4.7\%$ of the solid volume fraction and size $240\;\mu m\times240\;\mu m\times 2\;mm$. They consist of circular fibers with diameter $5\;\mu m$ mostly oriented in $xy$-plane (see Fig.~\ref{fig:sample}). The positions of the selected windows in the porous layer are chosen based on the distribution of $U_{z}^0$, $\mathbf x\in\Gamma_{p,in}$, and are shown in Fig.~\ref{fig:samplePos}.

\begin{figure}[!h]
  \centering
%  \begin{minipage}{0.53\textwidth}
%	\centering
%  	\includegraphics[width=1\textwidth]{pics/sample} 
%  	\caption{Microstructure example}
%  	\label{fig:sample}
%  \end{minipage}
%  \hspace{1mm}
%  \begin{minipage}{0.43\textwidth}
%	\centering
  	\includegraphics[width=0.43\textwidth]{pics/samplePosition} 
  	\caption{Position of microstructures in the porous layer (four black squares)}
  	\label{fig:samplePos}
%  \end{minipage}
\end{figure}

To interpolate microscale permeability in the whole porous layer at time $t=0\; s$ we use a piecewise linear in space function (see Fig.~\ref{fig:perm} on the left). For $t>0$, we use a function which is piecewise linear with respect to the inflow velocity.

\begin{figure}[!h]
  \centering
  \includegraphics[width=1.0\textwidth]{pics/perm} 
  \caption{Permeability}
  \label{fig:perm}
\end{figure}

Macroscopic simulation are performed at time $t=0\;s$, $7800\;s$, and $13200\;s$. The permeability in the whole porous layer is shown in Fig.~\ref{fig:perm}. It is obtained from the microscale simulations and it changes with time as more particles are loaded in different areas of the porous layer. Obtained distributions of the fluid pressure and the fluid velocity in the filter element at time $t=0$ are shown in Fig.~\ref{fig:3dvp}. We do not show here the fluid pressure and the fluid velocity in the whole filter element at time $7800\;$ and $13200\;s$, because the difference cannot be seen in this representation.

%\begin{equation}
%g_0=
%\begin{cases}
%\mathbf{K}_i+\left(\mathbf{K}_{i+1}-\mathbf{K}_i\right)\frac{x-x_i}{x_{i+1}-x_i},\quad x_i<x\leq x_{i+1},\;i=0,\ldots,N-1;\\
%\mathbf{K}_i,\quad x<x_0\cup x\geq x_N;
%\end{cases}
%\end{equation}

\begin{figure}[!h]
  \centering
  \includegraphics[width=1.0\textwidth]{pics/3d_v&p} 
  \caption{Fluid pressure and velocity}
  \label{fig:3dvp}
\end{figure}

\begin{figure}[!h]
  \centering
  \includegraphics[width=1.0\textwidth]{pics/inflowVy} 
  \caption{Inflow fluid velocity}
  \label{fig:inVy}
\end{figure}

In Fig.~\ref{fig:inVy} the inflow fluid velocity $U_z^m$ on $\Gamma_{p,in}$ and $t^m=0\;s$, $7800\;s$, and $13200\;s$ is shown. Distribution of the fluid pressure in the filtering media is presented from the inflow and outflow sides in Figs.~\ref{fig:Pin} and \ref{fig:Pout}, respectively. The fluid velocity in the porous layer is shown in Figs.~\ref{fig:porousVin} and \ref{fig:porousVout}. From the presented results (Figs.~\ref{fig:inVy}--\ref{fig:porousVout}) we already can observe the influence of the microscale simulations on the macroscale results. Pressure drop for the filter element also changes with time and it is equal to $40\;Pa$, $47\;Pa$, and $55\;Pa$ for time moments $0\;s$, $7800\;s$, and $13200\;s$, respectively.

Results of microscale simulations are presented in Table~\ref{tab:microResult}. We show the obtained permeability values for each microstructure for the given inflow fluid velocity. One can see from the table that the permeability changes with the deposition of the particles. This illustrates how the macroscale influences the microscale, since the particle loading in each $\omega_{\alpha}$ depends on the inflow fluid velocity for this $\omega_{\alpha}$.

\begin{figure}[!h]
  \centering
  \includegraphics[width=1.0\textwidth]{pics/Pin} 
  \caption{Fluid pressure in porous layer from inflow side}
  \label{fig:Pin}
\end{figure}
\begin{figure}[!h]
  \centering
  \includegraphics[width=1.0\textwidth]{pics/Pout} 
  \caption{Fluid pressure in porous layer from outflow side}
  \label{fig:Pout}
\end{figure}
\begin{figure}[!h]
  \centering
  \includegraphics[width=1.0\textwidth]{pics/porousVin} 
  \caption{Fluid velocity in porous layer from inflow side}
  \label{fig:porousVin}
\end{figure}
\begin{figure}[!h]
  \centering
  \includegraphics[width=1.0\textwidth]{pics/porousVout} 
  \caption{Fluid velocity in porous layer from outflow side}
  \label{fig:porousVout}
\end{figure}

\begin{table}[!h]
  \begin{center}
  \begin{tabular}{lcccccc}
  \hline
  & \multicolumn{3}{c}{$K_{zz}\times10^{11}$, $[m^2]$} 
  & \multicolumn{3}{c}{$u_{z}^{in}\times10^5$, $[m/s]$} \\
  \hline
  $t$, $[s]$ & $0$ & $7800$ & $13200$ 
  & $0$ & $7800$ & $13200$ \\
  \hline
  sample~1 & $5.28$ & $4.74$ & $4.27$
  & $5.34$ & $5.77$ & $6.31$ \\
  sample~2 & $5.29$ & $4.45$ & $3.70$
  & $7.40$ & $7.43$ & $7.38$ \\
  sample~3 & $5.32$ & $4.47$ & $3.70$
  & $7.77$ & $7.82$ & $7.68$ \\
  sample~4 & $5.40$ & $3.98$ & $2.91$
  & $11.50$ & $10.76$ & $9.93$ \\
  \hline
  %$\Delta\bar{p}$ & & & \\
  \end{tabular}
  \caption{Microsimulation results}
  \label{tab:microResult}
  \end{center}
\end{table}
