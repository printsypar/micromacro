% !TEX root = ./MiMaCoupling.tex
\section{Mathematical models of filtration processes}
\label{models}
In this Section we present and discuss the mathematical models used to describe a class of filtration problems. We start with full microscale model and gradually discuss the subclasses of microscopic and macroscopic models. Finally, we present the coupled micro and macroscale model used in this paper. 

\subsection{Full microscale problem}
\label{Fullmicro}

At first, we provide the full microscale model in $\Omega$ (the full filter element domain), which is the model in the case when the pore space of the filtering medium is resolved:
\begin{equation}
	\label{FullMicro}
	\mathcal{NS}\left(
		\mathbf{u}, 
		p, 
		\mathbf{u}_{in}, 
		0, 
		T, 
		\{\Omega_f\cup\Omega_{pf}\}, 
		\{\Gamma_w\cup\Gamma_{ps}\}, 
		\Gamma_{in}, 
		\Gamma_{out}
	\right),
\end{equation}
where 
	$\mathbf{u}$ is the fluid velocity vector, 
	$p$ is the fluid pressure,
	$T>0$ is the total time,
	$\mathbf{u}_{in}$ is the inlet velocity.

For the particle motion the following system of equations in Lagrangian coordinates is solved (for more details see \cite{LatzWiegmann} and references therein):
\begin{equation}
	\label{FullMicro2}
	\mathcal{P}\left(
		\mathbf y,
		\mathbf v,
		\mathbf u,
		\mathbf b,
		\{\Omega_f\cup\Omega_{pf}\},
		\{\Gamma_w\cup\Gamma_{ps}\}
	\right),
\end{equation}
where $\mathbf y$ and $\mathbf v$ are the particle position and velocity, respectively. In this setting, the initial position of the particle satisfies $\mathbf{b}\in\Gamma_{in}$.

\begin{remark}
If the filter media varies from one filter element to another (what happens when no perfect uniformity  can be achieved during filter media manufacturing process), Monte Carlo methods have to be applied to account for this variability. Furthermore, Monte Carlo simulations needs to be performed to account for the stochasticity of the Brownian motion. These aspects are properly treated when microscale models are considered, however detailed discussion on stochasticity is beyond the scope of this paper.
\end{remark}

The introduced model (\ref{FullMicro}), (\ref{FullMicro2}) is very detailed, and the existing computer power is not enough to simulate the filtration at a filter element scale in the above setting. To cope with the complexity, researchers usually simplify the above model in two directions, and perform simulations either at micro or at macroscale. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Microscale problem used in practical computations}
\label{micro}
As mentioned above, solving the full microscale model is not feasible on the existing computers. 
To perform {\it microscale simulations}, one selects small parallelepiped subdomain(s) $\omega_{\alpha}$ for $\alpha=\overline{1,A}$ as discussed in Section~\ref{Assumptions}. 
Then, the same set of equations as for full microscale model is solved. However, different computational domains are used as follows
\begin{align}
	\label{RedMicro}
	\mathcal{NS}&\left(
		\mathbf{u}, 
		p, 
		\mathbf{u}_{in}, 
		0, 
		T, 
		\omega_{f,\alpha}, 
		\gamma_{s,\alpha},
		\gamma_{in,\alpha}, 
		\gamma_{out,\alpha}
	\right),\\
	\label{RedMicro2}
	\mathcal{P}&\left(
		\mathbf y,
		\mathbf v,
		\mathbf u,
		\mathbf b,
		\omega_{f,\alpha},
		\gamma_{s,\alpha}
	\right),
\end{align}
for $\alpha=\overline{1,A}$. The value $\mathbf u_{in}$ is a prescribed value, usually it comes from dividing the total flow rate throughout the filtering element by the total surface of the filtering medium. The initial position of the particles satisfy $\mathbf b\in \gamma_{in,\alpha}$. Boundary conditions are adjusted in this case. The side boundaries $\gamma_{w,\alpha}$ are often equipped with periodic boundary conditions, which are inspired by the homogenization approach. 

The computations are usually embedded in Monte Carlo algorithm to cope with stochasticity of the microstructure and of the Brownian motion. 

In general, the computer simulation of the filtration processes at micro scale is performed using a fractional time step algorithm (see Section \ref{timeDiscr}. At each time step the fully coupled system for flow and particles transport and deposition is decoupled and solved into two substeps. At the first substep the microstructure is frozen and does not change while the flow field is computed. At the second substep the just computed flow field is frozen and used to compute particle's motion and deposition. More details about the time discretization algorithm are provided below. In the practice some further simplifications can be done. For example, the simulations are not done for all $\alpha=\overline{1,A}$, but just for one or few microgeometries. The flow can also be considered quasi steady state, and instead of recomputing it at each time step, it is recomputed only after a significant change in the microscale geometry (i.e., after enough particles are deposited). Moreover, instead of injecting particles continuously, one can inject the particles on portions, called sometimes batches.

\begin{remark}
We recall that the microstructures $\omega_{\alpha}$ change with time due to deposited particles, when the latter become part of the solid domain.
\end{remark}

A critical component of the simulation of filtration processes, which has to be considered carefully, is the adhesion of the particles to the solid structure. There are different models which can be considered, e.g. Hamaker model, sieving, collision count (see e.g. \cite{FilterDictMan} and references therein). In each particular case the choice of the adhesion model is selected depending on process and materials conditions. 

The described mathematical model for flow and transport of particles within a microstructure, in application to filtration problems, is described in \cite{LatzWiegmann} and implemented in GeoDict software \cite{GeoDict}. A module of GeoDict, namely FilterDict, was used to carry out the microscale simulations presented here. 
More detailed description of the mathematical model and software features can be found in the user manual of FilterDict module \cite{FilterDictMan}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Macroscale problem}
\label{macro}

As mentioned above, simulating filtration problems in a pore scale resolved geometry is not feasible, therefore macroscale models have been used in the literature for simulation at the level of filter element. Macroscale models can be either derived from microscale models via asymptotic homogenization or volume averaging, or can be written directly at the macroscale based on conservation laws, and complemented with constitutive relations when necessary. 

Performing, macroscale simulation of filtration processes, one has to deal with two types of domains: domain occupied by pure fluid and domain occupied by porous media. In general, in such cases one needs also proper interface conditions on the interface between the  plain and the porous media. For detailed discussion on this issue, as well as on the choice of the governing equations, we refer, e.g., to \cite{Brinkman,PhD_Laptev,IlievLaptev04}. This study uses Navier-Stokes--Brinkman equations for describing the flow within a filter element. This means that the considerations are restricted to relatively slow flows, for fast flows at macroscopic scale we refer to \cite{Filtech13_fast} and the references therein.

The initial boundary value flow problem to be solved is described as follows:
\begin{equation}
	\label{Macro}
	\mathcal{NSB}\left(
		\mathbf{U}, 
		P, 
		\mathbf{U}_{in}, 
		\mathbf K,
		0, 
		T, 
		\Omega_f, 
		\Omega_p,
		\Gamma_w, 
		\Gamma_{in}, 
		\Gamma_{out}\right),
\end{equation}
where 
	$\mathbf{U}$ is the fluid velocity, 
	$P$ is the fluid pressure,
	$\mathbf U_{in}$ is the inflow velocity,
	$\mathbf{K}$ is a symmetric, positive definite, intrinsic permeability tensor. 

Further on, at macroscale the transport of contaminants and their capturing is described by a convection-diffusion-reaction equation for the concentration of particles:%, which reads as follows:  
\begin{equation}
	\label{eq_ConcEvolution2}
	\mathcal{CDR}\left(
		C,
		\mathbf{U}, 
		C_{in}, 
		0, 
		T, 
		\Omega_f, 
		\Omega_p,
		\Gamma_w, 
		\Gamma_{in}, 
		\Gamma_{out}\right).
\end{equation}
%\begin{equation}
%	\label{eq_ConcEvolution}
%	\frac{\partial\conc}{\partial t}+\vel\cdot\nabla\conc - D\,\Delta \conc =
%	\begin{cases}
%	\hskip3ex 0,&\,\mathbf{x}\in\Omega_f\\
%	&\\[-1.5ex]
%	-\frac{\partial \capt}{\partial t},&\,\mathbf{x}\in\Omega_p
%	\end{cases}\,.
%\end{equation}
%Here, $D$ denotes the diffusion constant and $\capt$ the concentration of the captured (deposited) particles in the filtering medium. Thus, the evolution of the concentration $\conc(\mathbf{x},t)$ of dissolved particles is modeled by a convection-diffusion-reaction equation, where the reactive term is non-zero in the porous region only, and it is coupled with the \emph{deposition rate}, i.e. with the time derivative of the deposit $\capt$. 

Furthermore, when the filtering medium is loaded with particles, the pore space changes with time. This change in turn affects the flow resistivity of the medium. Therefore, the permeability has to be treated as a quantity depending on both, $\mathbf{x}$ and $t$. The main challenge is to find a correlation between the deposited particles and the change in the permeability. A straight forward way of doing this at macroscale is to use the \emph{porosity} of the filtering medium
$
\phi = V_{\text{pores}}/V,
$
which is often a space dependent function $\phi(\mathbf{x})$. Many permeability models relate the local permeability to the corresponding porosity. A well-known example is the model introduced in \cite{JJ86} for fibrous porous media:
\begin{equation}
	\label{permJJ}
	%\perm_{JJ}(\phi) 
	K_{JJ}(\phi)= - r_{\text{fib}}^2 \frac{3}{20} \frac{\ln(1-\phi)+0.931}{1-\phi}\,,
\end{equation}
where $r_{\text{fib}}$ is the (average) radius of the fibers that form the medium. 
In terms of flow resistance, the dirt deposited in the medium might be very different from fibers, e.g. if the particles are more or less spherical in shape. For granular porous media, the Kozeny-Carman formula for the permeability (see, e.g., \cite{Kaviany}) is widely used:
\begin{equation}
	\label{permKC}
	%\perm_{KC}(\phi) 
	K_{KC}(\phi)=  r_{\text{par}}^2  c_{\text{par}}\frac{\phi^3}{(1-\phi)^2}\,,
\end{equation}
where $r_{\text{par}}$ denotes the (average) radius of the particles and $c_{\text{par}}$ is related to the sphericity of the particles. 
For the loading of a fibrous medium with particles, a combined model by taking the harmonic average of the permeability of the clean medium and the contribution by the particle loading was proposed in \cite{HybridPermeabilityFiltech}: 
\begin{equation}
\label{permHybrid}
%\perm(t) = \left(\dfrac{1}{\perm_{\text{clean}}} + \dfrac{1}{\perm_{\text{load}}(t)}\right)^{-1}\,.
K(\phi(t)) = \left(\dfrac{1}{K^{fibres}(\phi_0)}+ \dfrac{1}{K^{particles}(\phi(t))}\right)^{-1}\,.
\end{equation}
%$\perm_{\text{load}}(t)$ is assumed to depend on the increment of solid volume fraction $\bar{\phi}=1-\phi$ at time $t$, i.e.
%\[
%\perm_{\text{load}}(t)
%K_{load}(\phi)=\perm_{\text{load}}(\bar{\phi_+}(t))\,,\;\bar{\phi}_+ = \bar{\phi}(t)-\bar{\phi}(0)\,.
%\]

The macroscale problem and the solution algorithm are discussed in more details, e.g., in  \cite{CiegisIlievLakdwala,ZahraPhD,KirschFiltech,HybridPermeabilityFiltech}. The proposed method is implemented in the software tool FiltEST \cite{FiltEST}, which was used to carry out the macroscale simulation in this study.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\begin{figure}[!h]
%  \centering
%  \includegraphics[width=0.8\textwidth]{pics/2DMiMawithNotations} 
%  \caption{Computational domains for macro- and microscale problems}
%  \label{notations}
%\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Multiscale problem}
\label{multi}

The microscale setting described in \ref{micro} provides very detailed information about the flow within the pore scale resolved filtering medium and the capturing of the particles there, but it is missing the influence of the filter element geometry and of the macroscopic heterogeneity of the filtering medium on the filtration efficiency. The macroscale setting described in Section \ref{macro} accounts properly for the filter element geometry, but formulas like 
(\ref{permJJ})--(\ref{permHybrid}) work only with average quantities, e.g., porosity. They do not return full permeability tensor, and thus the detailed information about the pore scale geometry is missing. 

A coupled multiscale formulation is proposed here to provide more consistent consideration of the filtration processes. It is as follows\\
{\it at macroscale}
\begin{equation}
	\label{CoupledMacro}
	\mathcal{NSB}\left(
		\mathbf{U}, 
		P, 
		\mathbf{U}_{in}, 
		\mathbf K,
		0, 
		T, 
		\Omega_f, 
		\Omega_p,
		\Gamma_w, 
		\Gamma_{in}, 
		\Gamma_{out}\right),
\end{equation}
{\it at microscale}
\begin{align}
	\label{CoupledMicro1}
	\mathcal{NS}&\left(
		\mathbf{u}, 
		p, 
		\{\mathbf{U}(t,\mathbf x),\;\mathbf x\in\Gamma_{p,in}\} , 
		0, 
		T, 
		\omega_{f,\alpha}, 
		\gamma_{s,\alpha},
		\gamma_{in,\alpha}, 
		\gamma_{out,\alpha}
	\right),\\
	\label{CoupledMicro2}
	\mathcal{P}&\left(
		\mathbf y,
		\mathbf v,
		\mathbf u,
		\mathbf b,
		\omega_{f,\alpha},
		\gamma_{s,\alpha}
	\right),
\end{align}
where the permeability $\mathbf K$ is a calculated according to (\ref{avPerm}).

The flow within the filter element is modeled at macroscale by (\ref{CoupledMacro}). Particles transport and deposition are modeled at microscale by (\ref{CoupledMicro1}) and (\ref{CoupledMicro2}), using the partitioning $\omega_\alpha$. The coupling from micro to macroscale comes from the recomputed permeability, the coupling from macro to microscale results from using local macroscale velocities as inflow conditions in (\ref{CoupledMicro1}) and (\ref{CoupledMicro2}). Additional assumption here is that there is a perfect mixing of the particles in the liquid zone upward the filter media.


%The flow within the filter element is modeled at macroscale, using the same flow equations \ref{eqx-eqxx}  as in Subsection \ref{macro}. The permeability in equation \ref{eq-per} is a function of the microscale variables. Particles transport and deposition is modeled at microscale, using the partitioning $\omega_\alpha$ and the introduced above microscale model \ref{eq-micro1-eq-micron}. The coupling from micro to macro scale comes from the recomputed permeability, the coupling from macro to micro results from prescribing inlet velocities at each window at each time moment as macroscale velocities. Additional assumption here is that there is a perfect mixing of the particles in the liquid zone upward the filter media.


