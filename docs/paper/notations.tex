% !TEX root = ./MiMaCoupling.tex
\section{Preliminaries on multiscale modeling of filtration processes}
\label{coupling}
In this Section, we first state the assumptions under which the models are valid, and introduce notations. Next, the governing equations that are used as building blocks in the models are presented. Lastly, we shortly recall some upscaling results which are later adapted for the needs of the presented here multiscale algorithm.

\subsection{Assumptions and notations}
\label{Assumptions}
Let us first list the assumptions under which the filtration is modeled and simulated in this paper. 
\begin{assumption}
\label{ass1}
The flow is laminar.
\end{assumption}
\begin{assumption}
\label{ass2}
The flow at the pore level is in Stokes regime, so that Darcy law is valid at the scale of the effective porous media. In fact, in certain practical computations this can be slightly relaxed, if Navier-Stokes-Brinkman equation is used for macroscale computations.
\end{assumption}
\begin{assumption}
\label{ass3}
Dilute flow of small particles is considered: the particles do not influence the flow, and the non-captured particles do not interact with each other.
\end{assumption}
\begin{assumption}
\label{ass4}
Particles are considered to be spherical, having mass and impulse. Particles are considered to be volumeless in the transport, but the volume of the deposited particles is accounted for. 
\end{assumption}
\begin{assumption}
\label{ass5}
Depth filtration is considered, i.e., particles are deposited within the porous media. Cake filtration (corresponding to the case when the particles are deposited on the filtering medium surface) can also be handled by the presented model, but its description would require more complicated notations.
\end{assumption}

We need to introduce some notations before describing the models. Let the computational domain be defined by $\Omega\subset\mathbb{R}^3$.  Let the boundary $\partial\Omega$ be split into three parts $\partial\Omega=\Gamma_{in} \cup\Gamma_{out}\cup\Gamma_{w}$ such that $\Gamma_{\alpha}\cap\Gamma_{\beta}=\emptyset$ for $\alpha\neq\beta$ and $\alpha$, $\beta=\{in,\: out,\: w\}$. Indices '$in$', '$out$', and '$w$' correspond to a flow inlet, a flow outlet, and solid walls of the filter element housing. Here by walls we mean the walls of the filter element housing. 

\begin{figure}[!h]
  \centering
  \includegraphics[width=0.7\textwidth]{pics/2DMiGeneralSetting} 
  \caption{Detailed description of the computational domain $\Omega$}
  \label{fig2}
\end{figure}

For the purpose of describing the multiscale approach, we describe the computation domain in three different ways, namely  as the macroscale domain, as the full microscale domain and lastly as a partitioned microscale domain. 
\paragraph{Macroscale domain notations} are later used in describing the macroscale models. The computational domain on the macroscale is divided into two subdomains $\Omega_f$ and $\Omega_p$ corresponding to the fluid and to the porous subdomains, respectively (see Fig.~\ref{fig2}). The subdomains satisfy $\Omega_f\cup\Omega_p=\Omega$ and $\Omega_f\cap\Omega_p=\emptyset$. We also introduce two interfaces between the fluid and the porous medium, namely $\Gamma_{p,in}$ for the interface from the side of the inlet and $\Gamma_{p,out}$ for the interface from the side of the outlet. These interfaces satisfy $\Gamma_{p,in}\cup\Gamma_{p,out}=\partial\Omega_f\cap\partial\Omega_p$. 
\paragraph{Full microscale domain notations}  are used to introduce the global microscale model. The computational domain on the full microscale domain comprises of $\Omega_{pf}$ and $\Omega_{ps}$ denoting the pore space of the filtering medium and   the rigid solid skeleton of the filtering medium, respectively. The latter corresponds to the volume occupied by fibers, if a nonwoven porous media like the one on Fig.~\ref{fig1} and on Fig.~\ref{fig:sample} is used. The total surface of the solid skeleton (except the one intersecting with the walls of the filter housing) is denoted by $\Gamma_{ps}$. Let us note that in fact the pore space changes as the time $t$ evolves due to the deposition of particles, and the same holds for the solid skeleton, i.e.,  $\Omega_{pf}=\Omega_{pf}(t)$ and $\Omega_{ps}=\Omega_{ps}(t)$. In the above notations $\Omega_f\cup\Omega_{pf}(t)$ stand for the volume occupied by fluid in the case when the filtering medium is fully resolved at pore scale. Obviously, $\Omega_{pf}(t)\cup\Omega_{ps}(t)=\Omega_p$.
\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.8\textwidth]{pics/3DMiMa} 
  \caption{An illustration of the overall setup for multiscale simulations. The complete filter element is considered at the macroscale. Several "windows" are chosen at different locations of the filtering medium, and the filtering medium there is resolved at the microscale (pore scale). }
  \label{fig1}
\end{figure}
\paragraph{Partitioned microscale domain notations} are required to describe microscale simulations in selected local subdomain(s) of $\Omega_p$. Let $\omega_{\alpha}$ be a partition of $\Omega_{p}$, where $\omega_{\alpha}\subset{\mathbb{R}^3}$ for $\alpha=\overline{1,A}$ (see Fig.~\ref{notations}). In other words, $\omega_{\alpha}$ are microstructures corresponding to a small part of the porous domain. Each domain $\omega_{\alpha}$ consist of two complementary parts:  fluid part $\omega_{f,\alpha}$ and solid part $\omega_{s,\alpha}$, such that $\omega_{\alpha}=\omega_{f,\alpha}\cup\omega_{s,\alpha}$ and $\omega_{f,\alpha}\cap\omega_{s,\alpha}=\emptyset$. Let the boundary of each domain $\omega_{\alpha}$ be split into three non-intersecting parts, namely  $\partial\omega_{\alpha}=\gamma_{in,\alpha}\cup\gamma_{out,\alpha}\cup\gamma_{w,\alpha}$. Indices '$in$', '$out$', and '$w$' correspond to inflow, outflow, and side boundaries, respectively (see Fig.~\ref{notations}). The interface between solid and fluid is denoted by $\gamma_{s,\alpha}$, which is equal to $\partial\omega_{f,\alpha}\cap\partial\omega_{s,\alpha}$, and stands for the walls of the solid skeleton formed by the fibers and the deposited particles. The notations for this partitioning are completed by introducing $\omega^{par}_{s,\alpha}(t)$ as notation for the subdomain occupied by particles which are immobilized (captured, sieved, deposited) at time $t$. Then, $\omega_{s,\alpha}(t)  = \omega^{clean}_{s,\alpha} \cup  \omega^{par}_{s,\alpha}(t)$, where $\omega^{clean}_{s,\alpha}$ represents solid microstructure before particles immobilization starts.

\begin{figure}[!h]
  \centering
  \includegraphics[width=1.0\textwidth]{pics/2DMiMawithNotations_2} 
  \caption{Left: The figure illustrates a cross-section of a filter element. Right: The figure illustrates the geometry of the filtering media on the microscale. These form the computational domains for macro and microscale problems, respectively.}
  \label{notations}
\end{figure}

\subsection{Governing equations}
\label{Governing}
In this subsection we will list the equations which will be used in the next Subsection for describing microscale, macroscale, and multiscale models. The equations will be written in some generic domains, the latter will be specified below for each of the models. Let ${\it D_f}$ denotes some generic fluid domain, and  $\it{D_p}$ denotes some generic domain occupied by porous media. Further on, $\partial{\it D_{in}}$ denotes generic inlet, $\partial{\it D_{out}}$ denotes generic outlet, $\partial{\it D_{w}}$ denotes generic solid walls. A generic time interval is denoted here as  $[\mathit T_{0}, \mathit T_{1}]$.

Initial boundary value problem for incompressible unsteady {\bf Navier-Stokes equations} is denoted by 
\begin{equation}
	\label{NS}
	\mathcal{NS}\left(
		\boldsymbol{u}, 
		p, 
		\boldsymbol{u}_{in}, 
		\mathit T_0, 
		\mathit T_1, 
		D_f, 
		\partial D_w, 
		\partial D_{in}, 
		\partial D_{out}\right)
\end{equation}
and reads:
\begin{align}
	\label{gen1}
	\rho \frac{\partial \boldsymbol{u}}{\partial t}
	-\bigtriangledown\cdot\left(\mu \bigtriangledown \boldsymbol{u} \right)
	+ \left( \rho \boldsymbol{u} \cdot \bigtriangledown \right) \boldsymbol{u}
	+\bigtriangledown p &= \mathbf f \text{ in }  [\mathit T_{0}, \mathit T_{1}]\times{\it D_f},\\
	\label{gen2}
	\bigtriangledown\cdot\boldsymbol{u} &= 0 
	\text{ in }[\mathit T_{0}, \mathit T_{1}]\times{\it D_f},\\
	\label{gen3}
	\boldsymbol{u} &= \mathbf 0 \text{ on }  [\mathit T_{0}, \mathit T_{1}]\times{\it \partial D_w},
	\\
	\label{gen4}
	\boldsymbol{u} &= \boldsymbol{u}_{in}
	\text{ on }  [\mathit T_{0}, \mathit T_{1}]\times{\it \partial D_{in}},\\
	\label{gen5}
	p &= 0 \text{ on } [\mathit T_{0}, \mathit T_{1}]\times{\it \partial D_{out}},\\
	\label{gen7}
	\boldsymbol{u}(t=\mathit T_{0}) &= \mathbf{0} \text{ in }{\it D_f};
\end{align}
where 
	$\rho$ is the fluid density, 
	$\boldsymbol{u}$ is the fluid velocity vector, 
	$t$ is the time,
	$\mu$ is the fluid dynamic viscosity,
	$p$ is the fluid pressure,
	$\mathbf{f}$ is the force,
	$\boldsymbol{u}_{in}$ is the inlet velocity vector.
Equations~(\ref{gen1}) and (\ref{gen2}) are the momentum and mass conservation equations, respectively. Equation~(\ref{gen3}) represents the no-slip condition for the fluid velocity on all solid walls. Equations (\ref{gen4}), (\ref{gen5}) are the inlet and outlet boundary conditions.  Initial conditions are specified by equation (\ref{gen7}).

%Here we have introduced notations $NS\mathbf{u}$ and $DIV\mathbf{u}$, to be used for denoting momentum and continuity equation, respectively. 

Initial boundary value problem for incompressible unsteady {\bf Navier-Stokes-Brinkman} equations is denoted by
\begin{equation}
	\label{NSB}
	\mathcal{NSB}\left(
		\boldsymbol{u}, 
		p, 
		\boldsymbol{u}_{in}, 
		\mathbf K,
		\mathit T_0, 
		\mathit T_1, 
		D_f, 
		D_p,
		\partial D_w, 
		\partial D_{in}, 
		\partial D_{out}\right)
\end{equation}
and reads:
\begin{align}
	\label{NSB1}
	\begin{split}
	\rho \frac{\partial \boldsymbol{u}}{\partial t}
	-\bigtriangledown\cdot\left(\mu \bigtriangledown \boldsymbol{u} \right)
	+ \left( \rho \boldsymbol{u} \cdot \bigtriangledown \right) \boldsymbol{u}\;\\
	+\mu \mathbf{H}\boldsymbol{u}
	+\bigtriangledown p &= \mathbf f \text{ in }  [{\it T_{0}, T_{1}}]\times\{\mathit D_f\cup\mathit D_p\},
	\end{split}\\
	\label{NSB2}
	\bigtriangledown\cdot\boldsymbol{u} &= 0 \text{ in }   [{\it T_{0}, T_{1}}]\times\{\mathit D_f\cup\mathit D_p\},\\
	\label{NSB3}
	\boldsymbol{u} &= \mathbf 0 \text{ on }  [{\it T_{0}, T_{1}}]\times{\mathit \partial D_w},
	\\
	\label{NSB4}
	\boldsymbol{u} &= \boldsymbol{u}_{in}, 
	\text{ on }  [{\it T_{0}, T_{1}}]\times{\partial \mathit D_{in}},\\
	\label{NSB5}
	p &= 0 \text{ on } [{\it T_{0}, T_{1}}]\times{\partial \mathit D_{out}},\\
	\label{NSB7}
	\boldsymbol{u}(t=T_{0}) &= \mathbf{0} \text{ in }{\mathit D_f},
\end{align}
\begin{equation}
  \label{perm}
  \mathbf{H}=
  \begin{cases}
  \mathbf{K}^{-1}&\text{ in }[{\it T_{0}, T_{1}}]\times{\mathit D_p},\\
  0&\text{ in }[{\it T_{0}, T_{1}}]\times{\mathit D_f};
  \end{cases}
\end{equation}
with a symmetric, positive definite, intrinsic permeability tensor $\mathbf{K}$.  Notations  
	$\rho$,
	$\boldsymbol{u}$, 
	$t$,
	$\mu$,
	$p$,
	$\mathbf{f}$,
	$\boldsymbol{u}_{in}$, have the same meaning as in Navier-Stokes equations above. 
Equations (\ref{NSB1}) and (\ref{NSB2}) are the momentum and continuity equation in the Navier-Stokes-Brinkman system, respectively. In connection with filtration problems, the usage of Navier-Stokes-Brinkman system of equations is discussed in \cite{PhD_Laptev,IlievLaptev04}.

The following system of equations is governing the {\bf motion of non-interacting particles} in Lagrangian coordinates (for more details see \cite{LatzWiegmann} and references therein) and {\bf particles reflection/adhesion}. To shorten the specification of the governing equations, the system (\ref{Particle1})--(\ref{Particle4}) is denoted by

\begin{equation}
	\label{Particle}
	\mathcal{P}\left(
		\boldsymbol y,
		\boldsymbol v,
		\boldsymbol u,
		\boldsymbol b,
		\mathit D_f,
		\partial\it D_w
	\right)
\end{equation}
and reads 
\begin{align}
	\label{Particle1}
	& d \boldsymbol{y} - \boldsymbol{v}dt = 
	\mathbf 0 \quad\text{ in }[{\it T_{0}, T_{1}}], \\
	\label{Particle2}
	& \begin{cases}
	\!\begin{aligned}%[b]
	        d\boldsymbol{v} = 
		-M\left(\boldsymbol{v}(\boldsymbol{y})-\boldsymbol{u}(\boldsymbol{y})\right)dt
		+\frac{QE_0(\boldsymbol{y})}{m}dt & \\
	        +\sigma d\mathbf{W}(t)  \;\text{ in }[{\it T_{0}, T_{1}}], \;\: &
        \end{aligned}           & \text{for }\boldsymbol{y}\in\mathit{D}_f, \\%[1ex]
	\boldsymbol{v} = A(m\boldsymbol{v},\partial\it D_w,\mathcal{M})\;\text{ in }[{\it T_{0}, T_{1}}], & \text{for }\boldsymbol{y}\in\partial\mathit{D}_w,
	\end{cases}
	\\
%	\begin{split}
%	& \text{if }\boldsymbol{y}\in\mathit{D}_f,\text{then}\\
%	& \quad\quad\quad d \boldsymbol{v} = 
%	-M\left(\boldsymbol{v}(\boldsymbol{y})-\boldsymbol{u}(\boldsymbol{y})\right)dt
%	+\frac{QE_0(\boldsymbol{y})}{m}dt\\
%	&\quad\quad\quad\quad\quad\quad\quad\quad\quad\quad\quad+\sigma d\mathbf{W}(t) 
%	\quad\text{ in }[{\it T_{0}, T_{1}}],\\ 
%	\end{split}\\
%	\label{Particle3}
%	& \quad\quad\quad \left\langle dW_i(t),dW_j(t)\right\rangle = \delta_{ij}dt \quad\text{ in }[{\it T_{0}, T_{1}}], \\
%	& \text{if }\boldsymbol{y}\in\partial\mathit{D}_w,\text{then}\\
%	\label{Particle2A}
%	& \quad\quad\quad \boldsymbol{v} = A(m\boldsymbol{v},\omega_s,\mathcal{M}) \quad\text{ in } [{\it T_{0}, T_{1}}], \\
	&\left\langle dW_i(t),dW_j(t)\right\rangle = \delta_{ij}dt \quad\text{ in }[{\it T_{0}, T_{1}}],\\
\label{Particle4}
	& \boldsymbol{y}(0)=\boldsymbol{b},\quad
	\boldsymbol{v}(\boldsymbol{y}(0))=\boldsymbol{u}(\boldsymbol{b}),
\end{align}
where %$\boldsymbol{y} \in {\mathit D_f}$ and 
$$
M = 6\pi\mu\frac{R}{C_cm}, \qquad
\sigma^2 = \frac{2k_B \mathrm{T}M}{m}, \qquad
K_n = \frac{\lambda}{R},
$$
$$
C_c = 1+K_n\left(1.17+0.525\exp{-\frac{0.78}{K_n}}\right),
$$
	$\boldsymbol{y}$ is the particle position, 
	$\boldsymbol{v}$ is the particle velocity,
	$M$ is the friction coefficient,
	$Q$ is the particle charge,
	$m$ is the particle mass,
	$E_0$ is the electric field,
	$d\mathbf{W}(t)$ is the 3D probability (Wiener) measure,
	$\mathcal{M}$ stands for particles and fibers material properties,
	$\boldsymbol{b}$ is the initial position of the particle,
	$C_c$ is the Cunningham correction,
	$K_n$ is the Knudsen number,
	$R$ is the particle radius,
	$k_B$ is the Boltzmann constant,
	$\mathrm{T}$ is the Ambient temperature,
	$\lambda$ is the mean free path.

The first equation in (\ref{Particle2}) corresponds particle motion within fluid domain, which accounts for the particle friction with fluid, the electric attraction, and the Brownian  motion. The second equation in (\ref{Particle2}) describes particles reflection/adhesion at solid walls. Different equations and inequalities can be used to model this effect, as well as other mechanisms of particles filtration. Detailed discussion on this topic is beyond the goals of this paper, the reader can find details in \cite{FilterDictMan} and references therein. This relation indicates that after hitting a solid wall, the particle can be captured or can elastically or inelastically reflect, depending on particles impulse $m\boldsymbol{v}$, on the microgeometry $\partial\it D_w$, on particles and fibers material properties $\mathcal{M}$, etc. Equations (\ref{Particle4}) are the initial conditions for the particle positions and velocity. In this setting $\boldsymbol{b}$ is a point on $\partial \mathit D_{in}$. This equation describes particles transport in liquids and gases. For liquids $C_c=1$.

%In each particular case the choice of the adhesion model is selected depending on process and materials conditions. 
%Here we just introduce a generic notation
%\begin{equation}
%	\label{ParticleA}
%	\mathcal{A}\left(
%		\boldsymbol y,
%		\boldsymbol v,
%		\boldsymbol u,
%		\boldsymbol b,
%		\mathit D_f
%	\right)
%\end{equation}
%which is defined as 
%\begin{equation}
%	\label{Particle2A}
%	\boldsymbol{v} = A(\boldsymbol{v},\omega_s,\mathcal{M}), \boldsymbol{y} \in {\mathit D_s}. 
%\end{equation}

A {\bf convection-diffusion-reaction} equation with respect to concentration of particles is denoted by 
\begin{equation}
	\label{CDR}
	\mathcal{CDR}\left(
		C,
		\boldsymbol{u}, 
		C_{in}, 
		\mathit T_0, 
		\mathit T_1, 
		D_f, 
		D_p,
		\partial D_w, 
		\partial D_{in}, 
		\partial D_{out}\right)
\end{equation}
and reads as follows:  
\begin{align}
	\label{eq_ConcEvolution}
	& 
	\frac{\partial\conc}{\partial t}+\boldsymbol{u}\cdot\nabla\conc - D\,\Delta \conc =
	\begin{cases}
	\hskip3ex 0&\text{ in }[{\it T_{0}, T_{1}}]\times\mathit{D}_f,\\
	&\\[-1.5ex]
	-\frac{\partial \capt}{\partial t}&\text{ on }[{\it T_{0}, T_{1}}]\times\mathit{D}_p,
	\end{cases}\\
	& C = C_{in}\quad\text{ on }[{\it T_{0}, T_{1}}]\times\partial\mathit{D}_{in},\\
	& \frac{\partial C}{\partial \mathbf{n}} = 0\quad
	\:\text{ on }[{\it T_{0}, T_{1}}]\times\left\{\partial\mathit{D}_{w}\cup\partial\mathit{D}_{out}\right\};
\end{align}
Here, $C$ is the particle concentration, $D$ denotes the diffusion constant, $\capt$ denotes concentration of captured (deposited) particles within the filtering medium, $\mathbf{n}$ is unit normal vector. The reactive term is non-zero in the porous region only, and it is expressed by the \emph{deposition rate}, i.e. the time derivative of the deposit $\capt$. 

In this case different models for the particles capturing can be used depending on the type of the filtering media and the operation regimes, what results in using different types of reaction terms $\partial M/\partial t$ in the concentration equation (\ref{eq_ConcEvolution}). For details we refer to \cite{KirschFiltech,HybridPermeabilityFiltech}. 


\subsection{Upscaling}
\label{upscaling}

In the case of scale separation, a good approximation to the solution of the microscale equations can obtained by one way two step procedure: solve a cell problem to compute the effective properties, and solve the upscaled equations using these effective properties as input. This can be done by means of asymptotic homogenization or volume averaging, (see, e.g., \cite{Hornung,EspedalFasanoMikelic,Kaviany}) and references therein. Here we will shortly recall some useful upscaling results, which will serve as building blocks for our multiscale approach.

First, recall that in the case of periodic or statistically homogeneous porous media, pore scale Stokes flow can be upscaled to Darcy flow \cite{Hornung,EspedalFasanoMikelic} by means of asymptotic homogenization. The permeability tensor in this case is obtained by postprocessing the solution of the so called cell problem, which requires solution of the steady state Stokes equations equipped with periodic boundary conditions. To avoid overloading with notations, in this subsection we suppose that the introduced above notation $\omega_\alpha$ stands for a periodicity cell. Let us denote by $\mathbf u$ and $p$  the solutions to the Stokes BVP in $\omega_\alpha$. The permeability tensor for $\omega_\alpha$ is than calculated as 
\begin{equation}
	\label{permeability}
	K_{ij}=\int_{\omega_{f,\alpha}}\bigtriangledown u_i\cdot\bigtriangledown u_j dx \text{ for }\alpha=\overline{1,A}.
\end{equation}
If volume averaging is used,  with averaging over $\omega_{\alpha}$ or over $\omega_{f,\alpha}$ (see Section \ref{Assumptions} for description of these subdomains), the components of the permeability tensor are calculated as follows:
\begin{equation}
K_{ij}=
\mu\frac{\left\langle \mathbf u\right\rangle_{\omega_{f,\alpha},i}}
{\left\langle\bigtriangledown p\right\rangle_{\omega_{f,\alpha},j} }
\text{ in }\omega_{\alpha};
\label{avPerm}
\end{equation}
where
$$
\left\langle\cdot\right\rangle_{\omega_{f,\alpha},i}=\frac{1}{\int_{\omega_{f,\alpha}}d\mathbf{x}}\int_{\omega_{f,\alpha}}
(\cdot)_id\mathbf x.$$
In the case of scale separation, computations of permeability based on asymptotic homogenization and on volume averaging give equivalent results. See the useful discussion on this issue in \cite{GriebelKlitz}.

Next, we shortly mention that homogenization of Stokes equations to Brinkman equations is discussed in \cite{Allaire}. Applying Brinkman equations for filtration problems is discussed in \cite{PhD_Laptev,IlievLaptev04}.

Finally, it is well known that the macroscale limit of Langevin equation (\ref{Particle1})--(\ref{Particle4})  (under proper assumptions which are valid here) is a scalar concentration equation.



