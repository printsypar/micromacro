%\documentclass[10pt,a4paper,twoside]{article}
\documentclass[11pt,a4paper]{article}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{authblk}
\usepackage{verbatim}
\usepackage{graphicx}
\usepackage{color}

\newtheorem{assumption}{Assumption}
\newtheorem{remark}{Remark}
\newtheorem{definition}{Definition}

\title{On a multiscale approach for filter efficiency simulations}
\author[1,2]{O.~Iliev}
\author[1]{Z.~Lakdawala}
\author[2]{G.~Printsypar}
\affil[1]{Department of Flow and Material Simulations, Fraunhofer ITWM, Kaiserslautern, Germany}
\affil[2]{Numerical Porous Media Center (NumPor), King Abdullah University of Science and Technology (KAUST), Saudi Arabia}

\begin{document}
\maketitle
\abstract{
Filtration in general, and the dead end depth filtration of solid particles out of fluid in particular, is intrinsic multiscale problem. The deposition (capturing of particles) essentially depends on local velocity, on microgeometry (pore scale geometry) of the filtering medium and on the diameter distribution of the particles. The deposited (captured) particles change the microstructure of the porous media what leads to change of permeability. The changed permeability directly influences the velocity field and pressure distribution inside the filter element. To close the loop, we mention that the velocity influences the transport and deposition of particles. In certain cases one can evaluate the filtration efficiency considering only microscale or only macroscale models, but in general an accurate prediction of the filtration efficiency requires multiscale models and algorithms. 
%
This paper discusses the single scale and the multiscale models, and presents a fractional time step discretization algorithm for the multiscale problem. The velocity within the filter element is computed at macroscale, and is used as input for the solution of microscale problems at selected locations of the porous medium. The microscale problem is solved with respect to transport and capturing of individual particles, and its solution is postprocessed to provide permeability values for macroscale computations. Results from computational experiments with an oil filter are presented and discussed. 

%It is common understanding that filtration is a multiscale phenomenon. The processes occurring at different scales are equally important for simulating the filter efficiency. At the macroscale, it is possible to take into consideration the design of the filter housing and the flow field. On the other hand, the microscale takes into account the microstructure of the filtering media and the particles deposition. In this study, an approach for simulating the filter efficiency that couples both the micro and macroscale processes is proposed. This paper begins by stating different models for both scales. Moreover, the existing approaches for modeling the filtration processes are presented and discussed. Next, the proposed algorithm coupling the micro and macroscale models is introduced. A discussion on the fractional time step discretization technique used in this algorithm is also presented. Finally, we conclude with a qualitative numerical experiment to show the capability of the coupling approach performed for a real filter element geometry.
}

\newcommand{\perm}{\textbf{K}}
\newcommand{\conc}{C}
\newcommand{\vel}{ \mathbf{U}}
\newcommand{\capt}{M}

\input{introduction}
\input{notations}
\input{model}
\input{timeDiscretization}
%\input{algorithm}
\input{numExperiment}
\input{conclusion}

\bibliographystyle{plain}
\pagestyle{empty}
\begin{thebibliography}{10}
\bibitem{Filtech13_fast}
O.~Iliev, R.~Kirsch, Z.~Lakdawala, V.~Starikovicius (2013) Numerical simulation of non Darcy flow using filter element simulation toolbox (FiltEST), Filtech, Oct. 22--24, 2013, Wiesbaden, Germany
\bibitem{LatzWiegmann}
A.~Latz and A.~Wiegmann (2003) Simulation of fluid particle separation in realistic three dimensional fibre structures, Proc. Filtech Europa conference, pp.353--360, Duesseldorf, Germany.
\bibitem{GeoDict}
Math2Market (April 2013) http://geodict.com/
\bibitem{micro_RiefEtAl}
S.~Rief, D.~Kehrwald, K.~Schmidt, A.~Wiegmann (2008) Simulation of ceramic DPF Media, Soot deposition, Filtration efficiency and pressure drop evolution, World Filtration Congress, Leipzig.
\bibitem{micro_SalehEtAl}
A.M.~Saleh, S.A.~Hosseini, H.~Vahedi Tafreshi, B.~Pourdeyhimi (2013) 3-D microscale simulation of dust-loading in thin flat-sheet
filters: A comparison with 1-D macroscale simulations, Chemical Eng Science 99: 284--291
\bibitem{micro_BinEtAl}
B.~Xu, Y.~Wu, P.~Cui (2013) Semi-analytical and computational investigation of different dust loading structures affecting the performance of a fibrous air filter, Particuology  (article in press) http://dx.doi.org/10.1016/j.partic.2013.05.004
\bibitem{micro_Li}
S.-Q.~Li, J.S.~Marshall (2007) Discrete element simulation of micro-particle deposition on a cylindrical fiber in an array, Aerosol Science 38: 1031--1046
\bibitem{PhD_Laptev}
V.~Laptev (2003) Numerical solution of coupled flow in plain and porous media. PhD thesis, Tehnical University of Kaiserslautern, Germany 
\bibitem{IlievLaptev04}
O.~Iliev, V.~Laptev (2004) On numerical simulation of flow through oil filters, Comput Visual Sci 6: 139--146
\bibitem{CiegisIlievLakdwala}
R.~Ciegis, O.~Iliev, Z.~Lakdawala (2007) On parallel numerical algorithms for simulating industrial filtration problems, 
[J] Comput. Methods Appl. Math. 7, No. 2, 118-134.
Technical report No.114, Fraunhofer ITWM, Kaiserslautern, Germany
\bibitem{ZahraPhD}
Z.~Lakdawala (2010) On efficient algorithms for filtration related multiscale problems. PhD thesis, Tehnical University of Kaiserslautern, Germany 
\bibitem{CiegisIlievLakdawalaStarikovicius}
V.~Starikovicius, R.~Ciegis, O.~Iliev, Z.~Lakdawala, (2008) A parallel solver for the 3D simulation of flows through oil filters, Workshop High Performance Scientific Computing, Druskininkai, Lithuania, 5--8 February.
\bibitem{sufis}
Software for Flow Simulation in Oil Filters -- SuFiS (2013) 
http://www.itwm.fraunhofer.de/en/departments/flow-and-material-simulation/hydrodynamics/sufis.html
\bibitem{FiltEST}
Filter Element Simulation Tool (2013)\\ http://www.itwm.fraunhofer.de/abteilungen/stroemungs-und-
materialsimulation/hydrodynamik/filtest.html
\bibitem{Nassehi2005}
V.~Nassehi, N.S.~Hanspal, A.N.~Waghode, W.R.~Ruziwa, R.J.~Wakeman (2005) Finite-element modelling of combined free/porous flow regimes:
simulation of flow through pleated cartridge filters, Chemical Engineering Science, 60: 995--1006.
\bibitem{Nassehi2005_2}
R.J.~Wakemana, N.S.~Hanspal, A.N.~Waghode, and V.~Nassehi (2005) Analysis of pleat crowding and medium compression in pleated cartridge filters, Chemical Engineering Research and Design, 83(A10): 1246--1255.
\bibitem{Helmig}
K.~Mosthaf, K.~Baber, B.~Flemisch, R.~Helmig, A.~Leijnse, I.~Rybak, B.~Wohlmuth (2011) A coupling concept for two-phase compositional porous-medium and single-phase compositional free flow, Water Resources Research 47(10).
\bibitem{Brinkman}
H.~Brinkman (1947) A calculation of the viscous force exerted by a flowing fluid on a dense swarm of particles, Appl. Sci. Res. A, 1: 27--34.
%\bibitem{FilterDict_2006}
%S.~Rief, A.~Latz, A.~Wiegmann (2006) Research Note: Computer Simulations of air Filtration including Electric Surface Charges in 3-dimensional fibrous microstructures, Filtration 6(2):169-172.
\bibitem{FilterDictMan}
A.~Wiegmann, L.~Cheng, S.~Rief, A.~Latz, C.~Wagner, R.~Westerteiger (2013) FilterDict tutorail: Simulating Filter Efficiency and lifetime with FilterDict, http://geodict.com/UserGuide/FilterDict2012.pdf
%
% added by Oleg 1.12.
\bibitem{Hornung}
U.~Hornung (1996) 
Homogenization and porous media, Springer, New York.
\bibitem{EspedalFasanoMikelic}
M.~Espedal, A.~Fasano, A.~Mikelic (2000) 
Filtration in porous media and industrial application, 
Springer-Verlag Berlin and Heidelberg, v.1734.
\bibitem{GriebelKlitz}
M.~Griebel and M. ~Klitz (2010)
Homogenization and numerical simulation of flow in geometries with textile microstructures, SIAM MMS, vol.8(4), p.1439.
\bibitem{Kaviany}
M.~Kaviany (1991) 
Principles of heat transfer in porous media, Springer, New York.
\bibitem{Allaire}
G.~Allaire (1991) 
Homogenization of the Naier-Stokes equations in open sets perforated with tiny holes, Arch. Ration. Mech. Anal., vol.113(3) pp.209-259.
\bibitem{JJ86}
Graham W. Jackson and David F. James. The permeability of fibrous media. The Canadian Jounal of Chemical Engineering, 64(3): 364-374, 1986.
\bibitem{Iwa37}
T. Iwasaki. Some notes on sand filtration. J.Am.Wat.Wks. Ass., 29(10):1591-1602, 1937.
\bibitem{BW99}
R.C. Brown and D.Wake. Loading filters with monodisperse aerosols:macroscopic treatment. Journal of Aerosol Science, 30(2): 227-234, 1999.
\bibitem{Pod98}
Albert Podgorski. Macroscopic model of two stage aerosol filtration in fibrous filter without remission of deposits. Journal of Applied Sciences, 2; S929-S930, 1998.
\bibitem{Miin51}
D.M. Mints. Kinetics of the filtration of low concentration water suspensions. Dokl. Acad. Nauk S.S.S.R.,78(29: 315-318, 1951.
\bibitem{KirschFiltech}
O. Iliev, R. Kirsch, Z. Lakdawala, V. Starikovicius, 
On some macroscopic models for depth filtration: Analytical solutions and parameter identification,
In Proceedings of Filtech Europa, 2011.
\bibitem{HybridPermeabilityFiltech}
O. Iliev, Z. Lakdawala, R. Kirsch, K. Steiner, E. Toroshchin, M. Dedering,  
Starikovicius, V; 
CFD simulations for better filter element design, 
In Proceedings of Filtech Europa, 2011.
\bibitem{HouEfendievMsFEMBook}
T.Y. Hou, Y. Efendiev, Multiscale Finite Element Methods, in: Surveys and Tutorials in the Applied Mathematical Sciences, vol. Band 4, Springer, 2009.
\bibitem{Jenny}
P. Jenny, I. Lunati, Multi-scale finite-volume method for elliptic problems with heterogeneous coefficients and source terms, Proc. Appl. Math. Mech.
6(1): 485--486, 2006.
\bibitem{IlievLazarovWillems}
O. Iliev, R.D. Lazarov, J. Willems, Fast numerical upscaling of heat equation for fibrous materials, J. Comput. Vis. Sci. (2009).
\bibitem{Enquist}
B. Enquist, Z. Huang. Heterogenous multiscale method: A general methodology for multiscale modeling, Phys. Rev. B, 67(9), 092101, 2003.
\bibitem{ILS_subgrid}
O. Iliev, Z. Lakdawala, V. Starikovicius. On a numerical subgrid upscaling algorithm for Stokes-Brinkman equations. Journal Computers and Mathematics with Applications, 65(3): 435-448, 2013 
\end{thebibliography}
\end{document}

