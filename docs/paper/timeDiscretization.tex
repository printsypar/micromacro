% !TEX root = ./MiMaCoupling.tex
\section{Time discretization}
\label{timeDiscr}
In this section we discretize in time the coupled micro and macroscale problem (\ref{CoupledMacro})--(\ref{CoupledMicro2}). At first, let us define a time mesh on $[0,T]$ and introduce some notations.
\begin{definition}
\label{mesh}
The mesh on $[0,T]$ denoted by $\mathcal{T}$ is given by a family $(\mathcal{T}^n)_{n=\overline{1,N}}$ with $N\subset\mathbb{N}^+$ and a family $\mathcal{N}$ such that
\begin{align*}
\mathcal{T}^n&=(t^{n},t^{n+1}],\quad\forall n=\overline{0,N-1},
\\
\mathcal{N}&=\{t^n, \; n=\overline{0,N}:\; 0=t^0<t^1<\ldots<t^N=T\}.
\end{align*}
\end{definition}
Let us introduce the following notations for the mesh $\mathcal{T}$
\begin{align*}
&f^n = f(t^n,\mathbf{x}), \quad n=\overline{0,N};\\
&\bigtriangleup t^{n+1} = t^{n+1}-t^{n}, \quad n=\overline{0,N-1}.
\end{align*}

To discretize the coupled micro and macroscale system of equations, we use fractional time step discretization method.  The intermediate time steps are denoted by
\begin{equation*}
t^{n+\theta} = t^n+\theta \Delta t^{n+1},
\end{equation*} 
where $\theta$ satisfies $\theta\in(0,1)$. 
%We use fractional time three-step discretization. Thus, two parameters for intermediate steps are required, namely $\theta$ and $\psi$, such that $\theta<\psi$. 
The coupled system is discretized using the mesh $\mathcal{T}$ for all $n=\overline{0,N-1}$. 
The time discretization of the macroscopic system of equations (\ref{CoupledMacro}) yields
\begin{align}
	\label{td1}
	\begin{split}
	 \rho \frac{\mathbf{U}^{n+\theta}-\mathbf{U}^{n}}{\theta \bigtriangleup t^{n+1}}
	- \bigtriangledown\cdot\left(\mu \bigtriangledown\mathbf{U}^{n+\theta}\right) 
	\quad\quad\quad\quad\quad\quad\\
	+ \left(\rho\mathbf{U}^{n+\theta}\cdot\bigtriangledown\right)\mathbf{U}^{n+\theta}
	+ \mu \mathbf{H}^{n}\mathbf{U}^{n+\theta}\\
	+ \bigtriangledown P^{n+\theta}  &= \mathbf{F}^{n+\theta}\; \text{ in }\Omega,
	\end{split}\\
	\bigtriangledown\cdot\mathbf{U}^{n+\theta} &= 0 \; \text{ in }\Omega,\\
	\mathbf{U}^{n+\theta}  &= \mathbf{U}_{in}, \; \text{ on }\Gamma_{in},\\
	P^{n+\theta}  &= 0, \; \text{ on }\Gamma_{out},\\
	\mathbf{U}^{n+\theta} &= \mathbf 0 \; \text{ on }\Gamma_{w},
\end{align}
\begin{equation}
	\mathbf{U}^n = 
	\begin{cases}
	\mathbf{0} & \text{ for }n=0,\\
	\mathbf{U}^{n-1+\theta} & \text{ for }n>0,\\
	\end{cases}
	\; \text{ in }\Omega,
	\label{td2}
\end{equation}
where $\mathbf{H}^{n}$ can be found using solutions $\mathbf{u}^{n}$ and $p^{n}$ from the previous time step. $\mathbf{H}^0$ corresponding to the clean permeability has to be precomputed in advance. 

The system of microscopic equations (\ref{CoupledMicro1}) for $\alpha=\overline{1,A}$ yields
\begin{align}
	\label{td3}
	\begin{split}
	\rho \frac{\mathbf{u}^{n+1}-\mathbf{u}^{n+\theta}}{(1-\theta) \bigtriangleup t^{n+1}}
	-\bigtriangledown\cdot\left(\mu\bigtriangledown\mathbf{u}^{n+1} \right)
	\quad\\
	+ \left(\rho\mathbf{u}^{n+1}\cdot\bigtriangledown\right)\mathbf{u}^{n+1}
	+\bigtriangledown p^{n+1} &= \mathbf f^{n+1} \; \text{ in }\omega_{f,\alpha}(t^n),
	\end{split}\\
	\bigtriangledown\cdot\mathbf{u}^{n+1} &= 0 \; \text{ in } \omega_{f,\alpha}(t^n),\\
	\mathbf{u}^{n+1} &= 0 \; \text{ on } \gamma_{s,\alpha}(t^n),\\
	\mathbf{u}^{n+1} &= \mathbf{U}^{n+\theta} \; \text{ on }\gamma_{in,\alpha},\\
	p^{n+1} &= 0 \; \text{ on }\gamma_{out,\alpha},\\
	\mathbf{u}^{n+1},\; p^{n+1}&\rightarrow\text{periodic on }\gamma_{w,\alpha},
%	\label{td4}
%	\mathbf{u}^0 &= \mathbf{0} \text{ in }\omega^m_{f,\alpha}.
\end{align}
\begin{equation}
	\label{td4}
	\mathbf{u}^{n+\theta} = 
	\begin{cases}
	\mathbf{0} & \text{ for }n=0,\\
	\mathbf{u}^{n} & \text{ for }n>0,\\
	\end{cases}
	\; \text{ in }\omega_{f,\alpha}(t^n),
\end{equation}

Finally, microscopic equations for particle motion (\ref{CoupledMicro2}) read 
\begin{align}
	\label{td5}
	& \mathbf{y}^{n+1} = \mathbf{y}^{n} + \mathbf{v}^{n+1}\bigtriangleup t^{n+1}, \\
	\label{td5a}
	& \begin{cases}
	\!\begin{aligned}%[b]
	\mathbf{v}^{n+1} = \mathbf{v}^{n} 
	&- M\left(\mathbf{v}^{n+1}-\mathbf{u}^{n+1}(\mathbf{y}^{n+1})\right)\bigtriangleup t^{n+1}\\
	&+\frac{QE_0(\mathbf{y}^{n+1})}{m}\bigtriangleup t^{n+1}+\sigma d\mathbf{W}^{n+1},
        \end{aligned}
        &\text{for }\mathbf{y}\in\omega_{f,\alpha}(t^n), \\%[1ex]
	\mathbf{v}^{n+1} = A(m\mathbf{v}^{n+1},\gamma_{s,\alpha}(t^n),\mathcal{M}), & \text{for }\mathbf{y}\in\gamma_{s,\alpha}(t^n),
	\end{cases}
	\\
%\begin{split}
%& \mathbf{v}^{n+1} = \mathbf{v}^{n} 
%- M\left(\mathbf{v}^{n+1}-\mathbf{u}^{n+1}(\mathbf{y}^{n+1})\right)\bigtriangleup t^{n+1} \\
%&\quad\quad\quad\quad\quad\quad\;
%+\frac{QE_0(\mathbf{y}^{n+1})}{m}\bigtriangleup t^{n+1}+\sigma d\mathbf{W}^{n+1},
%\end{split} \\
	& \left\langle dW_i^{n+1},dW_j^{n+1}\right\rangle = \delta_{ij}\bigtriangleup t^{n+1}, \\
	\label{td6}
	& \mathbf{y}^{n}=\mathbf{b},\quad \mathbf{v}^{n}=\mathbf{u}^{n+1}(\mathbf{b}).
\end{align}
%where $\mathbf y^{n+1}\in\omega_{f,\alpha}(t^n)$. 
After particles are immobilized in first equation of (\ref{td5a}) the microscopic computational domain is updated
\begin{align}
	&\omega_{s,\alpha}(t^{n+1})=\omega^{clean}_{s,\alpha}\cup\omega^{par}_{s,\alpha}(t^{n+1}),\\
	&\omega_{f,\alpha}(t^{n+1})=\omega_{\alpha}\setminus\omega_{s,\alpha}(t^{n+1}).
\end{align}

The presented time discretization implies that computations for all sub-models, namely the macroscopic, microscopic flow, and microscale particle motion models,  are performed sequentially at each time step. However, in practice processes described by these models occur at different time scales. After one batch of particles, the flow field in the each microstructure does not change much. Therefore, flow field from previous time step can be reused for the next particle tracking computations. Moreover, even when some particles are deposited and flow computations have to be performed at the microscale, permeability of the whole porous medium might not be changed enough to rerun the macroscale computations. Thus, we use big time steps $\bigtriangleup t^{n+1}$ for $n=\overline{0,N-1}$, which represent a time scale of the macroscopic changes, and we also perform additional refinement of the time sub-steps $(t^n+\theta \bigtriangleup t^{n+1}, t^{n+1}]$ for the microscopic flow.
