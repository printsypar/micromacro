\section{Micro- and macroscale simulation coupling}
\label{coupling}
{\color{blue}
Simulating filtering process only at the scale of a filter element does not account for changes of permeability occurring due to particle deposition at micro scale. To take into consideration this effect without microscale simulations, laboratory experiments have to be carried out and empirical relationships have to be provided as input data, which are usually expansive and time consuming procedures. Performing simulations of the filtering process only at the micro scale allows us to resolve microstructure and to account for the particle deposition. But the whole filter element can not be simulated at micro scale due to big size of the problem. Without resolving the filter element geometry we do not get complete picture of the process.
}
Thus, both scales are of the same importance for the filtration efficiency simulations. In this study we develop a coupling approach for the filter efficiency simulations which accounts for the processes occurring at the micro and macro scales.

Let us first make several assumptions on the process. 
\begin{assumption}
\label{ass1}
Fluid flow is laminar.
\end{assumption}
\begin{assumption}
Particles, which are not deposited, do not influence the fluid flow.
\end{assumption}
\begin{assumption}
Particles do not interact with each other. In other words, particle size and concentration are assumed to be small.
\end{assumption}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Macroscale problem}
\label{macro}
{\it {\color{red} (OI: rewrite) } {\color{blue} (GP: I tried to rewrite it below in blue color) }
To simulate a filter element two flow regimes have to be taken into account, namely free fluid flow and fluid flow through porous media. Stokes-Brinkman equations, which are an extension of Darcy model, are used to simulate the flow through the filter element. Let $\Omega$ be the computational domain representing the filter element and $\Omega_f$ and $\Omega_p$ be fluid and porous subsets of $\Omega$, respectively. Then, Stokes-Brinkman system of equations describing laminar, incompressible and isothermal flow reads
}

{\color{blue}
At macro scale filtering processes have to be modelled accounting for two flow regimes, namely free fluid flow coupled with fluid flow through porous media. There exist several different approaches to account for it (e.g. see \cite{Helmig} and references therein). This study uses Stokes--Brinkman equations, which are an extension of Darcy model (see \cite{Brinkman}).

Let the computational domain be defined by $\Omega\subset\mathbb{R}^3$, which is divided into two subsets $\Omega_f$ and $\Omega_p$ corresponding to the fluid and porous domains, respectively (see Figure~\ref{notations}). The subsets satisfy $\Omega_f\cup\Omega_p=\Omega$ and $\Omega_f\cap\Omega_p=\emptyset$. Let the boundary $\partial\Omega$ be split into three parts
$\partial\Omega=\Gamma_{in} \cup\Gamma_{out}\cup\Gamma_{w}$ such that $\Gamma_{\alpha}\cap\Gamma_{\beta}=\emptyset$ for $\alpha\neq\beta$ and $\alpha$, $\beta=\{in,\: out,\: w\}$. Indices '$in$', '$out$', and '$w$' correspond to a flow inlet, a flow outlet, and solid walls. The Stokes-Brinkman system of equations describing laminar, incompressible, isothermal flow reads
}
\begin{align}
\label{mac1}
\rho \frac{\partial \mathbf{U}}{\partial t}
- \bigtriangledown\cdot\left(\mu \bigtriangledown\mathbf{U}\right)
+\mu \mathbf{H}\mathbf{U}+\bigtriangledown P &= \mathbf{F} \text{ in }[0,T]\times\Omega,\\
\label{mac2}
\bigtriangledown\cdot\mathbf{U} &= 0 \text{ in }[0,T]\times\Omega,\\
\label{mac3}
\mathbf{U} = \mathbf{U}_{in}, \quad
\frac{\partial P}{\partial \mathbf{n}} &=0 \text{ on }[0,T]\times\Gamma_{in};\\
\label{mac4}
P = P_{out}, \quad \frac{\partial\mathbf{U}}{\partial \mathbf{n}}&=0\text{ on }[0,T]\times\Gamma_{out};\\
\label{mac5}
\mathbf{U} &=0 \text{ on }[0,T]\times\Gamma_{w};\\
\label{mac6}
\mathbf{U}(t=0) &= \mathbf{g} \text{ in }\Omega;
\end{align}
where 
	$\mathbf{U}$ is the fluid velocity, 
	$P$ is the fluid pressure,
	$\mathbf{F}$ is the macroscopic force vector,
	$\mathbf{n}$ is the outward normal vector,
	$\mathbf{g}$ is the initial condition and
\begin{equation}
  \label{perm}
  \mathbf{H}=
  \begin{cases}
  \mathbf{K}^{-1}(t,\mathbf x)&\text{ in }[0,T]\times\Omega_p,\\
  0&\text{ in }[0,T]\times\Omega_f;
  \end{cases}
\end{equation}
with a symmetric, positive definite, intrinsic permeability tensor $\mathbf{K}$. 

System of equations can be used to account for the fluid flow through a filter element. The problem and the solution algorithm are discussed in detail in PhD thesis of Z.~Lakdawala \cite{ZahraPhD} (see also references therein). The proposed method is implemented in software FiltEST \cite{FiltEST}, which was used to carry out the macroscale simulation in this study.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[!h]
  \centering
  \includegraphics[width=0.8\textwidth]{pics/2DMiMawithNotations} 
  \caption{Computational domains for macro- and microscale problems}
  \label{notations}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Microscale problems}
\label{micro}
Let $\omega_{\alpha}$ be a partition of $\Omega_p$, where $\omega_{\alpha}\subset{\mathbb{R}^3}$ for $\alpha=\overline{1,A}$ (see Figure~\ref{notations}). In other words $\omega_{\alpha}$ are microstructures associated with the macro porous domain. The microstructures $\omega_{\alpha}$ consist of two complementary parts the fluid $\omega_{f,\alpha}$ and the solid $\omega_{s,\alpha}$, such that $\omega_{\alpha}=\omega_{f,\alpha}\cup\omega_{s,\alpha}$ and $\omega_{f,\alpha}\cap\omega_{s,\alpha}=\emptyset$. 
Let the boundary of each domain $\omega_{\alpha}$ be split into three non-intersecting parts $\partial\omega_{\alpha}=\gamma_{in,\alpha}\cup\gamma_{out,\alpha}\cup\gamma_{w,\alpha}$. As before, indices '$in$', '$out$', and '$w$' correspond to inflow, outflow, and side boundaries, respectively.

At any given time $t=t^{\prime}$, the components of the permeability tensor $\mathbf{K}$ from (\ref{perm}) can be found as follows 
{\color{blue}
(from homogenization)
$$
K_{ij}=\int_{\mathbb{R}^3\setminus\omega_{s,\alpha}}\bigtriangledown u_i\cdot\bigtriangledown u_j dx \text{ for }\alpha=\overline{1,A};
$$
or
(from volume averaging ??? is averaging over $\omega_{\alpha}$ or over $\omega_{f,\alpha}$)
$$
K_{ij}(t^{\prime},\mathbf x)=
\mu\frac{\left\langle \mathbf u(t^{\prime},\mathbf x)\right\rangle_{\omega_{\alpha},i}}
{\left\langle\bigtriangledown p(t^{\prime},\mathbf x)\right\rangle_{\omega_{\alpha},j} }
\text{ in }\omega_{\alpha};
$$
where
$$
\left\langle\cdot\right\rangle_{\omega_{\alpha},i}=\frac{1}{\int_{\omega_{\alpha}}d\mathbf{x}}\int_{\omega_{\alpha}}
(\cdot)_id\mathbf x$$
}
with $\mathbf u$ and $p$  are solutions to micro problems, which are introduced below.

\begin{remark}
If clean porous media is homogeneous at initial time moment, the permeability tensor $\mathbf{K}$ in (\ref{perm}) will change in time and space because of the deposited particles. If the porous media is heterogeneous, $\mathbf{K}$ will also depend on the microstructures $\omega_{s,\alpha}$.
\end{remark}

The mathematical modeling of filtering processes at the micro scale is performed in two main steps. At first, for a given microstructure the flow field is computed. Then, one or several batches of particles are tracked in the computed flow field. Ordinary differential equations describing the particle motion account for friction with fluid, electric attraction of particles to the microstructure and diffuse motion.

%\begin{figure}
%  \centering
%  \includegraphics[width=0.6\textwidth]{pics/FilterDictExample} 
%  \caption{FilterDict simulation example}
%  \label{fig1}
%\end{figure}

Since it is assumed that the flow is laminar (see Assumption~\ref{ass1}), Stokes equations with periodic boundary conditions are solved to obtain the flow field:

{\it \color{red}(OI) reference 1) math, 2) Kaviany}

{\it \color{red}(OI) we suppose that the flow establishes very fast.}
{\color{blue} We assume it after discretization over time (see Remark~\ref{rem4}).} 

\begin{align}
\label{mic1}
\rho \frac{\partial \mathbf{u}}{\partial t}
-\bigtriangledown\cdot\left(\mu \bigtriangledown \mathbf{u} \right)
+\bigtriangledown p = \mathbf f &\text{ in }  [0,T]\times\omega_{f,\alpha},\\
\label{mic2}
\bigtriangledown\cdot\mathbf{u}=0 &\text{ in } [0,T]\times \omega_{f,\alpha},\\
\label{mic3}
\mathbf{u}=0 &\text{ on } [0,T]\times\{\partial\omega_{f,\alpha}\cap\partial\omega_{s,\alpha}\};\\
\label{mic4}
\mathbf{u} = \mathbf{U}, \quad \frac{\partial p}{\partial\mathbf{n}}=0&\text{ on }[0,T]\times\{\gamma_{in,\alpha}\cap\omega_{f,\alpha}\};\\
\label{mic5}
\frac{\partial\mathbf{u}}{\partial \mathbf{n}}=0,\quad p=0 &\text{ on }[0,T]\times\{\gamma_{out,\alpha}\cap\omega_{f,\alpha}\};\\
\label{mic6}
\mathbf{u},\; p\rightarrow\text{periodic} &\text{ on }[0,T]\times\gamma_{w,\alpha};\\
\label{mic7}
\mathbf{u}(t=0)=\mathbf{q} &\text{ in }\omega_{f,\alpha};
\end{align}
with $\alpha=\overline{1,A}$,
where 
	$\rho$ is the fluid density, 
	$\mathbf{u}$ is the fluid velocity, 
	$t$ is the time,
	$\mu$ is the fluid dynamic viscosity,
	$p$ is the fluid pressure,
	$\mathbf{f}$ is the force,
	$T>0$ is the total time,
	$q$ is the initial condition.

Equations~(\ref{mic1}) and (\ref{mic2}) are the momentum and mass conservation equations, respectively. Equation~(\ref{mic3}) represents the no-slip condition for the fluid velocity at the interfaces between fluid and solid phases. Equations (\ref{mic4})--(\ref{mic6}) are the boundary conditions. We remark that fluid velocity from the macroscale problem is used as the boundary condition for the inflow microscale velocity (see (\ref{mic4})). 

For the particle motion the following system of equations in Lagrangian coordinates is solved (for more details see \cite{LatzWiegmann} and references therein):
\begin{align}
\label{eq4}
d \mathbf{y} = \mathbf{v}dt \text{ in }[0,T], & \\
\label{eq5}
d \mathbf{v} = -M\left(\mathbf{v}(\mathbf{y})-\mathbf{u}(\mathbf{y})\right)dt
+\frac{QE_0(\mathbf{y})}{m}dt+\sigma d\mathbf{W}(t) \text{ in }[0,T], & \\
\label{eq8}
\left\langle dW_i(t),dW_j(t)\right\rangle = \delta_{ij}dt \text{ in }[0,T], & \\
\label{eq9}
\mathbf{y}(0)=\mathbf{b},\quad
\mathbf{v}(\mathbf{y}(0))=\mathbf{u}(\mathbf{b}),
\end{align}
where
$$
C_c = 1+K_n\left(1.17+0.525\exp{-\frac{0.78}{K_n}}\right),
$$
$$
M = 6\pi\mu\frac{R}{C_cm}, \qquad
\sigma^2 = \frac{2k_B TM}{m}, \qquad
K_n = \frac{\lambda}{R},
$$
	$\mathbf{y}$ is the particle position, 
	$\mathbf{v}$ is the particle velocity,
	$M$ is the friction coefficient,
	$Q$ is the particle charge,
	$m$ is the particle mass,
	$E_0$ is the electric field,
	$d\mathbf{W}(t)$ is the 3D probability (Wiener) measure,
	$\mathbf{b}$ is the initial position of the particle,
	$C_c$ is the Cunningham correction,
	$K_n$ is the Knudsen number,
	$R$ is the particle radius,
	$k_B$ is the Boltzmann constant,
	$T$ is the Ambient temperature,
	$\lambda$ is the mean free path.
The first term on the right hand side of equation~(\ref{eq5}) corresponds to the particle friction with fluid. The second and third terms represent electric attraction and the Brownian  motion. Equations (\ref{eq9}) are the initial conditions for the particle positions and velocity.

\begin{remark}
We would like to note that partition of the microstrucrutes $\omega_{\alpha}$ changes with time due to deposited particles, which become part of the solid domain
$$
\omega_{f,\alpha}(t)\cup\omega_{s,\alpha}(t)=\omega_{\alpha}.
$$
\end{remark}

Another important effect in the filtration process, which has to be considered carefully, is the adhesion of the particles to the solid structure. There are different models which can be considered, e.g. Hamaker model, sieving, collision count (see e.g. \cite{FilterDictMan}). Which adhesion model is better to use it has to be defined in each particular case based on process conditions.

\begin{remark}
For numerical experiment we do not use the whole set of $\omega_{\alpha}$ for $\alpha=\overline{1,A}$, because it is computationally very expensive. Instead a subset $\mathcal{L}\subset\{\overline{1,A}\}$ is defined, the micro problems are solved for all $\alpha\in \mathcal{L}$, and then the permeability tensor $\mathbf{K}$ is interpolated in $\Omega_p\setminus\{\cup_{\alpha\in \mathcal{L}}\omega_{\alpha}\}$.
\end{remark}

{\it {\color{red}(OI: rewrite) } 
Described mathematical model for the particles motion with fluid in a microstructure was proposed in \cite{LatzWiegmann} and implemented in GeoDict software \cite{GeoDict}. Module of GeoDict, namely FilterDict, was used to carry out the microscale simulations. 
%An example of FilterDict simulations is shown in Figure~\ref{fig1}. 
More detailed description of the mathematical model and software features can be found in the user manual of FilterDict module \cite{FilterDictMan}.}
